#define MyAppName "POS_System"
#define MyAppVersion "1.1.30"
#define MyAppPublisher "V4Codes"
#define MyAppExeName "POS-System.exe"
#define MyAppAssocName MyAppName + ""
#define MyAppAssocExt ".myp"
#define MyAppAssocKey StringChange(MyAppAssocName, " ", "") + MyAppAssocExt

[Setup]

AppId={{EB5B7843-7079-44DB-9CF7-C0754C808002}
AppName={#MyAppName}
AppVersion={#MyAppVersion}
AppPublisher={#MyAppPublisher}
DefaultDirName={autopf}\V4Codes\POS_System
ChangesAssociations=yes
DisableProgramGroupPage=yes
PrivilegesRequiredOverridesAllowed=commandline
OutputDir=C:\Users\RamzyAhmed\Desktop\V4Codes Projects
OutputBaseFilename=POS_System
SetupIconFile=C:\Users\RamzyAhmed\Desktop\V4Codes Projects\testfdd\POS - Cleanup\POS-System\Group 5.ico
//Password=12345
//Encryption=yes
Compression=lzma
SolidCompression=yes
WizardStyle=modern
UninstallDisplayIcon={app}\{#MyAppExeName}

[Languages]
Name: "english"; MessagesFile: "compiler:Default.isl"

[Tasks]
Name: "desktopicon"; Description: "{cm:CreateDesktopIcon}"; GroupDescription: "{cm:AdditionalIcons}"; Flags: unchecked

[Files]
Source: "C:\Users\RamzyAhmed\Desktop\V4Codes Projects\testfdd\POS - Cleanup\POS-System\bin\Release\{#MyAppExeName}"; DestDir: "{app}"; Flags: ignoreversion

Source: "C:\Users\RamzyAhmed\Desktop\V4Codes Projects\testfdd\POS - Cleanup\POS-System\bin\Release\*"; DestDir: "{app}"; Flags: uninsneveruninstall ignoreversion recursesubdirs createallsubdirs onlyifdoesntexist

Source: "C:\Users\RamzyAhmed\Desktop\V4Codes Projects\testfdd\POS - Cleanup\POS-System\bin\Release\*"; Excludes: "\libdb"; DestDir: "{app}"; Flags: ignoreversion recursesubdirs createallsubdirs

[Registry]
Root: HKA; Subkey: "Software\Classes\{#MyAppAssocExt}\OpenWithProgids"; ValueType: string; ValueName: "{#MyAppAssocKey}"; ValueData: ""; Flags: uninsdeletevalue
Root: HKA; Subkey: "Software\Classes\{#MyAppAssocKey}"; ValueType: string; ValueName: ""; ValueData: "{#MyAppAssocName}"; Flags: uninsdeletekey
Root: HKA; Subkey: "Software\Classes\{#MyAppAssocKey}\DefaultIcon"; ValueType: string; ValueName: ""; ValueData: "{app}\{#MyAppExeName},0"
Root: HKA; Subkey: "Software\Classes\{#MyAppAssocKey}\shell\open\command"; ValueType: string; ValueName: ""; ValueData: """{app}\{#MyAppExeName}"" ""%1"""
Root: HKA; Subkey: "Software\Classes\Applications\{#MyAppExeName}\SupportedTypes"; ValueType: string; ValueName: ".myp"; ValueData: ""

[Icons]
Name: "{autoprograms}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Flags: runmaximized
Name: "{autodesktop}\{#MyAppName}"; Filename: "{app}\{#MyAppExeName}"; Tasks: desktopicon; Flags: runmaximized

[Run]
Filename: "{app}\{#MyAppExeName}"; Description: "{cm:LaunchProgram,{#StringChange(MyAppName, '&', '&&')}}"; Flags: nowait postinstall skipifsilent runmaximized

[Dirs]
Name: "{app}"; Permissions: users-full

[Run]
Filename: "cmd.exe"; Parameters: "/C del ""{app}\installed.txt"""; Flags: runhidden