﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;

namespace Microsoft.Reporting.WinForms
{
    public static class LocalReportExtensions
    {
        public static void PrintToPrinter(this LocalReport report, DataGridViewRowCollection datasource)
        {
            PageSettings pageSettings = new PageSettings
            {
                //PaperSize = report.GetDefaultPageSettings().PaperSize,
                //Landscape = report.GetDefaultPageSettings().IsLandscape,
                Margins = report.GetDefaultPageSettings().Margins
            };
            int estimatedHeight = EstimateReportHeight(datasource);
            if (datasource != null)
            {
                pageSettings.PaperSize = new PaperSize("CustomSize", 280, report.GetDefaultPageSettings().PaperSize.Height + estimatedHeight);
                pageSettings.Landscape = false;
            }
            else
            {
                pageSettings.PaperSize = report.GetDefaultPageSettings().PaperSize;
                pageSettings.Landscape = report.GetDefaultPageSettings().IsLandscape;
            }

            Print(report, pageSettings, estimatedHeight);
        }

        private static int EstimateReportHeight(DataGridViewRowCollection datasource)
        {
            int estimatedHeight = 0;

            foreach (var dataItem in datasource)
            {
                estimatedHeight += 40;
            }

            //estimatedHeight += 590; // Adjust as needed

            return estimatedHeight;
        }

        public static void Print(this LocalReport report, PageSettings pageSettings, int height)
        {
            string deviceInfo =
                $@"<DeviceInfo>
                    <OutputFormat>EMF</OutputFormat>
                    <PageWidth>{280 * 100}in</PageWidth>
                    <PageHeight>0in</PageHeight>
                    <MarginTop>{pageSettings.Margins.Top * 100}in</MarginTop>
                    <MarginLeft>{pageSettings.Margins.Left * 100}in</MarginLeft>
                    <MarginRight>{pageSettings.Margins.Right * 100}in</MarginRight>
                </DeviceInfo>";
            List<Stream> streams = new List<Stream>();
            int pageIndex = 0;
            report.Render("Image", deviceInfo,
                (name, fileNameExtension, encoding, mimeType, willSeek) =>
                {
                    MemoryStream stream = new MemoryStream();
                    streams.Add(stream);
                    return stream;
                }, out Warning[] warnings);
            foreach (Stream stream in streams)
            {
                stream.Position = 0;
            }

            if (streams == null || streams.Count == 0)
            {
                throw new Exception("No stream to print.");
            }

            using (PrintDocument printDocument = new PrintDocument())
            {
                printDocument.DefaultPageSettings = pageSettings;
                if (!printDocument.PrinterSettings.IsValid)
                {
                    throw new Exception("Can't find the default printer.");
                }
                else
                {
                    printDocument.PrintPage += (sender, e) =>
                    {
                        Metafile pageImage = new Metafile(streams[pageIndex]);
                        int printHeight = (int)(pageImage.Height / (float)pageImage.Width * e.PageBounds.Width);

                        Rectangle adjustedRect = new Rectangle(e.PageBounds.Left, e.PageBounds.Top, e.PageBounds.Width, printHeight - 50);

                        e.Graphics.FillRectangle(Brushes.White, adjustedRect);
                        e.Graphics.DrawImage(pageImage, adjustedRect);

                        pageIndex++;
                        e.HasMorePages = pageIndex < streams.Count;
                    };
                    printDocument.EndPrint += (Sender, e) =>
                    {
                        if (streams != null)
                        {
                            foreach (Stream stream in streams)
                            {
                                stream.Close();
                            }

                            streams = null;
                        }
                    };
                    PrintController pr = new StandardPrintController();
                    printDocument.PrintController = pr;
                    printDocument.Print();
                }
            }
        }
    }
}
