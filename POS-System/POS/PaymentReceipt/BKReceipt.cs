﻿using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.POS.PaymentReceipt
{
    public partial class BKReceipt : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly Cashier cashier;
        private string store;
        private string address;
        private string mobile;
        private string email;
        private bool bkprintopt;
        private bool paymentprintopt;
        public BKReceipt(Cashier cashier)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            this.cashier = cashier;
            LoadStore();
        }

        public void LoadStore()
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblStore LIMIT 1", cn);
            dr = cmd.ExecuteReader();
            _ = dr.Read();
            if (dr.HasRows)
            {
                store = dr["store"].ToString();
                address = dr["address"].ToString() != string.Empty ? dr["address"].ToString() : "-";
                mobile = dr["mobile"].ToString() != string.Empty ? dr["mobile"].ToString() : "-";
                email = dr["email"].ToString() != string.Empty ? dr["email"].ToString() : "-";
                bkprintopt = dr["bookfield"].ToString() == "1";
                paymentprintopt = dr["paymentfiled"].ToString() == "1";
            }
            else
            {
                store = "V4Codes";
                address = "-";
                mobile = "-";
                email = "-";
                bkprintopt = true;
                paymentprintopt = true;
            }
            dr.Close();
            cn.Close();
        }

        public void LoadBKReceipt(string pcash, string cname)
        {
            try
            {
                if (bkprintopt)
                {
                    string sql = "Select * from BKReceipt where transno LIKE '" + cashier.lbltransno.Text + "'";
                    dbCon.ExecuteQuery(sql);
                    DataTable dt = dbCon.getTable(sql);

                    reportViewer1.LocalReport.ReportPath = Application.StartupPath + @"\Report\BKRPT.rdlc";
                    ReportDataSource rpt = new ReportDataSource("DataSet1", dt);
                    reportViewer1.LocalReport.DataSources.Clear();

                    ReportParameter StoreName = new ReportParameter("StoreName", store);
                    ReportParameter StoreAddress = new ReportParameter("StoreAddress", address);
                    ReportParameter InvoiceNumber = new ReportParameter("InvoiceNumber", cashier.lbltransno.Text);
                    ReportParameter StoreMobile = new ReportParameter("StoreMobile", mobile);
                    ReportParameter DiscountAmount = new ReportParameter("DiscountAmount", cashier.lbldiscount.Text);
                    ReportParameter SubTotalAmount = new ReportParameter("SubTotalAmount", cashier.lblsalestotal.Text);
                    ReportParameter TotalAmount = new ReportParameter("TotalAmount", cashier.lblDisplayTotal.Text);
                    ReportParameter PaidAmount = new ReportParameter("PaidAmount", pcash);
                    ReportParameter CustomerName = new ReportParameter("CustomerName", cname);
                    ReportParameter CashierName = new ReportParameter("CashierName", cashier.lblusername.Text);
                    ReportParameter StoreEmail = new ReportParameter("StoreEmail", email);

                    reportViewer1.LocalReport.SetParameters(DiscountAmount);
                    reportViewer1.LocalReport.SetParameters(TotalAmount);
                    reportViewer1.LocalReport.SetParameters(PaidAmount);
                    reportViewer1.LocalReport.SetParameters(CustomerName);
                    reportViewer1.LocalReport.SetParameters(StoreName);
                    reportViewer1.LocalReport.SetParameters(StoreAddress);
                    reportViewer1.LocalReport.SetParameters(InvoiceNumber);
                    reportViewer1.LocalReport.SetParameters(CashierName);
                    reportViewer1.LocalReport.SetParameters(StoreMobile);
                    reportViewer1.LocalReport.SetParameters(SubTotalAmount);
                    reportViewer1.LocalReport.SetParameters(StoreEmail);

                    reportViewer1.LocalReport.DataSources.Add(rpt);
                    reportViewer1.LocalReport.Refresh();
                    reportViewer1.ZoomMode = ZoomMode.PageWidth;
                    reportViewer1.LocalReport.PrintToPrinter(null);
                    reportViewer1.RefreshReport();
                }
            }
            catch (Exception ex)
            {
                cn.Close();
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void BKReceipt_Load(object sender, EventArgs e)
        {
            _ = bKReceiptTableAdapter.Fill(dataSet1.BKReceipt);
            reportViewer1.RefreshReport();
            reportViewer1.RefreshReport();
            reportViewer1.RefreshReport();
        }
    }
}
