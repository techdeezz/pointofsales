﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.POS.CancelOrder
{
    public partial class Void : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly CancelOrderModule cancelOrder;
        public Void(CancelOrderModule cancelOrder)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            this.cancelOrder = cancelOrder;
        }

        private void btnVoid_Click(object sender, EventArgs e)
        {
            try
            {
                bool found = false;
                string user;

                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM tblUser WHERE username = @username AND password = @password AND role='Administrator'", cn);
                _ = cmd.Parameters.AddWithValue("@username", txtusername.Text.ToUpper());
                _ = cmd.Parameters.AddWithValue("@password", txtpass.Text);
                dr = cmd.ExecuteReader();
                _ = dr.Read();
                if (dr.HasRows)
                {
                    found = true;
                    user = dr["username"].ToString();
                    string pass = dr["password"].ToString();
                    dr.Close();
                    cn.Close();
                    SaveCancelOrder(user);

                    if (cancelOrder.cmbinventory.Text == "Yes")
                    {
                        dbCon.ExecuteQuery("UPDATE tblProduct SET qty = qty +" + cancelOrder.numericUpDown1.Value + " WHERE pcode = '" + cancelOrder.txtpcode.Text + "'");
                    }
                    dbCon.ExecuteQuery("UPDATE tblCart SET qty = qty -" + cancelOrder.numericUpDown1.Value + " WHERE id LIKE '" + cancelOrder.txtid.Text + "'");
                    _ = MessageBox.Show("Order transaction successfully cancelled!", "Cancel Order", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Dispose();
                    cancelOrder.ReloadSoldList();
                    cancelOrder.Dispose();
                }
                if (found == false)
                {
                    _ = MessageBox.Show("Invalid username or password", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                cn.Close();
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void SaveCancelOrder(string user)
        {
            try
            {
                cn.Open();
                cmd = new SQLiteCommand("INSERT INTO tblCancel (transno,pcode,price,qty,total,sdate,voidby,cancelledby,reason,action)VALUES(@transno,@pcode,round(@price,2),@qty,@total,@sdate,@voidby,@cancelledby,@reason,@action)", cn);
                _ = cmd.Parameters.AddWithValue("@transno", cancelOrder.txttrans.Text);
                _ = cmd.Parameters.AddWithValue("@pcode", cancelOrder.txtpcode.Text);
                _ = cmd.Parameters.AddWithValue("@price", double.Parse(cancelOrder.txtprice.Text));
                _ = cmd.Parameters.AddWithValue("@qty", decimal.Parse(cancelOrder.txtqty.Text));
                _ = cmd.Parameters.AddWithValue("@total", double.Parse(cancelOrder.txttotal.Text));
                _ = cmd.Parameters.AddWithValue("@sdate", DateTime.Now);
                _ = cmd.Parameters.AddWithValue("@voidby", user);
                _ = cmd.Parameters.AddWithValue("@cancelledby", cancelOrder.txtcancelby.Text);
                _ = cmd.Parameters.AddWithValue("@reason", cancelOrder.txtreason.Text);
                _ = cmd.Parameters.AddWithValue("@action", cancelOrder.cmbinventory.Text);
                _ = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Error");
            }
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void Void_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Dispose();
            }
        }
        private void txtusername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void txtpass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnVoid.PerformClick();
            }
        }
    }
}
