﻿using POS_System.POS.Report;
using System;
using System.Windows.Forms;

namespace POS_System.POS.CancelOrder
{
    public partial class CancelOrderModule : Form
    {
        private readonly DailySales dailySales;
        public CancelOrderModule(DailySales dailySales)
        {
            InitializeComponent();
            _ = numericUpDown1.Focus();
            numericUpDown1.Select();
            this.dailySales = dailySales;
            cmbinventory.SelectedIndex = 0;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void CancelOrderModule_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Dispose();
            }
        }

        private void btnCOrder_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbinventory.Text != string.Empty && numericUpDown1.Value > 0)
                {
                    if (decimal.Parse(txtqty.Text) >= numericUpDown1.Value)
                    {
                        Void @void = new Void(this);
                        _ = @void.ShowDialog();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void ReloadSoldList()
        {
            dailySales.LoadSalesOnLoad();
        }

        private void cmbinventory_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void txtvoidby_KeyDown_1(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnCOrder.PerformClick();
            }
        }

        private void numericUpDown1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnCOrder.PerformClick();
            }
        }
    }
}
