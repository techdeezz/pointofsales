﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.POS
{
    public partial class Quantity : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private string pcode;
        private double price;
        private string transno;
        private decimal qty;
        private readonly Cashier cashier;
        private readonly string sTitle = "Point Of Sale";

        public Quantity(Cashier cashier)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            this.cashier = cashier;
        }

        private void Quantity_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                Dispose();
            }
        }
        public void ProductDetails(string pcode, double price, string transno, decimal qty)
        {
            this.pcode = pcode;
            this.price = price;
            this.transno = transno;
            this.qty = qty;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((e.KeyChar == (char)13) && (txtQty.Text != string.Empty))
            {
                try
                {
                    string id = "";
                    decimal cart_qty = 0;
                    bool found = false;

                    cn.Open();
                    cmd = new SQLiteCommand("SELECT * FROM tblCart WHERE transno = @transno AND pcode=@pcode", cn);
                    _ = cmd.Parameters.AddWithValue("@transno", transno);
                    _ = cmd.Parameters.AddWithValue("@pcode", pcode);
                    dr = cmd.ExecuteReader();
                    _ = dr.Read();
                    if (dr.HasRows)
                    {
                        id = dr["id"].ToString();
                        cart_qty = decimal.Parse(dr["qty"].ToString());
                        found = true;
                    }
                    else
                    {
                        found = false;
                    }

                    dr.Close();
                    cn.Close();

                    if (found)
                    {
                        if (qty < (decimal.Parse(txtQty.Text) + cart_qty))
                        {
                            _ = MessageBox.Show("Unable to proceed. Remaining quantity on hand is " + qty, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        cn.Open();
                        cmd = new SQLiteCommand("update tblCart set qty=(qty + " + decimal.Parse(txtQty.Text) + ") where id= '" + id + "'", cn);
                        _ = cmd.ExecuteReader();
                        cn.Close();
                        cashier.txtBarcode.Clear();
                        _ = cashier.txtBarcode.Focus();
                        cashier.LoadCart();
                        Dispose();
                    }
                    else
                    {
                        if (qty < decimal.Parse(txtQty.Text) + cart_qty)
                        {
                            _ = MessageBox.Show("Unable to proceed. Remaining quantity on hand is " + qty, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return;
                        }
                        cn.Open();
                        cmd = new SQLiteCommand("INSERT INTO tblCart(transno,pcode,price,qty,sdate,cashier)VALUES(@transno,@pcode,round(@price,2),@qty,@sdate,@cashier)", cn);

                        _ = cmd.Parameters.AddWithValue("@transno", transno);
                        _ = cmd.Parameters.AddWithValue("@pcode", pcode);
                        _ = cmd.Parameters.AddWithValue("@price", price);
                        _ = cmd.Parameters.AddWithValue("@qty", decimal.Parse(txtQty.Text));
                        _ = cmd.Parameters.AddWithValue("@sdate", DateTime.Now);
                        _ = cmd.Parameters.AddWithValue("@cashier", cashier.lblusername.Text);

                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        cashier.txtBarcode.Clear();
                        _ = cashier.txtBarcode.Focus();
                        cashier.LoadCart();
                        Dispose();
                    }
                }
                catch (Exception ex)
                {
                    _ = MessageBox.Show(ex.Message, sTitle);
                }
            }
        }
    }
}
