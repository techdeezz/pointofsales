﻿using POS_System.POS.PaymentReceipt;
using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System.POS.Settle
{
    public partial class SettlePayment : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly Cashier cashier;
        public SettlePayment(Cashier cashier)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            this.cashier = cashier;
        }

        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void button01_Click(object sender, EventArgs e)
        {
            txtCash.Text += button01.Text;
        }

        private void button02_Click(object sender, EventArgs e)
        {
            txtCash.Text += button02.Text;
        }

        private void button03_Click(object sender, EventArgs e)
        {
            txtCash.Text += button03.Text;
        }

        private void button04_Click(object sender, EventArgs e)
        {
            txtCash.Text += button04.Text;
        }

        private void button05_Click(object sender, EventArgs e)
        {
            txtCash.Text += button05.Text;
        }

        private void button06_Click(object sender, EventArgs e)
        {
            txtCash.Text += button06.Text;
        }

        private void button07_Click(object sender, EventArgs e)
        {
            txtCash.Text += button07.Text;
        }

        private void button08_Click(object sender, EventArgs e)
        {
            txtCash.Text += button08.Text;
        }

        private void button09_Click(object sender, EventArgs e)
        {
            txtCash.Text += button09.Text;
        }

        private void button00_Click(object sender, EventArgs e)
        {
            txtCash.Text += button00.Text;
        }

        private void button0_Click(object sender, EventArgs e)
        {
            txtCash.Text += button0.Text;
        }

        private void buttonC_Click(object sender, EventArgs e)
        {
            txtCash.Clear();
            _ = txtCash.Focus();
        }

        private void buttonEnter_Click(object sender, EventArgs e)
        {
            try
            {
                if ((double.Parse(txtChange.Text) < 0) || txtCash.Text.Equals("") || txtCash.Text == string.Empty)
                {
                    _ = MessageBox.Show("Insufficient amount, Please enter the correct amount!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
                else
                {
                    for (int i = 0; i < cashier.dgvCash.Rows.Count; i++)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("UPDATE tblProduct SET qty=qty-" + decimal.Parse(cashier.dgvCash.Rows[i].Cells[5].Value.ToString()) + " WHERE pcode='" + cashier.dgvCash.Rows[i].Cells[2].Value.ToString() + "' ", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();

                        cn.Open();
                        cmd = new SQLiteCommand("UPDATE tblCart SET status='Sold' WHERE id='" + cashier.dgvCash.Rows[i].Cells[1].Value.ToString() + "' ", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                    }
                    Receipt receipt = new Receipt(cashier);
                    receipt.LoadReceipt(txtCash.Text, txtChange.Text, cashier.dgvCash.Rows);
                    _ = Focus();
                    _ = MessageBox.Show("Payment successfully saved!", "Payment success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cashier.GetTransNo();
                    cashier.LoadCart();
                    cashier.btnpayment.BackColor = Color.DarkSlateGray;
                    Dispose();
                }
                cashier.Noti();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void txtCash_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double sale = double.Parse(txtSale.Text);
                double cash = double.Parse(txtCash.Text);
                double change = cash - sale;
                txtChange.Text = change.ToString("#,##0.00");

            }
            catch (Exception)
            {
                txtChange.Text = "0.00";
            }
        }

        private void SettlePayment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (txtCash.Text.Length > 0)
                {
                    txtCash.Clear();
                }
                else
                {
                    Dispose();
                }
            }
            if (e.KeyCode == Keys.Enter)
            {
                buttonEnter.PerformClick();
            }
        }
    }
}
