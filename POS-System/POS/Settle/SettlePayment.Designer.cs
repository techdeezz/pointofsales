﻿namespace POS_System.POS.Settle
{
    partial class SettlePayment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtSale = new System.Windows.Forms.TextBox();
            this.txtCash = new System.Windows.Forms.TextBox();
            this.txtChange = new System.Windows.Forms.TextBox();
            this.button07 = new System.Windows.Forms.Button();
            this.button08 = new System.Windows.Forms.Button();
            this.button09 = new System.Windows.Forms.Button();
            this.buttonC = new System.Windows.Forms.Button();
            this.button04 = new System.Windows.Forms.Button();
            this.button05 = new System.Windows.Forms.Button();
            this.button06 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.button01 = new System.Windows.Forms.Button();
            this.button02 = new System.Windows.Forms.Button();
            this.button03 = new System.Windows.Forms.Button();
            this.button00 = new System.Windows.Forms.Button();
            this.buttonEnter = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtSale
            // 
            this.txtSale.Enabled = false;
            this.txtSale.Font = new System.Drawing.Font("Consolas", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSale.Location = new System.Drawing.Point(25, 18);
            this.txtSale.Multiline = true;
            this.txtSale.Name = "txtSale";
            this.txtSale.ReadOnly = true;
            this.txtSale.Size = new System.Drawing.Size(290, 45);
            this.txtSale.TabIndex = 0;
            this.txtSale.Text = "0.00";
            this.txtSale.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // txtCash
            // 
            this.txtCash.Font = new System.Drawing.Font("Consolas", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCash.Location = new System.Drawing.Point(25, 79);
            this.txtCash.Name = "txtCash";
            this.txtCash.Size = new System.Drawing.Size(290, 48);
            this.txtCash.TabIndex = 0;
            this.txtCash.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txtCash.TextChanged += new System.EventHandler(this.txtCash_TextChanged);
            this.txtCash.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.textBox1_KeyPress);
            // 
            // txtChange
            // 
            this.txtChange.Enabled = false;
            this.txtChange.Font = new System.Drawing.Font("Consolas", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtChange.Location = new System.Drawing.Point(25, 148);
            this.txtChange.Multiline = true;
            this.txtChange.Name = "txtChange";
            this.txtChange.ReadOnly = true;
            this.txtChange.Size = new System.Drawing.Size(290, 45);
            this.txtChange.TabIndex = 0;
            this.txtChange.Text = "0.00";
            this.txtChange.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            // 
            // button07
            // 
            this.button07.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button07.FlatAppearance.BorderSize = 0;
            this.button07.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button07.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button07.ForeColor = System.Drawing.Color.White;
            this.button07.Location = new System.Drawing.Point(25, 221);
            this.button07.Name = "button07";
            this.button07.Size = new System.Drawing.Size(50, 50);
            this.button07.TabIndex = 1;
            this.button07.Text = "7";
            this.button07.UseVisualStyleBackColor = false;
            this.button07.Click += new System.EventHandler(this.button07_Click);
            // 
            // button08
            // 
            this.button08.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button08.FlatAppearance.BorderSize = 0;
            this.button08.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button08.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button08.ForeColor = System.Drawing.Color.White;
            this.button08.Location = new System.Drawing.Point(105, 221);
            this.button08.Name = "button08";
            this.button08.Size = new System.Drawing.Size(50, 50);
            this.button08.TabIndex = 2;
            this.button08.Text = "8";
            this.button08.UseVisualStyleBackColor = false;
            this.button08.Click += new System.EventHandler(this.button08_Click);
            // 
            // button09
            // 
            this.button09.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button09.FlatAppearance.BorderSize = 0;
            this.button09.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button09.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button09.ForeColor = System.Drawing.Color.White;
            this.button09.Location = new System.Drawing.Point(185, 221);
            this.button09.Name = "button09";
            this.button09.Size = new System.Drawing.Size(50, 50);
            this.button09.TabIndex = 3;
            this.button09.Text = "9";
            this.button09.UseVisualStyleBackColor = false;
            this.button09.Click += new System.EventHandler(this.button09_Click);
            // 
            // buttonC
            // 
            this.buttonC.BackColor = System.Drawing.Color.DarkSlateGray;
            this.buttonC.FlatAppearance.BorderSize = 0;
            this.buttonC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonC.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonC.ForeColor = System.Drawing.Color.White;
            this.buttonC.Location = new System.Drawing.Point(265, 221);
            this.buttonC.Name = "buttonC";
            this.buttonC.Size = new System.Drawing.Size(50, 50);
            this.buttonC.TabIndex = 4;
            this.buttonC.Text = "C";
            this.buttonC.UseVisualStyleBackColor = false;
            this.buttonC.Click += new System.EventHandler(this.buttonC_Click);
            // 
            // button04
            // 
            this.button04.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button04.FlatAppearance.BorderSize = 0;
            this.button04.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button04.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button04.ForeColor = System.Drawing.Color.White;
            this.button04.Location = new System.Drawing.Point(25, 296);
            this.button04.Name = "button04";
            this.button04.Size = new System.Drawing.Size(50, 50);
            this.button04.TabIndex = 1;
            this.button04.Text = "4";
            this.button04.UseVisualStyleBackColor = false;
            this.button04.Click += new System.EventHandler(this.button04_Click);
            // 
            // button05
            // 
            this.button05.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button05.FlatAppearance.BorderSize = 0;
            this.button05.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button05.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button05.ForeColor = System.Drawing.Color.White;
            this.button05.Location = new System.Drawing.Point(105, 296);
            this.button05.Name = "button05";
            this.button05.Size = new System.Drawing.Size(50, 50);
            this.button05.TabIndex = 2;
            this.button05.Text = "5";
            this.button05.UseVisualStyleBackColor = false;
            this.button05.Click += new System.EventHandler(this.button05_Click);
            // 
            // button06
            // 
            this.button06.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button06.FlatAppearance.BorderSize = 0;
            this.button06.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button06.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button06.ForeColor = System.Drawing.Color.White;
            this.button06.Location = new System.Drawing.Point(185, 296);
            this.button06.Name = "button06";
            this.button06.Size = new System.Drawing.Size(50, 50);
            this.button06.TabIndex = 3;
            this.button06.Text = "6";
            this.button06.UseVisualStyleBackColor = false;
            this.button06.Click += new System.EventHandler(this.button06_Click);
            // 
            // button0
            // 
            this.button0.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button0.FlatAppearance.BorderSize = 0;
            this.button0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button0.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button0.ForeColor = System.Drawing.Color.White;
            this.button0.Location = new System.Drawing.Point(265, 296);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(50, 50);
            this.button0.TabIndex = 4;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = false;
            this.button0.Click += new System.EventHandler(this.button0_Click);
            // 
            // button01
            // 
            this.button01.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button01.FlatAppearance.BorderSize = 0;
            this.button01.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button01.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button01.ForeColor = System.Drawing.Color.White;
            this.button01.Location = new System.Drawing.Point(25, 371);
            this.button01.Name = "button01";
            this.button01.Size = new System.Drawing.Size(50, 50);
            this.button01.TabIndex = 1;
            this.button01.Text = "1";
            this.button01.UseVisualStyleBackColor = false;
            this.button01.Click += new System.EventHandler(this.button01_Click);
            // 
            // button02
            // 
            this.button02.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button02.FlatAppearance.BorderSize = 0;
            this.button02.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button02.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button02.ForeColor = System.Drawing.Color.White;
            this.button02.Location = new System.Drawing.Point(105, 371);
            this.button02.Name = "button02";
            this.button02.Size = new System.Drawing.Size(50, 50);
            this.button02.TabIndex = 2;
            this.button02.Text = "2";
            this.button02.UseVisualStyleBackColor = false;
            this.button02.Click += new System.EventHandler(this.button02_Click);
            // 
            // button03
            // 
            this.button03.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button03.FlatAppearance.BorderSize = 0;
            this.button03.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button03.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button03.ForeColor = System.Drawing.Color.White;
            this.button03.Location = new System.Drawing.Point(185, 371);
            this.button03.Name = "button03";
            this.button03.Size = new System.Drawing.Size(50, 50);
            this.button03.TabIndex = 3;
            this.button03.Text = "3";
            this.button03.UseVisualStyleBackColor = false;
            this.button03.Click += new System.EventHandler(this.button03_Click);
            // 
            // button00
            // 
            this.button00.BackColor = System.Drawing.Color.DarkSlateGray;
            this.button00.FlatAppearance.BorderSize = 0;
            this.button00.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button00.Font = new System.Drawing.Font("Comic Sans MS", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button00.ForeColor = System.Drawing.Color.White;
            this.button00.Location = new System.Drawing.Point(265, 371);
            this.button00.Name = "button00";
            this.button00.Size = new System.Drawing.Size(50, 50);
            this.button00.TabIndex = 4;
            this.button00.Text = "00";
            this.button00.UseVisualStyleBackColor = false;
            this.button00.Click += new System.EventHandler(this.button00_Click);
            // 
            // buttonEnter
            // 
            this.buttonEnter.BackColor = System.Drawing.Color.DarkSlateGray;
            this.buttonEnter.FlatAppearance.BorderSize = 0;
            this.buttonEnter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.buttonEnter.Font = new System.Drawing.Font("Comic Sans MS", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonEnter.ForeColor = System.Drawing.Color.White;
            this.buttonEnter.Location = new System.Drawing.Point(25, 446);
            this.buttonEnter.Name = "buttonEnter";
            this.buttonEnter.Size = new System.Drawing.Size(290, 50);
            this.buttonEnter.TabIndex = 5;
            this.buttonEnter.Text = "Enter";
            this.buttonEnter.UseVisualStyleBackColor = false;
            this.buttonEnter.Click += new System.EventHandler(this.buttonEnter_Click);
            // 
            // SettlePayment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(340, 509);
            this.Controls.Add(this.buttonEnter);
            this.Controls.Add(this.button00);
            this.Controls.Add(this.button03);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button06);
            this.Controls.Add(this.button02);
            this.Controls.Add(this.buttonC);
            this.Controls.Add(this.button05);
            this.Controls.Add(this.button01);
            this.Controls.Add(this.button09);
            this.Controls.Add(this.button04);
            this.Controls.Add(this.button08);
            this.Controls.Add(this.button07);
            this.Controls.Add(this.txtChange);
            this.Controls.Add(this.txtCash);
            this.Controls.Add(this.txtSale);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettlePayment";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SettlePayment";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.SettlePayment_KeyDown);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button button07;
        private System.Windows.Forms.Button button08;
        private System.Windows.Forms.Button button09;
        private System.Windows.Forms.Button buttonC;
        private System.Windows.Forms.Button button04;
        private System.Windows.Forms.Button button05;
        private System.Windows.Forms.Button button06;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button button01;
        private System.Windows.Forms.Button button02;
        private System.Windows.Forms.Button button03;
        private System.Windows.Forms.Button button00;
        private System.Windows.Forms.Button buttonEnter;
        public System.Windows.Forms.TextBox txtSale;
        public System.Windows.Forms.TextBox txtCash;
        public System.Windows.Forms.TextBox txtChange;
    }
}