﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.POS.Lookup
{
    public partial class LookupProducts : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly MainForm mf = new MainForm();
        private string _pcode;
        private string _price;
        private string _qty;
        private readonly Cashier cashier;

        public LookupProducts(Cashier cashier)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadProductData();
            this.cashier = cashier;
            dgvProduct.Columns[9].Width = 40;
        }
        public void LoadProductData()
        {
            int i = 0;
            dgvProduct.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT p.pcode, p.barcode, p.pdesc, b.brand, c.category, p.price, p.qty,p.unit FROM tblProduct AS p INNER JOIN tblBrand AS b ON b.id=p.bid INNER JOIN tblCategory AS c ON c.id=p.cid WHERE isactive = 1 AND p.pdesc || b.brand || c.category || p.pcode || p.barcode LIKE '%" + txtSearch.Text + "%'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvProduct.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), double.Parse(dr[5].ToString()).ToString("0.00"), dr[6].ToString(), dr[7].ToString());
            }
            dr.Close();
            cn.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }
        public void CellSelect(string pcode, string price, string quantity)
        {
            Quantity qty = new Quantity(cashier);
            qty.ProductDetails(pcode, double.Parse(price), cashier.lbltransno.Text, decimal.Parse(quantity));
            _ = qty.ShowDialog();
        }

        private void dgvProduct_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvProduct.Columns[e.ColumnIndex].Name;
            if (colName == "Select")
            {
                CellSelect(dgvProduct.Rows[e.RowIndex].Cells[1].Value.ToString(), dgvProduct.Rows[e.RowIndex].Cells[6].Value.ToString(), dgvProduct.Rows[e.RowIndex].Cells[7].Value.ToString());
            }
            else
            {
                return;
            }
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadProductData();
        }

        private void dgvProduct_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvProduct.CurrentRow.Index;
            _pcode = dgvProduct[1, i].Value.ToString();
            _price = dgvProduct[6, i].Value.ToString();
            _qty = dgvProduct[7, i].Value.ToString();
        }

        private void dgvProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CellSelect(_pcode, _price, _qty);
            }
        }
        private void LookupProducts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Escape")  // Escape to close module box
            {
                if (txtSearch.Text.Length == 0)
                {
                    btnClose.PerformClick();
                }
                else
                {
                    txtSearch.Clear();
                    _ = txtSearch.Focus();
                }
            }
            if (e.KeyCode.ToString() == "F8")  // F8 to focus search box
            {
                _ = txtSearch.Focus();
            }
        }

        private void LookupProducts_FormClosing(object sender, FormClosingEventArgs e)
        {
            _ = cashier.dgvCash.Focus();
        }

        private void dgvProduct_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CellSelect(dgvProduct.Rows[e.RowIndex].Cells[1].Value.ToString(), dgvProduct.Rows[e.RowIndex].Cells[6].Value.ToString(), dgvProduct.Rows[e.RowIndex].Cells[7].Value.ToString());
        }
    }
}
