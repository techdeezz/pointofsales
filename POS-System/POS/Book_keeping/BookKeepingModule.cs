﻿using POS_System.POS.PaymentReceipt;
using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;
using TextBox = System.Windows.Forms.TextBox;

namespace POS_System.POS.Book_keeping
{
    public partial class BookKeepingModule : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly string sTitle = "Point Of Sale";
        private Point mouseOffset;
        private readonly Cashier cashier;
        public BookKeepingModule(Cashier cashier)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadCustomersy();
            cmbCustomer.SelectedIndex = -1;
            this.cashier = cashier;
        }

        public void LoadCustomersy()
        {
            cmbCustomer.Items.Clear();
            cmbCustomer.DataSource = dbCon.getTable("SELECT * FROM tblCustomer ORDER BY customer");
            cmbCustomer.DisplayMember = "customer";
            cmbCustomer.ValueMember = "id";
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void SupplierModule_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel.PerformClick();
            }
        }

        private void cmbCustomer_TextChanged(object sender, EventArgs e)
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblCustomer WHERE customer LIKE '" + cmbCustomer.Text + "'", cn);
            dr = cmd.ExecuteReader();
            _ = dr.Read();
            if (dr.HasRows)
            {
                lblId.Text = dr["id"].ToString();
                txtAddress.Text = dr["address"].ToString();
                txtEmail.Text = dr["email"].ToString();
                txtPhone.Text = dr["phone"].ToString();
                txtNic.Text = dr["nic"].ToString();
            }
            dr.Close();
            cn.Close();
        }

        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            if (cmbCustomer.SelectedItem != null)
            {
                if (MessageBox.Show("Are you sure want to save this record?", "Insert Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    for (int i = 0; i < cashier.dgvCash.Rows.Count; i++)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("UPDATE tblProduct SET qty=qty-" + decimal.Parse(cashier.dgvCash.Rows[i].Cells[5].Value.ToString()) + " WHERE pcode='" + cashier.dgvCash.Rows[i].Cells[2].Value.ToString() + "' ", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();

                        cn.Open();
                        cmd = new SQLiteCommand("UPDATE tblCart SET status='Sold',isbook_keeping=1,customerid='" + lblId.Text + "' WHERE id='" + cashier.dgvCash.Rows[i].Cells[1].Value.ToString() + "' ", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                    }
                    cn.Open();
                    cmd = new SQLiteCommand("INSERT INTO tblBookKeepinPayments(cutomerid,paymentdate,paidamount)VALUES(@cutomerid,@paymentdate,@paidamount)", cn);
                    _ = cmd.Parameters.AddWithValue("@cutomerid", lblId.Text);
                    _ = cmd.Parameters.AddWithValue("@paymentdate", DateTime.Now);
                    _ = cmd.Parameters.AddWithValue("@paidamount", txtadvance.Text);

                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                    BKReceipt receipt = new BKReceipt(cashier);
                    receipt.LoadBKReceipt(txtadvance.Text, cmbCustomer.Text);
                    _ = Focus();
                    _ = MessageBox.Show("Payment successfully saved!", "Payment success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cashier.GetTransNo();
                    cashier.LoadCart();
                    cashier.btnbookkeeping.BackColor = Color.DarkSlateGray;
                    _ = cashier.dgvCash.Focus();
                    Dispose();
                }
                cashier.Noti();
            }
            else
            {
                _ = MessageBox.Show("Please select a customer", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                _ = cmbCustomer.Focus();
            }
        }

        private void BookKeepingModule_Load(object sender, EventArgs e)
        {
            cmbCustomer.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbCustomer.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        private void txtadvance_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAdd.PerformClick();
            }
        }
        private bool txtadvanceJustEntered = false;
        private void txtadvance_Enter(object sender, EventArgs e)
        {
            txtadvance.SelectAll();
            txtadvanceJustEntered = true;
        }

        private void txtadvance_Click(object sender, EventArgs e)
        {
            if (txtadvanceJustEntered)
            {
                txtadvance.SelectAll();
            }
            txtadvanceJustEntered = false;
        }
    }
}
