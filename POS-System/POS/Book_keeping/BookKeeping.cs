﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.POS.Book_keeping
{
    public partial class BookKeeping : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private string id;
        public BookKeeping()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadBookKeepingData();
            dgvBookKeeping.Columns[8].Width = 25;
            dgvBookKeeping.Columns[9].Width = 25;
            dgvBookKeeping.Columns[10].Width = 25;
        }

        public void LoadBookKeepingData()
        {
            int i = 0;
            dgvBookKeeping.Rows.Clear();
            cn.Open();
            string query1 = "SUM(CART.subtotal)";
            string query2 = "(SELECT SUM(paidamount) FROM tblBookKeepinPayments AS BK WHERE BK.cutomerid = CART.customerid GROUP BY BK.cutomerid)";
            string query3 = "SUM(CART.subtotal) - (SELECT SUM(paidamount) FROM tblBookKeepinPayments AS BK WHERE BK.cutomerid = CART.customerid GROUP BY BK.cutomerid)";
            cmd = new SQLiteCommand("SELECT CART.customerid, CUS.customer,CUS.email,CUS.phone," + query1 + "," + query2 + "," + query3 + "   FROM tblCart AS CART INNER JOIN tblCustomer AS CUS ON CART.customerid = CUS.id WHERE CART.isbook_keeping = 1 AND CART.customerid IS NOT NULL AND CUS.customer LIKE '%" + txtSearch.Text + "%' GROUP BY CART.customerid", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvBookKeeping.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), double.Parse(dr[4].ToString()).ToString("0.00"), double.Parse(dr[5].ToString()).ToString("0.00"), double.Parse(dr[6].ToString()).ToString("0.00"));
            }
            dr.Close();
            cn.Close();
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                txtSearch.Clear();
                _ = txtSearch.Focus();
            }
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadBookKeepingData();
        }

        private void dgvSupplier_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvBookKeeping.Columns[e.ColumnIndex].Name;
            if (colName == "ViewOrder")
            {
                BookKeepingOrders book = new BookKeepingOrders
                {
                    customerId = Convert.ToInt32(dgvBookKeeping.Rows[e.RowIndex].Cells[1].Value.ToString())
                };
                book.LoadData(Convert.ToInt32(dgvBookKeeping.Rows[e.RowIndex].Cells[1].Value.ToString()));
                _ = book.ShowDialog();
            }
            else if (colName == "PaymentDetails")
            {
                BookKeepingPayment bookPay = new BookKeepingPayment
                {
                    CustomerId = Convert.ToInt32(dgvBookKeeping.Rows[e.RowIndex].Cells[1].Value.ToString())
                };
                bookPay.LoadPaymentData(Convert.ToInt32(dgvBookKeeping.Rows[e.RowIndex].Cells[1].Value.ToString()));
                _ = bookPay.ShowDialog();
            }
            else if (colName == "AddPayments")
            {
                AddPayment pay = new AddPayment();
                pay.lblId.Text = dgvBookKeeping.Rows[e.RowIndex].Cells[1].Value.ToString();
                _ = pay.ShowDialog();
            }
            else
            {
                return;
            }
            LoadBookKeepingData();
        }

        private void dgvSupplier_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvBookKeeping.CurrentRow.Index;
            id = dgvBookKeeping[1, i].Value.ToString();
        }

        private void Suppliers_Load(object sender, EventArgs e)
        {
            dgvBookKeeping.ClearSelection();
        }

        private void Suppliers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F8")  // F8 to focus search box
            {
                _ = txtSearch.Focus();
            }
        }

        private void dgvBookKeeping_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            BookKeepingOrders book = new BookKeepingOrders
            {
                customerId = Convert.ToInt32(dgvBookKeeping.Rows[e.RowIndex].Cells[1].Value.ToString())
            };
            book.LoadData(Convert.ToInt32(dgvBookKeeping.Rows[e.RowIndex].Cells[1].Value.ToString()));
            _ = book.ShowDialog();
        }
    }
}
