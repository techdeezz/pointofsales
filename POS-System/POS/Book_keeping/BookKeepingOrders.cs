﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.POS.Book_keeping
{
    public partial class BookKeepingOrders : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        public int customerId;

        public BookKeepingOrders()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
        }
        public void LoadData(int cusId)
        {
            int i = 0;
            dgvBookKeepingOrder.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT p.pcode, p.pdesc,c.sdate, c.price, c.qty, p.unit, c.disc, c.subtotal FROM tblCart AS c INNER JOIN tblProduct AS p ON c.pcode = p.pcode WHERE c.isbook_keeping = 1 AND c.customerid = " + cusId + " AND p.pdesc || p.pcode LIKE '%" + txtSearch.Text + "%'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvBookKeepingOrder.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), DateTime.Parse(dr[2].ToString()).ToShortDateString(), double.Parse(dr[3].ToString()).ToString("0.00"), dr[4].ToString(), dr[5].ToString(), double.Parse(dr[6].ToString()).ToString("0.00"), double.Parse(dr[7].ToString()).ToString("0.00"));
            }
            dr.Close();
            cn.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadData(customerId);
        }

        private void LookupProducts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Escape")  // Escape to close module box
            {
                if (txtSearch.Text.Length == 0)
                {
                    btnClose.PerformClick();
                }
                else
                {
                    txtSearch.Clear();
                    _ = txtSearch.Focus();
                }
            }
            if (e.KeyCode.ToString() == "F8")  // F8 to focus search box
            {
                _ = txtSearch.Focus();
            }
        }

        private void LookupProducts_Load(object sender, EventArgs e)
        {
            dgvBookKeepingOrder.ClearSelection();
        }
    }
}
