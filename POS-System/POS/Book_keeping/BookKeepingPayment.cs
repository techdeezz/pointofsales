﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.POS.Book_keeping
{
    public partial class BookKeepingPayment : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        public int CustomerId;

        public BookKeepingPayment()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            dgvBookKeepingPayment.ClearSelection();
        }
        public void LoadPaymentData(int CustId)
        {
            int i = 0;
            dgvBookKeepingPayment.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT paidamount, paymentdate FROM tblBookKeepinPayments WHERE cutomerid = " + CustId + "", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvBookKeepingPayment.Rows.Add(i, dr[0].ToString(), DateTime.Parse(dr[1].ToString()).ToString("dddd, MMMM d, yyyy hh:mm tt"));
            }
            dr.Close();
            cn.Close();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void LookupProducts_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Escape")  // Escape to close module box
            {
                btnClose.PerformClick();
            }
        }
    }
}
