﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System.POS.Book_keeping
{
    public partial class AddPayment : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly string sTitle = "Point Of Sale";
        private Point mouseOffset;
        public AddPayment()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            _ = txtAmount.Focus();
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtAmount.Text) && txtAmount.Text != "0.00")
                {

                    if (MessageBox.Show("Are you sure want to save this record?", "Insert Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("INSERT INTO tblBookKeepinPayments(cutomerid,paymentdate,paidamount)VALUES(@cutomerid,@paymentdate,@paidamount)", cn);

                        _ = cmd.Parameters.AddWithValue("@cutomerid", lblId.Text);
                        _ = cmd.Parameters.AddWithValue("@paymentdate", dtPayment.Value);
                        _ = cmd.Parameters.AddWithValue("@paidamount", txtAmount.Text);

                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully saved", sTitle);
                        Dispose();
                    }

                }
                else
                {
                    _ = MessageBox.Show("Please enter valid amount", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _ = txtAmount.Focus();
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }


        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void SupplierModule_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel.PerformClick();
            }
        }

        private void txtAmount_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && e.KeyChar != 46)
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)13)
            {
                btnSave.PerformClick();
            }
            if (e.KeyChar == 46)
            {
                if ((sender as TextBox).Text.IndexOf(e.KeyChar) != -1)
                {
                    e.Handled = true;
                }
            }
        }
    }
}
