﻿namespace POS_System.POS.Settings
{
    partial class ChangePassword
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ChangePassword));
            this.panel1 = new System.Windows.Forms.Panel();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblusername = new System.Windows.Forms.Label();
            this.btnNext = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.txtCurrentPass = new MetroFramework.Controls.MetroTextBox();
            this.txtConfirmPass = new MetroFramework.Controls.MetroTextBox();
            this.btnSave = new System.Windows.Forms.Button();
            this.txtnewpass = new MetroFramework.Controls.MetroTextBox();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.picClose);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(440, 50);
            this.panel1.TabIndex = 2;
            this.panel1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseDown);
            this.panel1.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseMove);
            this.panel1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.panel1_MouseUp);
            // 
            // picClose
            // 
            this.picClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picClose.Image = ((System.Drawing.Image)(resources.GetObject("picClose.Image")));
            this.picClose.Location = new System.Drawing.Point(412, 0);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(28, 28);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picClose.TabIndex = 1;
            this.picClose.TabStop = false;
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(148, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Change Password";
            // 
            // lblusername
            // 
            this.lblusername.AutoSize = true;
            this.lblusername.Font = new System.Drawing.Font("Consolas", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblusername.Location = new System.Drawing.Point(104, 94);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(106, 24);
            this.lblusername.TabIndex = 5;
            this.lblusername.Text = "Username";
            // 
            // btnNext
            // 
            this.btnNext.BackColor = System.Drawing.Color.Blue;
            this.btnNext.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNext.FlatAppearance.BorderSize = 0;
            this.btnNext.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNext.ForeColor = System.Drawing.Color.White;
            this.btnNext.Location = new System.Drawing.Point(290, 183);
            this.btnNext.Name = "btnNext";
            this.btnNext.Size = new System.Drawing.Size(108, 31);
            this.btnNext.TabIndex = 6;
            this.btnNext.Text = "Next";
            this.btnNext.UseVisualStyleBackColor = false;
            this.btnNext.Click += new System.EventHandler(this.btnNext_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::POS_System.Properties.Resources.checked_user_male_50px;
            this.pictureBox1.Location = new System.Drawing.Point(46, 68);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 50);
            this.pictureBox1.TabIndex = 4;
            this.pictureBox1.TabStop = false;
            // 
            // txtCurrentPass
            // 
            // 
            // 
            // 
            this.txtCurrentPass.CustomButton.Image = null;
            this.txtCurrentPass.CustomButton.Location = new System.Drawing.Point(324, 1);
            this.txtCurrentPass.CustomButton.Name = "";
            this.txtCurrentPass.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtCurrentPass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtCurrentPass.CustomButton.TabIndex = 1;
            this.txtCurrentPass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtCurrentPass.CustomButton.UseSelectable = true;
            this.txtCurrentPass.CustomButton.Visible = false;
            this.txtCurrentPass.DisplayIcon = true;
            this.txtCurrentPass.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtCurrentPass.Icon = global::POS_System.Properties.Resources.key_22px;
            this.txtCurrentPass.Lines = new string[0];
            this.txtCurrentPass.Location = new System.Drawing.Point(46, 138);
            this.txtCurrentPass.MaxLength = 32767;
            this.txtCurrentPass.Name = "txtCurrentPass";
            this.txtCurrentPass.PasswordChar = '●';
            this.txtCurrentPass.PromptText = "Current Password";
            this.txtCurrentPass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtCurrentPass.SelectedText = "";
            this.txtCurrentPass.SelectionLength = 0;
            this.txtCurrentPass.SelectionStart = 0;
            this.txtCurrentPass.ShortcutsEnabled = true;
            this.txtCurrentPass.Size = new System.Drawing.Size(352, 29);
            this.txtCurrentPass.TabIndex = 3;
            this.txtCurrentPass.UseSelectable = true;
            this.txtCurrentPass.UseSystemPasswordChar = true;
            this.txtCurrentPass.WaterMark = "Current Password";
            this.txtCurrentPass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtCurrentPass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtCurrentPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtCurrentPass_KeyDown);
            // 
            // txtConfirmPass
            // 
            // 
            // 
            // 
            this.txtConfirmPass.CustomButton.Image = null;
            this.txtConfirmPass.CustomButton.Location = new System.Drawing.Point(324, 1);
            this.txtConfirmPass.CustomButton.Name = "";
            this.txtConfirmPass.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtConfirmPass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtConfirmPass.CustomButton.TabIndex = 1;
            this.txtConfirmPass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtConfirmPass.CustomButton.UseSelectable = true;
            this.txtConfirmPass.CustomButton.Visible = false;
            this.txtConfirmPass.DisplayIcon = true;
            this.txtConfirmPass.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtConfirmPass.Icon = global::POS_System.Properties.Resources.key_22px;
            this.txtConfirmPass.Lines = new string[0];
            this.txtConfirmPass.Location = new System.Drawing.Point(46, 185);
            this.txtConfirmPass.MaxLength = 32767;
            this.txtConfirmPass.Name = "txtConfirmPass";
            this.txtConfirmPass.PasswordChar = '●';
            this.txtConfirmPass.PromptText = "Confirm Password";
            this.txtConfirmPass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtConfirmPass.SelectedText = "";
            this.txtConfirmPass.SelectionLength = 0;
            this.txtConfirmPass.SelectionStart = 0;
            this.txtConfirmPass.ShortcutsEnabled = true;
            this.txtConfirmPass.Size = new System.Drawing.Size(352, 29);
            this.txtConfirmPass.TabIndex = 3;
            this.txtConfirmPass.UseSelectable = true;
            this.txtConfirmPass.UseSystemPasswordChar = true;
            this.txtConfirmPass.Visible = false;
            this.txtConfirmPass.WaterMark = "Confirm Password";
            this.txtConfirmPass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtConfirmPass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            this.txtConfirmPass.KeyDown += new System.Windows.Forms.KeyEventHandler(this.txtConfirmPass_KeyDown);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.Blue;
            this.btnSave.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSave.FlatAppearance.BorderSize = 0;
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(290, 232);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(108, 31);
            this.btnSave.TabIndex = 7;
            this.btnSave.Text = "Save";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Visible = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // txtnewpass
            // 
            // 
            // 
            // 
            this.txtnewpass.CustomButton.Image = null;
            this.txtnewpass.CustomButton.Location = new System.Drawing.Point(324, 1);
            this.txtnewpass.CustomButton.Name = "";
            this.txtnewpass.CustomButton.Size = new System.Drawing.Size(27, 27);
            this.txtnewpass.CustomButton.Style = MetroFramework.MetroColorStyle.Blue;
            this.txtnewpass.CustomButton.TabIndex = 1;
            this.txtnewpass.CustomButton.Theme = MetroFramework.MetroThemeStyle.Light;
            this.txtnewpass.CustomButton.UseSelectable = true;
            this.txtnewpass.CustomButton.Visible = false;
            this.txtnewpass.DisplayIcon = true;
            this.txtnewpass.FontSize = MetroFramework.MetroTextBoxSize.Medium;
            this.txtnewpass.Icon = global::POS_System.Properties.Resources.key_22px;
            this.txtnewpass.Lines = new string[0];
            this.txtnewpass.Location = new System.Drawing.Point(46, 138);
            this.txtnewpass.MaxLength = 32767;
            this.txtnewpass.Name = "txtnewpass";
            this.txtnewpass.PasswordChar = '●';
            this.txtnewpass.PromptText = "New Password";
            this.txtnewpass.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.txtnewpass.SelectedText = "";
            this.txtnewpass.SelectionLength = 0;
            this.txtnewpass.SelectionStart = 0;
            this.txtnewpass.ShortcutsEnabled = true;
            this.txtnewpass.Size = new System.Drawing.Size(352, 29);
            this.txtnewpass.TabIndex = 8;
            this.txtnewpass.UseSelectable = true;
            this.txtnewpass.UseSystemPasswordChar = true;
            this.txtnewpass.Visible = false;
            this.txtnewpass.WaterMark = "New Password";
            this.txtnewpass.WaterMarkColor = System.Drawing.Color.FromArgb(((int)(((byte)(109)))), ((int)(((byte)(109)))), ((int)(((byte)(109)))));
            this.txtnewpass.WaterMarkFont = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Pixel);
            // 
            // ChangePassword
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(440, 285);
            this.Controls.Add(this.txtnewpass);
            this.Controls.Add(this.btnNext);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.lblusername);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.txtConfirmPass);
            this.Controls.Add(this.txtCurrentPass);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "ChangePassword";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Change Password";
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.ChangePassword_KeyDown);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox picClose;
        private System.Windows.Forms.Label label1;
        private MetroFramework.Controls.MetroTextBox txtCurrentPass;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lblusername;
        public System.Windows.Forms.Button btnNext;
        private MetroFramework.Controls.MetroTextBox txtConfirmPass;
        public System.Windows.Forms.Button btnSave;
        private MetroFramework.Controls.MetroTextBox txtnewpass;
    }
}