﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System.POS.Settings
{
    public partial class ChangePassword : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private readonly SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly Cashier cashier;
        private Point mouseOffset;
        public ChangePassword(Cashier cashier)
        {
            InitializeComponent();
            this.cashier = cashier;
            lblusername.Text = cashier.lblusername.Text;

            _ = txtCurrentPass.Focus();
            txtCurrentPass.Select();
            Size = new Size(440, 230);
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnNext_Click(object sender, EventArgs e)
        {
            try
            {
                string oldPass = dbCon.getPassword(lblusername.Text.ToUpper());
                if (oldPass != txtCurrentPass.Text)
                {
                    _ = MessageBox.Show("Wrong password please try again!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    _ = txtCurrentPass.Focus();
                    return;
                }
                else
                {
                    txtCurrentPass.Visible = false;
                    btnNext.Visible = false;
                    txtnewpass.Visible = true;
                    txtConfirmPass.Visible = true;
                    btnSave.Visible = true;
                    _ = txtnewpass.Focus();
                    txtnewpass.Select();
                    Size = new Size(440, 285);
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtnewpass.Text != txtConfirmPass.Text)
                {
                    _ = MessageBox.Show("New password and confirm password didn't match please try again!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    _ = txtnewpass.Focus();
                }
                else
                {
                    if (MessageBox.Show("Are you sure to change password?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        dbCon.ExecuteQuery("UPDATE tblUser set password = '" + txtnewpass.Text + "' WHERE username='" + lblusername.Text.ToUpper() + "'");
                        _ = MessageBox.Show("Password has been successfully updated", "Update bpassword", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtCurrentPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnNext.PerformClick();
            }
        }

        private void txtConfirmPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.PerformClick();
            }
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void ChangePassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Dispose();
            }
        }
    }
}
