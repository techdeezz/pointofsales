﻿using POS_System.Common;
using POS_System.LoginModule;
using POS_System.POS.Book_keeping;
using POS_System.POS.Lookup;
using POS_System.POS.Report;
using POS_System.POS.Settings;
using POS_System.POS.Settle;
using System;
using System.Data.SQLite;
using System.Drawing;
using System.Threading.Tasks;
using System.Windows.Forms;
using USB_Barcode_Scanner;

namespace POS_System.POS
{
    public partial class Cashier : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly string sTitle = "Point Of Sale";

        private decimal qty;
        private string id;
        private string price;
        private string pcode;
        private int selectedRowIndex;
        private int sri;
        public Cashier()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            WindowState = FormWindowState.Maximized;
            dgvCash.Columns[9].Width = 25;
            dgvCash.Columns[10].Width = 25;
            dgvCash.Columns[11].Width = 25;
            GetTransNo();
            lbldate.Text = DateTime.Now.ToShortDateString();
            btnsearchprd.BackColor = Color.DarkSlateGray;
            BarcodeScanner barcodescanner = new BarcodeScanner(this);
            barcodescanner.BarcodeScanned += BarcodeScanner_BarcodeScanned;
            Noti();
        }
        public void Noti()
        {
            MainForm main = new MainForm();
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM vwCriticalItems", cn);

            object Exist = query.ExecuteScalar();
            cn.Close();
            if (Convert.ToInt32(Exist) > 0)
            {
                //NotificationSingle noti = new NotificationSingle(main);
                //noti.ShowAlert(Exist + " notifications received");
                pictureBox3.Image = POS_System.Properties.Resources.notification_receive;
            }
            else
            {
                pictureBox3.Image = POS_System.Properties.Resources.notification;
            }
        }

        private void picClose_Click(object sender, System.EventArgs e)
        {
            if (label3.Text == "Administrator")
            {
                if (dgvCash.Rows.Count > 0)
                {
                    if (MessageBox.Show("Are you sure to clear the cart?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        if (MessageBox.Show("Are you sure to exist the application?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            ClearCart();
                            Close();
                        }
                    }
                }
                else
                {
                    if (MessageBox.Show("Are you sure to exist the application?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Close();
                    }
                }
            }
            else
            {
                if (dgvCash.Rows.Count > 0)
                {
                    if (MessageBox.Show("Are you sure to clear the cart?", "Confirmation", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning) == DialogResult.OK)
                    {
                        if (MessageBox.Show("Are you sure to exist the application?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            ClearCart();
                            Application.Exit();
                        }
                    }
                }
                else
                {
                    if (MessageBox.Show("Are you sure to exist the application?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        Application.Exit();
                    }
                }
            }
        }
        #region Button
        public void slide(Button button)
        {
            btnsearchprd.BackColor = Color.DarkSlateGray;
            btndiscount.BackColor = Color.DarkSlateGray;
            btnpayment.BackColor = Color.DarkSlateGray;
            btnclearcart.BackColor = Color.DarkSlateGray;
            btnsales.BackColor = Color.DarkSlateGray;
            btnchangepassword.BackColor = Color.DarkSlateGray;
            btnlogout.BackColor = Color.DarkSlateGray;
            button.BackColor = Color.FromArgb(43, 57, 79);

        }

        private void btnsearchprd_Click(object sender, System.EventArgs e)
        {
            slide(btnsearchprd);
            LookupProducts lookup = new LookupProducts(this);
            lookup.LoadProductData();
            lookup.dgvProduct.Select();
            _ = lookup.ShowDialog();
        }

        private void btndiscount_Click(object sender, System.EventArgs e)
        {
            slide(btndiscount);
            Discount disc = new Discount(this);
            disc.lblid.Text = id;
            disc.txttotalprice.Text = price;
            _ = disc.ShowDialog();
        }

        private void btnpayment_Click(object sender, System.EventArgs e)
        {
            slide(btnpayment);
            SettlePayment settle = new SettlePayment(this);
            settle.txtSale.Text = lblDisplayTotal.Text;
            _ = settle.ShowDialog();
        }
        public void ClearCart()
        {
            cn.Open();
            cmd = new SQLiteCommand("DELETE FROM tblCart WHERE transno LIKE '" + lbltransno.Text + "'", cn);
            _ = cmd.ExecuteNonQuery();
            cn.Close();

        }

        private void btnclearcart_Click(object sender, System.EventArgs e)
        {
            slide(btnclearcart);
            if (MessageBox.Show("Remove all items from cart?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                ClearCart();
                _ = MessageBox.Show("Cart has been successfully cleared", "Point Of Sale", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadCart();
            }
        }

        private void btnsales_Click(object sender, System.EventArgs e)
        {
            slide(btnsales);
            DailySales ds = new DailySales(this)
            {
                soldUser = lblusername.Text
            };
            ds.dtFrom.Enabled = false;
            ds.dtTo.Enabled = false;
            ds.cmbCashier.Enabled = false;
            ds.txtSearch.Enabled = false;
            ds.cmbCashier.Text = label3.Text == "Cashier" ? lblusername.Text : "All Cashier";
            _ = ds.ShowDialog();
        }

        private void btnchangepassword_Click(object sender, System.EventArgs e)
        {
            slide(btnchangepassword);
            ChangePassword chng = new ChangePassword(this);
            _ = chng.ShowDialog();
        }

        private void btnlogout_Click(object sender, System.EventArgs e)
        {
            slide(btnlogout);
            if (dgvCash.Rows.Count > 0)
            {
                _ = MessageBox.Show("Unable to logout Please cancell all transaction and continue!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
            else
            {
                if (MessageBox.Show("Are sure to logout?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    Hide();
                    Login login = new Login();
                    _ = login.ShowDialog();
                }
                else
                {
                    btnlogout.BackColor = Color.DarkSlateGray;
                }
            }
        }

        #endregion

        public void LoadCart()
        {
            try
            {
                bool hascart = false;
                int i = 0;
                double total = 0;
                double discount = 0;
                int pc = selectedRowIndex;

                dgvCash.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT c.id, c.pcode, p.pdesc, c.qty, c.price, c.disc, c.subtotal,c.total,p.unit FROM tblCart AS c INNER JOIN tblProduct AS p ON c.pcode=p.pcode WHERE c.transno LIKE @transno AND c.status LIKE 'Pending'", cn);
                _ = cmd.Parameters.AddWithValue("@transno", lbltransno.Text);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    total += Convert.ToDouble(dr["total"].ToString());
                    discount += Convert.ToDouble(dr["disc"].ToString());
                    _ = dgvCash.Rows.Add(i, dr["id"].ToString(), dr["pcode"].ToString(), dr["pdesc"].ToString(), double.Parse(dr["price"].ToString()).ToString("0.00"), dr["qty"].ToString(), dr["unit"].ToString(), dr["disc"].ToString(), double.Parse(dr["subtotal"].ToString()).ToString("#,##0.00"));
                    hascart = true;
                }
                dr.Close();
                cn.Close();
                lblsalestotal.Text = total.ToString("#,##0.00");
                lbldiscount.Text = discount.ToString("#,##0.00");
                GetCartTotal();
                if (hascart)
                {
                    btnclearcart.Enabled = true;
                    btnpayment.Enabled = true;
                    btndiscount.Enabled = true;
                    btnbookkeeping.Enabled = true;
                }
                else
                {
                    btnclearcart.Enabled = false;
                    btnpayment.Enabled = false;
                    btndiscount.Enabled = false;
                    btnbookkeeping.Enabled = false;
                }
                sri = pc;
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, sTitle);
            }
        }

        public void GetCartTotal()
        {
            double discount = double.Parse(lbldiscount.Text);
            double sales = double.Parse(lblsalestotal.Text) - discount;
            lblDisplayTotal.Text = sales.ToString("#,##0.00");
        }

        private void timer1_Tick(object sender, System.EventArgs e)
        {
            lbltimer.Text = DateTime.Now.ToString("dddd, dd MMMM yyyy hh:mm:ss tt");
        }
        public void GetTransNo()
        {
            try
            {
                string sdate = DateTime.Now.ToString("yyyyMMdd");
                int count;
                string transNo;
                cn.Open();
                cmd = new SQLiteCommand("SELECT transno FROM tblCart WHERE transno LIKE '" + sdate + "%' ORDER BY id desc LIMIT 1", cn);
                dr = cmd.ExecuteReader();
                _ = dr.Read();
                if (dr.HasRows)
                {
                    transNo = dr[0].ToString();
                    count = int.Parse(transNo.Substring(8, 4));
                    lbltransno.Text = sdate + (count + 1);
                }
                else
                {
                    transNo = sdate + "1001";
                    lbltransno.Text = transNo;
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                cn.Close();
                _ = MessageBox.Show(ex.Message, sTitle);
            }
        }
        private void picMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private DateTime lastPressTime;
        private void Cashier_KeyDown(object sender, KeyEventArgs e)
        {
            DateTime pressTime = DateTime.Now;
            if (e.KeyCode.ToString() == "F1")
            {
                btnsearchprd.PerformClick();
            }
            if (e.KeyCode.ToString() == "F2")
            {
                btndiscount.PerformClick();
            }
            if (e.KeyCode.ToString() == "F3")
            {
                btnpayment.PerformClick();
            }
            if (e.KeyCode.ToString() == "F4")
            {
                btnbookkeeping.PerformClick();
            }
            if (e.KeyCode.ToString() == "F5")
            {
                btnclearcart.PerformClick();
            }
            if (e.KeyCode.ToString() == "F6")
            {
                btnsales.PerformClick();
            }
            if (e.KeyCode.ToString() == "F8")
            {
                _ = txtBarcode.Focus();
            }
            if (e.KeyCode.ToString() == "Escape")
            {
                _ = dgvCash.Focus();
            }
            if (e.KeyCode == Keys.Space)
            {
                if (pressTime.Subtract(lastPressTime).TotalMilliseconds < 500)
                {
                    _ = txtBarcode.Focus();
                }
            }
            lastPressTime = pressTime;
        }

        private void dgvCash_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete")
            {
                if (id != null)
                {
                    if (MessageBox.Show("Are you sure to remove the item from cart?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        dbCon.ExecuteQuery("DELETE FROM tblCart WHERE id LIKE '" + id.ToString() + "'");
                        LoadCart();
                    }
                }
            }
            if (e.KeyCode == Keys.Oemplus || e.KeyCode == Keys.Add)
            {
                cn.Open();
                cmd = new SQLiteCommand("SELECT SUM(qty) as qty FROM tblProduct WHERE pcode LIKE '" + pcode.ToString() + "' GROUP BY pcode", cn);
                decimal i = decimal.Parse(cmd.ExecuteScalar().ToString());
                cn.Close();
                if (qty < i)
                {
                    dbCon.ExecuteQuery("UPDATE tblCart SET qty = qty + 1 WHERE transno LIKE '" + lbltransno.Text + "' AND pcode LIKE '" + pcode.ToString() + "'");
                    LoadCart();
                    if (sri == 0)
                    {
                        dgvCash.CurrentCell = dgvCash.Rows[0].Cells[0];
                        dgvCash.Rows[0].Selected = true;
                    }
                    else
                    {
                        dgvCash.ClearSelection();
                        dgvCash.CurrentCell = dgvCash.Rows[sri].Cells[0];
                        dgvCash.Rows[sri].Selected = true;
                    }
                }
                else
                {
                    _ = MessageBox.Show("Unable to proceed. Remaining quantity on hand is " + i, "Out of Stock", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            if (e.KeyCode == Keys.OemMinus || e.KeyCode == Keys.Subtract)
            {
                cn.Open();
                cmd = new SQLiteCommand("SELECT SUM(qty) as qty FROM tblCart WHERE pcode LIKE '" + pcode.ToString() + "' AND status LIKE 'Pending' GROUP BY pcode", cn);
                decimal i = decimal.Parse(cmd.ExecuteScalar().ToString());
                cn.Close();
                if (i > 1)
                {
                    dbCon.ExecuteQuery("UPDATE tblCart SET qty = qty - 1 WHERE transno LIKE '" + lbltransno.Text + "' AND pcode LIKE '" + pcode.ToString() + "'");
                    LoadCart();
                    if (sri == 0)
                    {
                        dgvCash.CurrentCell = dgvCash.Rows[0].Cells[0];
                        dgvCash.Rows[0].Selected = true;
                    }
                    else
                    {
                        dgvCash.ClearSelection();
                        dgvCash.CurrentCell = dgvCash.Rows[sri].Cells[0];
                        dgvCash.Rows[sri].Selected = true;
                    }
                }
                else
                {
                    if (MessageBox.Show("Are you sure to remove the item from cart?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        dbCon.ExecuteQuery("DELETE FROM tblCart WHERE id LIKE '" + id.ToString() + "'");
                        LoadCart();
                    }
                }
            }
            else
            {
                return;
            }
        }

        private void txtBarcode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                txtBarcode.Clear();
                _ = txtBarcode.Focus();
            }
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                return;
            }
        }

        private async void txtBarcode_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (txtBarcode.Text == string.Empty)
                {
                    return;
                }
                else
                {
                    string _pcode;
                    double _price;
                    decimal _qty;
                    cn.Open();
                    cmd = new SQLiteCommand("SELECT * FROM tblProduct WHERE barcode LIKE '" + txtBarcode.Text + "' AND isactive = 1", cn);
                    dr = cmd.ExecuteReader();
                    _ = dr.Read();
                    if (dr.HasRows)
                    {
                        qty = decimal.Parse(dr["qty"].ToString());
                        _pcode = dr["pcode"].ToString();
                        _price = double.Parse(dr["price"].ToString());
                        Common.Quantity quantity = new Common.Quantity();
                        await Task.Delay(700);
                        _ = quantity.ShowDialog();
                        _qty = decimal.Parse(quantity.text);

                        dr.Close();
                        cn.Close();
                        // insert to tblCart
                        if (_qty.ToString() != null && _qty.ToString() != string.Empty && _qty > 0)
                        {
                            AddToCart(_pcode, _price, _qty);
                            txtBarcode.Clear();
                        }
                        else
                        {
                            return;
                        }
                    }
                    dr.Close();
                    cn.Close();
                }
            }
            catch (Exception ex)
            {
                cn.Close();
                _ = MessageBox.Show(ex.Message, sTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void AddToCart(string _pcode, double _price, decimal _qty)
        {
            try
            {
                string id = "";
                decimal cart_qty = 0;
                bool found = false;

                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM tblCart WHERE transno = @transno AND pcode=@pcode", cn);
                _ = cmd.Parameters.AddWithValue("@transno", lbltransno.Text);
                _ = cmd.Parameters.AddWithValue("@pcode", _pcode);
                dr = cmd.ExecuteReader();
                _ = dr.Read();
                if (dr.HasRows)
                {
                    id = dr["id"].ToString();
                    cart_qty = decimal.Parse(dr["qty"].ToString());
                    found = true;
                }
                else
                {
                    found = false;
                }

                dr.Close();
                cn.Close();

                if (found)
                {
                    if (qty < (_qty + cart_qty))
                    {
                        _ = MessageBox.Show("Unable to proceed. Remaining quantity on hand is " + qty, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    cn.Open();
                    cmd = new SQLiteCommand("UPDATE tblCart SET qty=(qty + " + _qty + ") where id= '" + id + "'", cn);
                    _ = cmd.ExecuteReader();
                    cn.Close();
                    txtBarcode.SelectionStart = 0;
                    txtBarcode.SelectionLength = txtBarcode.Text.Length;
                    LoadCart();
                }
                else
                {
                    if (qty < (_qty + cart_qty))
                    {
                        _ = MessageBox.Show("Unable to proceed. Remaining quantity on hand is " + qty, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return;
                    }
                    cn.Open();
                    cmd = new SQLiteCommand("INSERT INTO tblCart(transno,pcode,price,qty,sdate,cashier)VALUES(@transno,@pcode,round(@price,2),@qty,@sdate,@cashier)", cn);

                    _ = cmd.Parameters.AddWithValue("@transno", lbltransno.Text);
                    _ = cmd.Parameters.AddWithValue("@pcode", _pcode);
                    _ = cmd.Parameters.AddWithValue("@price", _price);
                    _ = cmd.Parameters.AddWithValue("@qty", _qty);
                    _ = cmd.Parameters.AddWithValue("@sdate", DateTime.Now);
                    _ = cmd.Parameters.AddWithValue("@cashier", lblusername.Text);

                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                    LoadCart();
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, sTitle);
            }
        }

        private void dgvCash_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvCash.CurrentRow.Index;
            selectedRowIndex = dgvCash.CurrentRow.Index;
            id = dgvCash[1, i].Value.ToString();
            pcode = dgvCash[2, i].Value.ToString();
            price = dgvCash[8, i].Value.ToString();
            qty = decimal.Parse(dgvCash[5, i].Value.ToString());
        }

        private void dgvCash_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvCash.Columns[e.ColumnIndex].Name;

            if (colName == "Delete")
            {
                if (MessageBox.Show("Are you sure to remove the item from cart?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    dbCon.ExecuteQuery("DELETE FROM tblCart WHERE id LIKE '" + dgvCash.Rows[e.RowIndex].Cells[1].Value.ToString() + "'");
                    LoadCart();
                }
            }
            if (colName == "colAdd")
            {
                cn.Open();
                cmd = new SQLiteCommand("SELECT SUM(qty) as qty FROM tblProduct WHERE pcode LIKE '" + dgvCash.Rows[e.RowIndex].Cells[2].Value.ToString() + "' GROUP BY pcode", cn);
                decimal i = decimal.Parse(cmd.ExecuteScalar().ToString());
                cn.Close();
                if (decimal.Parse(dgvCash.Rows[e.RowIndex].Cells[5].Value.ToString()) < i)
                {
                    dbCon.ExecuteQuery("UPDATE tblCart SET qty = qty + 1 WHERE transno LIKE '" + lbltransno.Text + "' AND pcode LIKE '" + dgvCash.Rows[e.RowIndex].Cells[2].Value.ToString() + "'");
                    LoadCart();
                    if (sri == 0)
                    {
                        dgvCash.CurrentCell = dgvCash.Rows[0].Cells[0];
                        dgvCash.Rows[0].Selected = true;
                    }
                    else
                    {
                        dgvCash.ClearSelection();
                        dgvCash.CurrentCell = dgvCash.Rows[sri].Cells[0];
                        dgvCash.Rows[sri].Selected = true;
                    }
                }
                else
                {
                    _ = MessageBox.Show("Unable to proceed. Remaining quantity on hand is " + i, "Out of Stock", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
            }
            else if (colName == "colReduce")
            {
                cn.Open();
                cmd = new SQLiteCommand("SELECT SUM(qty) as qty FROM tblCart WHERE pcode LIKE '" + dgvCash.Rows[e.RowIndex].Cells[2].Value.ToString() + "' AND status LIKE 'Pending' GROUP BY pcode", cn);
                decimal x = decimal.Parse(cmd.ExecuteScalar().ToString());
                cn.Close();
                if (x > 1)
                {
                    dbCon.ExecuteQuery("UPDATE tblCart SET qty = qty - 1 WHERE transno LIKE '" + lbltransno.Text + "' AND pcode LIKE '" + dgvCash.Rows[e.RowIndex].Cells[2].Value.ToString() + "'");
                    LoadCart();
                    if (sri == 0)
                    {
                        dgvCash.CurrentCell = dgvCash.Rows[0].Cells[0];
                        dgvCash.Rows[0].Selected = true;
                    }
                    else
                    {
                        dgvCash.ClearSelection();
                        dgvCash.CurrentCell = dgvCash.Rows[sri].Cells[0];
                        dgvCash.Rows[sri].Selected = true;
                    }
                }
                else
                {
                    if (MessageBox.Show("Are you sure to remove the item from cart?", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        dbCon.ExecuteQuery("DELETE FROM tblCart WHERE id LIKE '" + dgvCash.Rows[e.RowIndex].Cells[1].Value.ToString() + "'");
                        LoadCart();
                    }
                }
            }
        }

        private void Cashier_FormClosing(object sender, FormClosingEventArgs e)
        {
            ClearCart();
        }


        private void btnsearchprd_Enter(object sender, EventArgs e)
        {
            btnsearchprd.BackColor = Color.FromArgb(43, 57, 79);
        }

        private void btnsearchprd_Leave(object sender, EventArgs e)
        {
            btnsearchprd.BackColor = Color.DarkSlateGray;
        }

        private void btndiscount_Enter(object sender, EventArgs e)
        {
            btndiscount.BackColor = Color.FromArgb(43, 57, 79);
        }

        private void btndiscount_Leave(object sender, EventArgs e)
        {
            btndiscount.BackColor = Color.DarkSlateGray;
        }

        private void btnpayment_Enter(object sender, EventArgs e)
        {
            btnpayment.BackColor = Color.FromArgb(43, 57, 79);
        }

        private void btnpayment_Leave(object sender, EventArgs e)
        {
            btnpayment.BackColor = Color.DarkSlateGray;
        }

        private void btnclearcart_Enter(object sender, EventArgs e)
        {
            btnclearcart.BackColor = Color.FromArgb(43, 57, 79);
        }

        private void btnclearcart_Leave(object sender, EventArgs e)
        {
            btnclearcart.BackColor = Color.DarkSlateGray;
        }

        private void btnsales_Enter(object sender, EventArgs e)
        {
            btnsales.BackColor = Color.FromArgb(43, 57, 79);
        }

        private void btnsales_Leave(object sender, EventArgs e)
        {
            btnsales.BackColor = Color.DarkSlateGray;
        }

        private void btnchangepassword_Enter(object sender, EventArgs e)
        {
            btnchangepassword.BackColor = Color.FromArgb(43, 57, 79);
        }

        private void btnchangepassword_Leave(object sender, EventArgs e)
        {
            btnchangepassword.BackColor = Color.DarkSlateGray;
        }

        private void BarcodeScanner_BarcodeScanned(object sender, BarcodeScannerEventArgs e)
        {
            txtBarcode.Text = e.Barcode;
        }

        private void btnbookkeeping_Click(object sender, EventArgs e)
        {
            slide(btnbookkeeping);
            BookKeepingModule book = new BookKeepingModule(this);
            book.txttotalamount.Text = lblDisplayTotal.Text;
            _ = book.ShowDialog();
        }

        private void btnbookkeeping_Enter(object sender, EventArgs e)
        {
            btnbookkeeping.BackColor = Color.FromArgb(43, 57, 79);
        }

        private void btnbookkeeping_Leave(object sender, EventArgs e)
        {
            btnbookkeeping.BackColor = Color.DarkSlateGray;
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            Notifications notification = new Notifications();
            int i = 0;
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM vwCriticalItems", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                NotificationMsg alert = new NotificationMsg(main)
                {
                    TopLevel = false,
                    StartPosition = FormStartPosition.CenterParent
                };
                alert.lblpcode.Text = dr["pcode"].ToString();
                alert.qty = Convert.ToDecimal(dr["qty"].ToString());
                alert.lblmessage.Enabled = false;
                alert.pictureBox1.Enabled = false;
                alert.lblmessage.Text = dr["qty"].ToString() == "0" ? i + ". " + dr["pdesc"].ToString() + " is out of stock - " + dr["qty"].ToString() : i + ". " + dr["pdesc"].ToString() + " is below your reorder level - " + dr["qty"].ToString();
                alert.pictureBox1.Image = dr["qty"].ToString() == "0" ? Properties.Resources.danger : Properties.Resources.warning;
                notification.flowLayoutPanel1.Controls.Add(alert);
                alert.Show();
            }
            dr.Close();
            cn.Close();
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM vwCriticalItems", cn);

            object Exist = query.ExecuteScalar();
            cn.Close();
            notification.label1.Visible = Convert.ToInt32(Exist) <= 0;
            notification.Show();
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            pictureBox3.Cursor = Cursors.Hand;
        }
    }
}
