﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System.POS
{
    public partial class Discount : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly string sTitle = "Point Of Sale";
        private readonly Cashier cashier;
        private Point mouseOffset;
        public Discount(Cashier cashier)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            this.cashier = cashier;
            KeyPreview = true;
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void txtdiscount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double disc = double.Parse(txttotalprice.Text) * double.Parse(txtdiscount.Text) * 0.01;
                txtdiscountamount.Text = disc.ToString();
            }
            catch (Exception)
            {
                txtdiscountamount.Text = "";
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (MessageBox.Show("Are you sure to add discount?", sTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    cn.Open();
                    cmd = new SQLiteCommand("UPDATE tblCart SET disc_percent=@disc_percent WHERE id=@id", cn);
                    _ = cmd.Parameters.AddWithValue("@disc_percent", double.Parse(txtdiscount.Text));
                    _ = cmd.Parameters.AddWithValue("@id", int.Parse(lblid.Text));
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                    cashier.LoadCart();
                    Dispose();
                }
            }
            catch (Exception ex)
            {
                cn.Close();
                _ = MessageBox.Show(ex.Message, sTitle);
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void txtdiscount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.PerformClick();
            }
        }

        private void Discount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel.PerformClick();
            }
        }

        private void radioButton2_CheckedChanged(object sender, EventArgs e)
        {
            if (amount.Checked)
            {
                txtdiscountamount.Enabled = true;
                txtdiscount.Enabled = false;
                txtdiscountamount.Focus();
            }
        }

        private void percentage_CheckedChanged(object sender, EventArgs e)
        {
            if (percentage.Checked)
            {
                txtdiscountamount.Enabled = false;
                txtdiscount.Enabled = true;
                txtdiscount.Focus();
            }
        }

        private void txtdiscountamount_TextChanged(object sender, EventArgs e)
        {
            try
            {
                double discperc = double.Parse(txtdiscountamount.Text) * 100 / double.Parse(txttotalprice.Text);
                txtdiscount.Text = discperc.ToString();
            }
            catch (Exception)
            {
                txtdiscountamount.Text = "";
            }
        }

        private void txtdiscountamount_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.PerformClick();
            }
        }
    }
}
