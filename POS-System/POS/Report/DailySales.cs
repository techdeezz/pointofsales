﻿using POS_System.POS.CancelOrder;
using System;
using System.Data.SQLite;
using System.Linq;
using System.Windows.Forms;

namespace POS_System.POS.Report
{
    public partial class DailySales : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly Cashier cashier;
        public string soldUser;
        private string _txtid;
        private string _txttrans;
        private string _txtpcode;
        private string _txtdesc;
        private string _txtprice;
        private string _txtqty;
        private string _txtdiscount;
        private string _txttotal;
        private readonly FormCollection fc = Application.OpenForms;

        public DailySales(Cashier cashier)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadCashier();
            dgvSales.Columns[10].Width = 25;
            this.cashier = cashier;
            LoadSalesOnLoad();
        }
        public DailySales()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadCashier();
            dgvSales.Columns[10].Width = 25;
            LoadSalesOnLoad();
            dgvSales.ClearSelection();
        }

        public void LoadCashier()
        {
            cmbCashier.Items.Clear();
            _ = cmbCashier.Items.Add("All Cashier");
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblUser", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                string _username = dr["username"].ToString();
                _ = cmbCashier.Items.Add(char.ToUpper(_username.First()) + _username.Substring(1).ToLower());
            }
            dr.Close();
            cn.Close();
        }

        public void LoadSalesOnLoad()
        {
            int i = 0;
            double total = 0;
            dgvSales.Rows.Clear();
            cn.Open();
            cmd = cmbCashier.Text == "Select a Cashier"
                ? new SQLiteCommand("SELECT c.id, c.transno, c.pcode, p.pdesc,c.sdate, c.price, c.qty, c.disc, c.subtotal, c.total, p.unit FROM tblCart AS c INNER JOIN tblProduct AS p ON c.pcode=p.pcode WHERE status LIKE 'Sold' AND c.transno  LIKE '%" + txtSearch.Text + "%'", cn)
                : cmbCashier.Text == "All Cashier"
                    ? new SQLiteCommand("SELECT c.id, c.transno, c.pcode, p.pdesc,c.sdate, c.price, c.qty, c.disc, c.subtotal, c.total, p.unit FROM tblCart AS c INNER JOIN tblProduct AS p ON c.pcode=p.pcode WHERE status LIKE 'Sold' AND date(sdate) BETWEEN '" + dtFrom.Value.ToString("yyyy-MM-dd") + "' AND '" + dtTo.Value.ToString("yyyy-MM-dd") + "' AND c.transno  LIKE '%" + txtSearch.Text + "%'", cn)
                    : new SQLiteCommand("SELECT c.id, c.transno, c.pcode, p.pdesc,c.sdate, c.price, c.qty, c.disc, c.subtotal, c.total, p.unit FROM tblCart AS c INNER JOIN tblProduct AS p ON c.pcode=p.pcode WHERE status LIKE 'Sold' AND date(sdate) BETWEEN '" + dtFrom.Value.ToString("yyyy-MM-dd") + "' AND '" + dtTo.Value.ToString("yyyy-MM-dd") + "' AND cashier LIKE '" + cmbCashier.Text + "' AND  c.transno  LIKE '%" + txtSearch.Text + "%'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                total += double.Parse(dr["subtotal"].ToString());
                _ = dgvSales.Rows.Add(i, dr["id"].ToString(), dr["transno"].ToString(), dr["pcode"].ToString(), dr["pdesc"].ToString(), DateTime.Parse(dr["sdate"].ToString()).ToShortDateString(), double.Parse(dr["price"].ToString()).ToString("0.00"), dr["qty"].ToString(), dr["unit"].ToString(), dr["disc"].ToString(), double.Parse(dr["subtotal"].ToString()).ToString("#,##0.00"));
            }
            dr.Close();
            cn.Close();
            lblTotal.Text = total.ToString("#,##0.00");
        }

        private void cmbCashier_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (TopLevel)
            {
                dtFrom.Enabled = false;
                dtTo.Enabled = false;
            }
            else
            {
                dtFrom.Enabled = true;
                dtTo.Enabled = true;
            }
            LoadSalesOnLoad();
        }

        private void dtFrom_ValueChanged(object sender, EventArgs e)
        {
            LoadSalesOnLoad();
        }

        private void dtTo_ValueChanged(object sender, EventArgs e)
        {
            LoadSalesOnLoad();
        }

        private void DailySales_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                if (TopLevel)
                {
                    Dispose();
                }
                else if (txtSearch.Text.Length > 0 || cmbCashier.Text != "Select a Cashier")
                {
                    txtSearch.Clear();
                    cmbCashier.Text = "Select a Cashier";
                    dtFrom.Value = DateTime.Now;
                    dtFrom.Enabled = false;
                    dtTo.Enabled = false;
                    dtTo.Value = DateTime.Now;
                    LoadSalesOnLoad();
                }
            }
            if (e.KeyCode == Keys.F8)
            {
                _ = txtSearch.Focus();
            }
        }

        public void CellCancel(string txtid, string txttrans, string txtpcode, string txtdesc, string txtprice, string txtqty, string txtdiscount, string txttotal)
        {
            CancelOrderModule cancelOrder = new CancelOrderModule(this);
            cancelOrder.txtid.Text = txtid;
            cancelOrder.txttrans.Text = txttrans;
            cancelOrder.txtpcode.Text = txtpcode;
            cancelOrder.txtdesc.Text = txtdesc;
            cancelOrder.txtprice.Text = txtprice;
            cancelOrder.txtqty.Text = txtqty;
            cancelOrder.txtdiscount.Text = txtdiscount;
            cancelOrder.txttotal.Text = txttotal;
            cancelOrder.txtcancelby.Text = soldUser;
            _ = cancelOrder.ShowDialog();
        }
        private void dgvSales_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvSales.Columns[e.ColumnIndex].Name;
            if (colName == "Cancel")
            {
                CellCancel(dgvSales.Rows[e.RowIndex].Cells[1].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[2].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[3].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[4].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[6].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[7].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[9].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[10].Value.ToString());
            }
            else
            {
                return;
            }
        }

        private void dgvSales_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvSales.CurrentRow.Index;
            _txtid = dgvSales[1, i].Value.ToString();
            _txttrans = dgvSales[2, i].Value.ToString();
            _txtpcode = dgvSales[3, i].Value.ToString();
            _txtdesc = dgvSales[4, i].Value.ToString();
            _txtprice = dgvSales[5, i].Value.ToString();
            _txtqty = dgvSales[6, i].Value.ToString();
            _txtdiscount = dgvSales[8, i].Value.ToString();
            _txttotal = dgvSales[9, i].Value.ToString();
        }

        private void dgvSales_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                CellCancel(_txtid, _txttrans, _txtpcode, _txtdesc, _txtprice, _txtqty, _txtdiscount, _txttotal);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadSalesOnLoad();
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            DailySalesReport report = new DailySalesReport();
            string param = "Date From: " + dtFrom.Value.ToString("yyyy-MM-dd") + " And " + dtTo.Value.ToString("yyyy-MM-dd");
            if (cmbCashier.Text == "Select a Cashier")
            {
                report.LoadDailyReport("SELECT c.id, c.transno, c.pcode, p.pdesc, c.price, c.qty, c.disc, c.subtotal, c.total, p.unit FROM tblCart AS c INNER JOIN tblProduct AS p ON c.pcode=p.pcode WHERE status LIKE 'Sold' AND c.transno  LIKE '%" + txtSearch.Text + "%'", "Date not specified!", "Cashier not specified!");
            }
            else if (cmbCashier.Text == "All Cashier")
            {
                report.LoadDailyReport("SELECT c.id, c.transno, c.pcode, p.pdesc, c.price, c.qty, c.disc, c.subtotal, c.total, p.unit FROM tblCart AS c INNER JOIN tblProduct AS p ON c.pcode=p.pcode WHERE status LIKE 'Sold' AND date(sdate) BETWEEN '" + dtFrom.Value.ToString("yyyy-MM-dd") + "' AND '" + dtTo.Value.ToString("yyyy-MM-dd") + "' AND c.transno  LIKE '%" + txtSearch.Text + "%'", param, "All Cashiers");
            }
            else
            {
                report.LoadDailyReport("SELECT c.id, c.transno, c.pcode, p.pdesc, c.price, c.qty, c.disc, c.subtotal, c.total, p.unit FROM tblCart AS c INNER JOIN tblProduct AS p ON c.pcode=p.pcode WHERE status LIKE 'Sold' AND date(sdate) BETWEEN '" + dtFrom.Value.ToString("yyyy-MM-dd") + "' AND '" + dtTo.Value.ToString("yyyy-MM-dd") + "' AND cashier LIKE '" + cmbCashier.Text + "' AND  c.transno  LIKE '%" + txtSearch.Text + "%'", param, cmbCashier.Text);
            }
            _ = report.ShowDialog();
        }

        private void dgvSales_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            CellCancel(dgvSales.Rows[e.RowIndex].Cells[1].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[2].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[3].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[4].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[6].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[7].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[9].Value.ToString(), dgvSales.Rows[e.RowIndex].Cells[10].Value.ToString());
        }
    }
}
