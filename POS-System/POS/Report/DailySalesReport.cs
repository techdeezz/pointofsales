﻿using Microsoft.Reporting.WinForms;
using System;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.POS.Report
{
    public partial class DailySalesReport : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;

        private string store;
        private string address;
        private string mobile;
        private string email;
        public DailySalesReport()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadStore();
        }
        public void LoadStore()
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblStore LIMIT 1", cn);
            dr = cmd.ExecuteReader();
            _ = dr.Read();
            if (dr.HasRows)
            {
                store = dr["store"].ToString();
                address = dr["address"].ToString() != string.Empty ? dr["address"].ToString() : "-";
                mobile = dr["mobile"].ToString() != string.Empty ? dr["mobile"].ToString() : "-";
                email = dr["email"].ToString() != string.Empty ? dr["email"].ToString() : "-";
            }
            else
            {
                store = "V4Codes";
                address = "-";
                mobile = "-";
                email = "-";
            }
            dr.Close();
            cn.Close();
        }

        public void LoadDailyReport(string sql, string param, string cashier)
        {
            try
            {
                dbCon.ExecuteQuery(sql);
                DataTable dt = dbCon.getTable(sql);

                reportViewer1.LocalReport.ReportPath = Application.StartupPath + @"\Report\DailySalesReport.rdlc";
                ReportDataSource rpt = new ReportDataSource("DataSet1", dt);
                reportViewer1.LocalReport.DataSources.Clear();

                ReportParameter StoreName = new ReportParameter("StoreName", store);
                ReportParameter StoreAddress = new ReportParameter("StoreAddress", address);
                ReportParameter Header = new ReportParameter("Header", "Daily sales report");
                ReportParameter StoreMobile = new ReportParameter("StoreMobile", mobile);
                ReportParameter StoreEmail = new ReportParameter("StoreEmail", email);
                ReportParameter ReportDate = new ReportParameter("ReportDate", param);
                ReportParameter Cashier = new ReportParameter("Cashier", cashier);

                reportViewer1.LocalReport.SetParameters(Header);
                reportViewer1.LocalReport.SetParameters(StoreEmail);
                reportViewer1.LocalReport.SetParameters(ReportDate);
                reportViewer1.LocalReport.SetParameters(StoreName);
                reportViewer1.LocalReport.SetParameters(StoreAddress);
                reportViewer1.LocalReport.SetParameters(Cashier);
                reportViewer1.LocalReport.SetParameters(StoreMobile);

                reportViewer1.LocalReport.DataSources.Add(rpt);
                reportViewer1.LocalReport.Refresh();
                reportViewer1.SetDisplayMode(DisplayMode.PrintLayout);
                reportViewer1.ZoomMode = ZoomMode.PageWidth;

                reportViewer1.RefreshReport();
            }
            catch (Exception ex)
            {
                cn.Close();
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void DailySalesReport_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Dispose();
            }
        }
    }
}
