﻿namespace POS_System.POS
{
    partial class Cashier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cashier));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label3 = new System.Windows.Forms.Label();
            this.lblusername = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnchangepassword = new System.Windows.Forms.Button();
            this.btnsales = new System.Windows.Forms.Button();
            this.btnclearcart = new System.Windows.Forms.Button();
            this.btnbookkeeping = new System.Windows.Forms.Button();
            this.btnlogout = new System.Windows.Forms.Button();
            this.btnpayment = new System.Windows.Forms.Button();
            this.btndiscount = new System.Windows.Forms.Button();
            this.btnsearchprd = new System.Windows.Forms.Button();
            this.panel4 = new System.Windows.Forms.Panel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.picMinimize = new System.Windows.Forms.PictureBox();
            this.picClose = new System.Windows.Forms.PictureBox();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.lblname = new System.Windows.Forms.Label();
            this.panel5 = new System.Windows.Forms.Panel();
            this.txtQty = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lblDisplayTotal = new System.Windows.Forms.Label();
            this.lbldiscount = new System.Windows.Forms.Label();
            this.ddd = new System.Windows.Forms.Label();
            this.lblsalestotal = new System.Windows.Forms.Label();
            this.aa = new System.Windows.Forms.Label();
            this.txtBarcode = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lbldate = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lbltransno = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lbl1 = new System.Windows.Forms.Label();
            this.lbltimer = new System.Windows.Forms.Label();
            this.dgvCash = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column9 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column7 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column8 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.colReduce = new System.Windows.Forms.DataGridViewImageColumn();
            this.colAdd = new System.Windows.Forms.DataGridViewImageColumn();
            this.Delete = new System.Windows.Forms.DataGridViewImageColumn();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.dataGridViewImageColumn1 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn2 = new System.Windows.Forms.DataGridViewImageColumn();
            this.dataGridViewImageColumn3 = new System.Windows.Forms.DataGridViewImageColumn();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMinimize)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCash)).BeginInit();
            this.SuspendLayout();
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.label3);
            this.panel2.Controls.Add(this.lblusername);
            this.panel2.Controls.Add(this.pictureBox1);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel2.Location = new System.Drawing.Point(0, 0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(210, 180);
            this.panel2.TabIndex = 0;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Consolas", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 143);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(210, 22);
            this.label3.TabIndex = 2;
            this.label3.Text = "Cashier";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblusername
            // 
            this.lblusername.ForeColor = System.Drawing.Color.White;
            this.lblusername.Location = new System.Drawing.Point(0, 115);
            this.lblusername.Name = "lblusername";
            this.lblusername.Size = new System.Drawing.Size(210, 22);
            this.lblusername.TabIndex = 1;
            this.lblusername.Text = "Username";
            this.lblusername.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::POS_System.Properties.Resources.person_100px;
            this.pictureBox1.Location = new System.Drawing.Point(55, 6);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(100, 100);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel1.Controls.Add(this.btnchangepassword);
            this.panel1.Controls.Add(this.btnsales);
            this.panel1.Controls.Add(this.btnclearcart);
            this.panel1.Controls.Add(this.btnbookkeeping);
            this.panel1.Controls.Add(this.btnlogout);
            this.panel1.Controls.Add(this.btnpayment);
            this.panel1.Controls.Add(this.btndiscount);
            this.panel1.Controls.Add(this.btnsearchprd);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(210, 700);
            this.panel1.TabIndex = 2;
            // 
            // btnchangepassword
            // 
            this.btnchangepassword.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnchangepassword.FlatAppearance.BorderSize = 0;
            this.btnchangepassword.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnchangepassword.ForeColor = System.Drawing.Color.White;
            this.btnchangepassword.Image = global::POS_System.Properties.Resources.password_reset_30px;
            this.btnchangepassword.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnchangepassword.Location = new System.Drawing.Point(0, 450);
            this.btnchangepassword.Name = "btnchangepassword";
            this.btnchangepassword.Size = new System.Drawing.Size(210, 45);
            this.btnchangepassword.TabIndex = 12;
            this.btnchangepassword.Text = "   Change Password";
            this.btnchangepassword.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnchangepassword.UseVisualStyleBackColor = true;
            this.btnchangepassword.Click += new System.EventHandler(this.btnchangepassword_Click);
            this.btnchangepassword.Enter += new System.EventHandler(this.btnchangepassword_Enter);
            this.btnchangepassword.Leave += new System.EventHandler(this.btnchangepassword_Leave);
            // 
            // btnsales
            // 
            this.btnsales.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnsales.FlatAppearance.BorderSize = 0;
            this.btnsales.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsales.ForeColor = System.Drawing.Color.White;
            this.btnsales.Image = global::POS_System.Properties.Resources.total_sales_30px;
            this.btnsales.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsales.Location = new System.Drawing.Point(0, 405);
            this.btnsales.Name = "btnsales";
            this.btnsales.Size = new System.Drawing.Size(210, 45);
            this.btnsales.TabIndex = 11;
            this.btnsales.Text = "   Daily Sales (F6)";
            this.btnsales.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnsales.UseVisualStyleBackColor = true;
            this.btnsales.Click += new System.EventHandler(this.btnsales_Click);
            this.btnsales.Enter += new System.EventHandler(this.btnsales_Enter);
            this.btnsales.Leave += new System.EventHandler(this.btnsales_Leave);
            // 
            // btnclearcart
            // 
            this.btnclearcart.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnclearcart.Enabled = false;
            this.btnclearcart.FlatAppearance.BorderSize = 0;
            this.btnclearcart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnclearcart.ForeColor = System.Drawing.Color.White;
            this.btnclearcart.Image = global::POS_System.Properties.Resources.delete_history_30px;
            this.btnclearcart.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnclearcart.Location = new System.Drawing.Point(0, 360);
            this.btnclearcart.Name = "btnclearcart";
            this.btnclearcart.Size = new System.Drawing.Size(210, 45);
            this.btnclearcart.TabIndex = 10;
            this.btnclearcart.Text = "   Clear Cart (F5)";
            this.btnclearcart.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnclearcart.UseVisualStyleBackColor = true;
            this.btnclearcart.Click += new System.EventHandler(this.btnclearcart_Click);
            this.btnclearcart.Enter += new System.EventHandler(this.btnclearcart_Enter);
            this.btnclearcart.Leave += new System.EventHandler(this.btnclearcart_Leave);
            // 
            // btnbookkeeping
            // 
            this.btnbookkeeping.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnbookkeeping.Enabled = false;
            this.btnbookkeeping.FlatAppearance.BorderSize = 0;
            this.btnbookkeeping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbookkeeping.Font = new System.Drawing.Font("Century Gothic", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnbookkeeping.ForeColor = System.Drawing.Color.White;
            this.btnbookkeeping.Image = global::POS_System.Properties.Resources.literature_30px;
            this.btnbookkeeping.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnbookkeeping.Location = new System.Drawing.Point(0, 315);
            this.btnbookkeeping.Name = "btnbookkeeping";
            this.btnbookkeeping.Size = new System.Drawing.Size(210, 45);
            this.btnbookkeeping.TabIndex = 9;
            this.btnbookkeeping.Text = "   Add to Book Keeping (F4)";
            this.btnbookkeeping.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnbookkeeping.UseVisualStyleBackColor = true;
            this.btnbookkeeping.Click += new System.EventHandler(this.btnbookkeeping_Click);
            this.btnbookkeeping.Enter += new System.EventHandler(this.btnbookkeeping_Enter);
            this.btnbookkeeping.Leave += new System.EventHandler(this.btnbookkeeping_Leave);
            // 
            // btnlogout
            // 
            this.btnlogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnlogout.FlatAppearance.BorderSize = 0;
            this.btnlogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnlogout.ForeColor = System.Drawing.Color.White;
            this.btnlogout.Image = global::POS_System.Properties.Resources.logout_cash_30px;
            this.btnlogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnlogout.Location = new System.Drawing.Point(0, 655);
            this.btnlogout.Name = "btnlogout";
            this.btnlogout.Size = new System.Drawing.Size(210, 45);
            this.btnlogout.TabIndex = 8;
            this.btnlogout.Text = "   Logout";
            this.btnlogout.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnlogout.UseVisualStyleBackColor = true;
            this.btnlogout.Click += new System.EventHandler(this.btnlogout_Click);
            // 
            // btnpayment
            // 
            this.btnpayment.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnpayment.Enabled = false;
            this.btnpayment.FlatAppearance.BorderSize = 0;
            this.btnpayment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnpayment.ForeColor = System.Drawing.Color.White;
            this.btnpayment.Image = global::POS_System.Properties.Resources.card_payment_30px;
            this.btnpayment.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnpayment.Location = new System.Drawing.Point(0, 270);
            this.btnpayment.Name = "btnpayment";
            this.btnpayment.Size = new System.Drawing.Size(210, 45);
            this.btnpayment.TabIndex = 4;
            this.btnpayment.Text = "   Settle Payments (F3)";
            this.btnpayment.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnpayment.UseVisualStyleBackColor = true;
            this.btnpayment.Click += new System.EventHandler(this.btnpayment_Click);
            this.btnpayment.Enter += new System.EventHandler(this.btnpayment_Enter);
            this.btnpayment.Leave += new System.EventHandler(this.btnpayment_Leave);
            // 
            // btndiscount
            // 
            this.btndiscount.Dock = System.Windows.Forms.DockStyle.Top;
            this.btndiscount.Enabled = false;
            this.btndiscount.FlatAppearance.BorderSize = 0;
            this.btndiscount.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btndiscount.ForeColor = System.Drawing.Color.White;
            this.btndiscount.Image = global::POS_System.Properties.Resources.get_a_discount_30px;
            this.btndiscount.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btndiscount.Location = new System.Drawing.Point(0, 225);
            this.btndiscount.Name = "btndiscount";
            this.btndiscount.Size = new System.Drawing.Size(210, 45);
            this.btndiscount.TabIndex = 3;
            this.btndiscount.Text = "   Add Discount (F2)";
            this.btndiscount.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btndiscount.UseVisualStyleBackColor = true;
            this.btndiscount.Click += new System.EventHandler(this.btndiscount_Click);
            this.btndiscount.Enter += new System.EventHandler(this.btndiscount_Enter);
            this.btndiscount.Leave += new System.EventHandler(this.btndiscount_Leave);
            // 
            // btnsearchprd
            // 
            this.btnsearchprd.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnsearchprd.FlatAppearance.BorderSize = 0;
            this.btnsearchprd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnsearchprd.ForeColor = System.Drawing.Color.White;
            this.btnsearchprd.Image = global::POS_System.Properties.Resources.search_property_30px;
            this.btnsearchprd.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnsearchprd.Location = new System.Drawing.Point(0, 180);
            this.btnsearchprd.Name = "btnsearchprd";
            this.btnsearchprd.Size = new System.Drawing.Size(210, 45);
            this.btnsearchprd.TabIndex = 2;
            this.btnsearchprd.Text = "   Search Product (F1)";
            this.btnsearchprd.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnsearchprd.UseVisualStyleBackColor = true;
            this.btnsearchprd.Click += new System.EventHandler(this.btnsearchprd_Click);
            this.btnsearchprd.Enter += new System.EventHandler(this.btnsearchprd_Enter);
            this.btnsearchprd.Leave += new System.EventHandler(this.btnsearchprd_Leave);
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panel4.Controls.Add(this.pictureBox3);
            this.panel4.Controls.Add(this.picMinimize);
            this.panel4.Controls.Add(this.picClose);
            this.panel4.Controls.Add(this.pictureBox2);
            this.panel4.Controls.Add(this.lblname);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel4.Location = new System.Drawing.Point(210, 0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(990, 50);
            this.panel4.TabIndex = 4;
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.BackColor = System.Drawing.Color.DarkSlateGray;
            this.pictureBox3.Image = global::POS_System.Properties.Resources.notification;
            this.pictureBox3.Location = new System.Drawing.Point(867, 0);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.TabIndex = 13;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // picMinimize
            // 
            this.picMinimize.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picMinimize.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picMinimize.Image = global::POS_System.Properties.Resources.reduce_28px;
            this.picMinimize.Location = new System.Drawing.Point(929, 0);
            this.picMinimize.Name = "picMinimize";
            this.picMinimize.Size = new System.Drawing.Size(28, 28);
            this.picMinimize.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picMinimize.TabIndex = 3;
            this.picMinimize.TabStop = false;
            this.picMinimize.Click += new System.EventHandler(this.picMinimize_Click);
            // 
            // picClose
            // 
            this.picClose.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.picClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.picClose.Image = ((System.Drawing.Image)(resources.GetObject("picClose.Image")));
            this.picClose.Location = new System.Drawing.Point(963, 0);
            this.picClose.Name = "picClose";
            this.picClose.Size = new System.Drawing.Size(28, 28);
            this.picClose.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.picClose.TabIndex = 2;
            this.picClose.TabStop = false;
            this.picClose.Click += new System.EventHandler(this.picClose_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::POS_System.Properties.Resources.user_50px;
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(50, 50);
            this.pictureBox2.TabIndex = 1;
            this.pictureBox2.TabStop = false;
            this.pictureBox2.Visible = false;
            // 
            // lblname
            // 
            this.lblname.AutoSize = true;
            this.lblname.ForeColor = System.Drawing.Color.White;
            this.lblname.Location = new System.Drawing.Point(56, 14);
            this.lblname.Name = "lblname";
            this.lblname.Size = new System.Drawing.Size(122, 20);
            this.lblname.TabIndex = 0;
            this.lblname.Text = "Name and Role";
            this.lblname.Visible = false;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.Color.Silver;
            this.panel5.Controls.Add(this.txtQty);
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.lblDisplayTotal);
            this.panel5.Controls.Add(this.lbldiscount);
            this.panel5.Controls.Add(this.ddd);
            this.panel5.Controls.Add(this.lblsalestotal);
            this.panel5.Controls.Add(this.aa);
            this.panel5.Controls.Add(this.txtBarcode);
            this.panel5.Controls.Add(this.label6);
            this.panel5.Controls.Add(this.lbldate);
            this.panel5.Controls.Add(this.label5);
            this.panel5.Controls.Add(this.lbltransno);
            this.panel5.Controls.Add(this.label2);
            this.panel5.Controls.Add(this.lbl1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel5.Location = new System.Drawing.Point(942, 50);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(258, 650);
            this.panel5.TabIndex = 5;
            // 
            // txtQty
            // 
            this.txtQty.Location = new System.Drawing.Point(146, 329);
            this.txtQty.Name = "txtQty";
            this.txtQty.Size = new System.Drawing.Size(88, 26);
            this.txtQty.TabIndex = 12;
            this.txtQty.Text = "1";
            this.txtQty.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.txtQty.Visible = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 330);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 25);
            this.label1.TabIndex = 11;
            this.label1.Text = "Quantity";
            this.label1.Visible = false;
            // 
            // lblDisplayTotal
            // 
            this.lblDisplayTotal.BackColor = System.Drawing.Color.DarkSlateGray;
            this.lblDisplayTotal.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.lblDisplayTotal.Font = new System.Drawing.Font("Consolas", 26.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDisplayTotal.ForeColor = System.Drawing.Color.White;
            this.lblDisplayTotal.Location = new System.Drawing.Point(0, 543);
            this.lblDisplayTotal.Name = "lblDisplayTotal";
            this.lblDisplayTotal.Size = new System.Drawing.Size(258, 107);
            this.lblDisplayTotal.TabIndex = 10;
            this.lblDisplayTotal.Text = "0.00";
            this.lblDisplayTotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbldiscount
            // 
            this.lbldiscount.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldiscount.Location = new System.Drawing.Point(106, 467);
            this.lbldiscount.Name = "lbldiscount";
            this.lbldiscount.Size = new System.Drawing.Size(140, 20);
            this.lbldiscount.TabIndex = 8;
            this.lbldiscount.Text = "0.00";
            this.lbldiscount.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // ddd
            // 
            this.ddd.AutoSize = true;
            this.ddd.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ddd.Location = new System.Drawing.Point(10, 466);
            this.ddd.Name = "ddd";
            this.ddd.Size = new System.Drawing.Size(103, 23);
            this.ddd.TabIndex = 7;
            this.ddd.Text = "Discount : ";
            // 
            // lblsalestotal
            // 
            this.lblsalestotal.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblsalestotal.Location = new System.Drawing.Point(106, 418);
            this.lblsalestotal.Name = "lblsalestotal";
            this.lblsalestotal.Size = new System.Drawing.Size(140, 20);
            this.lblsalestotal.TabIndex = 8;
            this.lblsalestotal.Text = "0.00";
            this.lblsalestotal.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // aa
            // 
            this.aa.AutoSize = true;
            this.aa.Font = new System.Drawing.Font("Century Gothic", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.aa.Location = new System.Drawing.Point(7, 417);
            this.aa.Name = "aa";
            this.aa.Size = new System.Drawing.Size(108, 23);
            this.aa.TabIndex = 7;
            this.aa.Text = "Sub Total : ";
            // 
            // txtBarcode
            // 
            this.txtBarcode.Location = new System.Drawing.Point(15, 299);
            this.txtBarcode.Name = "txtBarcode";
            this.txtBarcode.Size = new System.Drawing.Size(219, 26);
            this.txtBarcode.TabIndex = 6;
            this.txtBarcode.TextChanged += new System.EventHandler(this.txtBarcode_TextChanged);
            this.txtBarcode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtBarcode_KeyPress);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Image = global::POS_System.Properties.Resources.barcode_35px;
            this.label6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.label6.Location = new System.Drawing.Point(9, 257);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(182, 25);
            this.label6.TabIndex = 5;
            this.label6.Text = "Barcode (F8)";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbldate
            // 
            this.lbldate.AutoSize = true;
            this.lbldate.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbldate.Location = new System.Drawing.Point(14, 186);
            this.lbldate.Name = "lbldate";
            this.lbldate.Size = new System.Drawing.Size(140, 22);
            this.lbldate.TabIndex = 4;
            this.lbldate.Text = "0000000000000";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(60, 25);
            this.label5.TabIndex = 3;
            this.label5.Text = "Date";
            // 
            // lbltransno
            // 
            this.lbltransno.AutoSize = true;
            this.lbltransno.Font = new System.Drawing.Font("Consolas", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltransno.Location = new System.Drawing.Point(14, 89);
            this.lbltransno.Name = "lbltransno";
            this.lbltransno.Size = new System.Drawing.Size(140, 22);
            this.lbltransno.TabIndex = 2;
            this.lbltransno.Text = "0000000000000";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Century Gothic", 15.75F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(11, 50);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(163, 25);
            this.label2.TabIndex = 1;
            this.label2.Text = "Transaction No";
            // 
            // lbl1
            // 
            this.lbl1.BackColor = System.Drawing.Color.DarkSlateGray;
            this.lbl1.Dock = System.Windows.Forms.DockStyle.Top;
            this.lbl1.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbl1.ForeColor = System.Drawing.Color.White;
            this.lbl1.Location = new System.Drawing.Point(0, 0);
            this.lbl1.Name = "lbl1";
            this.lbl1.Size = new System.Drawing.Size(258, 31);
            this.lbl1.TabIndex = 0;
            this.lbl1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // lbltimer
            // 
            this.lbltimer.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lbltimer.BackColor = System.Drawing.Color.Teal;
            this.lbltimer.Font = new System.Drawing.Font("Consolas", 21.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbltimer.ForeColor = System.Drawing.Color.White;
            this.lbltimer.Location = new System.Drawing.Point(209, 593);
            this.lbltimer.Name = "lbltimer";
            this.lbltimer.Size = new System.Drawing.Size(733, 107);
            this.lbltimer.TabIndex = 9;
            this.lbltimer.Text = "00:00:00";
            this.lbltimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // dgvCash
            // 
            this.dgvCash.AllowUserToAddRows = false;
            this.dgvCash.AllowUserToDeleteRows = false;
            this.dgvCash.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvCash.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.AllCells;
            this.dgvCash.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvCash.BackgroundColor = System.Drawing.Color.White;
            this.dgvCash.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.dgvCash.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.Color.DarkSlateGray;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.Color.White;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvCash.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvCash.ColumnHeadersHeight = 30;
            this.dgvCash.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.DisableResizing;
            this.dgvCash.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column9,
            this.Column2,
            this.Column4,
            this.Column7,
            this.Column5,
            this.Column3,
            this.Column6,
            this.Column8,
            this.colReduce,
            this.colAdd,
            this.Delete});
            this.dgvCash.EditMode = System.Windows.Forms.DataGridViewEditMode.EditProgrammatically;
            this.dgvCash.EnableHeadersVisualStyles = false;
            this.dgvCash.Location = new System.Drawing.Point(210, 50);
            this.dgvCash.Name = "dgvCash";
            this.dgvCash.ReadOnly = true;
            this.dgvCash.RowHeadersVisible = false;
            this.dgvCash.Size = new System.Drawing.Size(732, 543);
            this.dgvCash.TabIndex = 6;
            this.dgvCash.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvCash_CellContentClick);
            this.dgvCash.SelectionChanged += new System.EventHandler(this.dgvCash_SelectionChanged);
            this.dgvCash.KeyDown += new System.Windows.Forms.KeyEventHandler(this.dgvCash_KeyDown);
            // 
            // Column1
            // 
            this.Column1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column1.HeaderText = "No";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 53;
            // 
            // Column9
            // 
            this.Column9.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column9.HeaderText = "Id";
            this.Column9.Name = "Column9";
            this.Column9.ReadOnly = true;
            this.Column9.Visible = false;
            this.Column9.Width = 49;
            // 
            // Column2
            // 
            this.Column2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column2.HeaderText = "Pcode";
            this.Column2.Name = "Column2";
            this.Column2.ReadOnly = true;
            this.Column2.Width = 81;
            // 
            // Column4
            // 
            this.Column4.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.Column4.HeaderText = "ProductName";
            this.Column4.Name = "Column4";
            this.Column4.ReadOnly = true;
            // 
            // Column7
            // 
            this.Column7.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column7.DefaultCellStyle = dataGridViewCellStyle2;
            this.Column7.HeaderText = "Price";
            this.Column7.Name = "Column7";
            this.Column7.ReadOnly = true;
            this.Column7.Width = 69;
            // 
            // Column5
            // 
            this.Column5.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter;
            this.Column5.DefaultCellStyle = dataGridViewCellStyle3;
            this.Column5.HeaderText = "Qty";
            this.Column5.Name = "Column5";
            this.Column5.ReadOnly = true;
            this.Column5.Width = 58;
            // 
            // Column3
            // 
            this.Column3.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            this.Column3.HeaderText = "Unit";
            this.Column3.Name = "Column3";
            this.Column3.ReadOnly = true;
            this.Column3.Width = 60;
            // 
            // Column6
            // 
            this.Column6.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column6.DefaultCellStyle = dataGridViewCellStyle4;
            this.Column6.HeaderText = "Discount";
            this.Column6.Name = "Column6";
            this.Column6.ReadOnly = true;
            this.Column6.Width = 95;
            // 
            // Column8
            // 
            this.Column8.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.AllCells;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleRight;
            this.Column8.DefaultCellStyle = dataGridViewCellStyle5;
            this.Column8.HeaderText = "Total";
            this.Column8.Name = "Column8";
            this.Column8.ReadOnly = true;
            this.Column8.Width = 65;
            // 
            // colReduce
            // 
            this.colReduce.HeaderText = "";
            this.colReduce.Image = global::POS_System.Properties.Resources.reduce_20px;
            this.colReduce.Name = "colReduce";
            this.colReduce.ReadOnly = true;
            this.colReduce.Width = 5;
            // 
            // colAdd
            // 
            this.colAdd.FillWeight = 6F;
            this.colAdd.HeaderText = "";
            this.colAdd.Image = global::POS_System.Properties.Resources.add_new_20px;
            this.colAdd.MinimumWidth = 3;
            this.colAdd.Name = "colAdd";
            this.colAdd.ReadOnly = true;
            this.colAdd.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.colAdd.Width = 3;
            // 
            // Delete
            // 
            this.Delete.FillWeight = 6F;
            this.Delete.HeaderText = "";
            this.Delete.Image = global::POS_System.Properties.Resources.remove_20px;
            this.Delete.Name = "Delete";
            this.Delete.ReadOnly = true;
            this.Delete.ToolTipText = "Delete";
            this.Delete.Width = 5;
            // 
            // timer1
            // 
            this.timer1.Enabled = true;
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // dataGridViewImageColumn1
            // 
            this.dataGridViewImageColumn1.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewImageColumn1.FillWeight = 6F;
            this.dataGridViewImageColumn1.HeaderText = "";
            this.dataGridViewImageColumn1.Image = global::POS_System.Properties.Resources.edit_property_22px;
            this.dataGridViewImageColumn1.MinimumWidth = 3;
            this.dataGridViewImageColumn1.Name = "dataGridViewImageColumn1";
            this.dataGridViewImageColumn1.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            // 
            // dataGridViewImageColumn2
            // 
            this.dataGridViewImageColumn2.AutoSizeMode = System.Windows.Forms.DataGridViewAutoSizeColumnMode.Fill;
            this.dataGridViewImageColumn2.FillWeight = 6F;
            this.dataGridViewImageColumn2.HeaderText = "";
            this.dataGridViewImageColumn2.Image = global::POS_System.Properties.Resources.minus_22px;
            this.dataGridViewImageColumn2.MinimumWidth = 3;
            this.dataGridViewImageColumn2.Name = "dataGridViewImageColumn2";
            this.dataGridViewImageColumn2.Resizable = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridViewImageColumn2.ToolTipText = "Delete";
            // 
            // dataGridViewImageColumn3
            // 
            this.dataGridViewImageColumn3.FillWeight = 6F;
            this.dataGridViewImageColumn3.HeaderText = "";
            this.dataGridViewImageColumn3.Image = global::POS_System.Properties.Resources.remove_20px;
            this.dataGridViewImageColumn3.Name = "dataGridViewImageColumn3";
            this.dataGridViewImageColumn3.ToolTipText = "Delete";
            this.dataGridViewImageColumn3.Width = 10;
            // 
            // Cashier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1200, 700);
            this.Controls.Add(this.dgvCash);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.lbltimer);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.Name = "Cashier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Cashier";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Cashier_FormClosing);
            this.KeyDown += new System.Windows.Forms.KeyEventHandler(this.Cashier_KeyDown);
            this.panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picMinimize)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.picClose)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvCash)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox picClose;
        public System.Windows.Forms.DataGridView dgvCash;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn1;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn2;
        private System.Windows.Forms.Label lbl1;
        private System.Windows.Forms.Label lbltimer;
        private System.Windows.Forms.Label ddd;
        private System.Windows.Forms.Label aa;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lbldate;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.PictureBox picMinimize;
        private System.Windows.Forms.DataGridViewImageColumn dataGridViewImageColumn3;
        public System.Windows.Forms.Label lblusername;
        public System.Windows.Forms.Label lbltransno;
        public System.Windows.Forms.TextBox txtBarcode;
        public System.Windows.Forms.Button btnsearchprd;
        public System.Windows.Forms.TextBox txtQty;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button btnpayment;
        public System.Windows.Forms.Label lblDisplayTotal;
        public System.Windows.Forms.Button btndiscount;
        public System.Windows.Forms.Label lblname;
        public System.Windows.Forms.Label lbldiscount;
        public System.Windows.Forms.Label lblsalestotal;
        public System.Windows.Forms.Button btnlogout;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column9;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column7;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column8;
        private System.Windows.Forms.DataGridViewImageColumn colReduce;
        private System.Windows.Forms.DataGridViewImageColumn colAdd;
        private System.Windows.Forms.DataGridViewImageColumn Delete;
        public System.Windows.Forms.Button btnchangepassword;
        public System.Windows.Forms.Button btnsales;
        private System.Windows.Forms.Button btnclearcart;
        public System.Windows.Forms.Button btnbookkeeping;
        public System.Windows.Forms.PictureBox pictureBox3;
    }
}