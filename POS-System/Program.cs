﻿using LicenseApp.License;
using POS_System.LoginModule;
using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;

namespace POS_System
{
    internal static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        /// 
        private const string MutexName = "MyAppMutex";
        [STAThread]
        private static void Main()
        {
            string id = ComputerInfo.GetComputerId();
            KeyManager key = new KeyManager(id);
            LicenseInfo lic = new LicenseInfo();
            _ = key.LoadSuretyFile(string.Format(@"{0}\libdb\Key.lic", Application.StartupPath), ref lic);
            string productKey = lic.ProductKey;
            string dt = string.Empty;
            if (key.ValidKey(ref productKey))
            {
                KeyValuesClass kv = new KeyValuesClass();
                if (key.DisassembleKey(productKey, ref kv))
                {
                    if (kv.Type == LicenseType.TRIAL)
                    {
                        dt = string.Format("{0}", (kv.Expiration - DateTime.Now.Date).Days);
                    }
                    else if (kv.Type == LicenseType.FULL)
                    {
                        dt = "1996";
                    }
                }
            }
            bool createdNew;
            using (Mutex mutex = new Mutex(true, MutexName, out createdNew))
            {
                if (!createdNew)
                {
                    MessageBox.Show("Another instance of the application is already running.", "Already Running", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    return;
                }
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                if (dt == string.Empty || int.Parse(dt) <= 0)
                {
                    if (MessageBox.Show("Your product is not activated yet or your product is expired please activate it", "Warning", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        Application.Run(new frmRegistration());
                    }
                }
                else
                {
                    if (!IsInstalled())
                    {
                        DBItems db = new DBItems();
                        db.SetupDB();
                        MarkAsInstalled();
                    }
                    Application.Run(new Login());
                }
            }
        }
        private static bool IsInstalled()
        {
            // Check if the file "installed.txt" exists in the application folder
            string installedFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "installed.txt");
            return File.Exists(installedFilePath);
        }

        private static void MarkAsInstalled()
        {
            // Create the file "installed.txt" in the application folder
            string installedFilePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "installed.txt");
            using (File.Create(installedFilePath))
            {
                // The file will be automatically closed after being created
            }
        }
    }
}
