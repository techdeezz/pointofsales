﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.Stock.Module
{
    public partial class ProductStockIn : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly string sTitle = "Point Of Sale";
        private SQLiteDataReader dr;
        private readonly StockIn stock;
        private string _pcode;
        private decimal _qty;
        public ProductStockIn(StockIn stock)
        {
            InitializeComponent();
            this.stock = stock;
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadProduct();
            dgvProduct.Columns[5].Width = 25;
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }
        public void LoadProduct()
        {
            int i = 0;
            dgvProduct.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT pcode, pdesc, qty, " +
                "unit FROM tblProduct WHERE isactive = 1 AND qty = 0 AND pdesc || pcode LIKE '%" + txtSearch.Text + "%'", cn);
            dr = cmd.ExecuteReader();

            while (dr.Read())
            {
                i++;
                _ = dgvProduct.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString());
            }
            dr.Close();
            cn.Close();
        }

        public bool CheckStockinExists(string pcode)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblStock WHERE pcode = @pcode AND refno = @refno", cn);

            _ = query.Parameters.AddWithValue("@pcode", pcode);
            _ = query.Parameters.AddWithValue("@refno", stock.txtrefno.Text);
            object Exist = query.ExecuteScalar();
            cn.Close();

            return Convert.ToInt32(Exist) > 0;
        }
        public void LoadCellClick(string val)
        {
            try
            {
                if (CheckStockinExists(val))
                {
                    cn.Open();
                    cmd = new SQLiteCommand("UPDATE tblStock SET refno = @refno,pcode = @pcode,sdate = @sdate,stockinby = @stockinby,supplierid = @supplierid WHERE pcode = @pcode AND refno = @refno", cn);
                    _ = cmd.Parameters.AddWithValue("@refno", stock.txtrefno.Text);
                    _ = cmd.Parameters.AddWithValue("@pcode", val);
                    _ = cmd.Parameters.AddWithValue("@sdate", stock.dtstock.Value);
                    _ = cmd.Parameters.AddWithValue("@stockinby", stock.txtstockby.Text);
                    _ = cmd.Parameters.AddWithValue("@supplierid", stock.lblid.Text);

                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                }
                else
                {
                    cn.Open();
                    cmd = new SQLiteCommand("INSERT INTO tblStock(refno,pcode,sdate,stockinby,supplierid)VALUES(@refno,@pcode,@sdate,@stockinby,@supplierid)", cn);
                    _ = cmd.Parameters.AddWithValue("@refno", stock.txtrefno.Text);
                    _ = cmd.Parameters.AddWithValue("@pcode", val);
                    _ = cmd.Parameters.AddWithValue("@sdate", stock.dtstock.Value);
                    _ = cmd.Parameters.AddWithValue("@stockinby", stock.txtstockby.Text);
                    _ = cmd.Parameters.AddWithValue("@supplierid", stock.lblid.Text);

                    _ = cmd.ExecuteNonQuery();
                    cn.Close();

                    cn.Open();
                    SQLiteCommand query = new SQLiteCommand("SELECT MAX(id) FROM  tblStock", cn);
                    object insertedId = query.ExecuteScalar();
                    cn.Close();

                    cn.Open();
                    cmd = new SQLiteCommand("INSERT INTO tmpStock(stockid,pcode,qty,sellingprice,actualprice) SELECT @stockid, pcode, qty, price, actualprice FROM tblProduct WHERE pcode = @pcode", cn);
                    _ = cmd.Parameters.AddWithValue("@stockid", Convert.ToDecimal(insertedId));
                    _ = cmd.Parameters.AddWithValue("@pcode", val);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                }
                stock.dgvStock.Rows.Clear();
                stock.LoadStockIn();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, sTitle);
            }
        }

        private void dgvProduct_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvProduct.Columns[e.ColumnIndex].Name;
            if (colName == "Select")
            {
                //
                if (stock.txtstockby.Text == string.Empty)
                {
                    _ = MessageBox.Show("Please specify the name of stock entrant", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Dispose();
                    _ = stock.dgvStock.Focus();
                    return;
                }
                if (Convert.ToDecimal(dgvProduct.Rows[e.RowIndex].Cells[3].Value.ToString()) == 0)
                {
                    if (MessageBox.Show("Are you sure to add this item?", sTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        LoadCellClick(dgvProduct.Rows[e.RowIndex].Cells[1].Value.ToString());
                    }
                }
                else
                {
                    _ = MessageBox.Show("Stock already in, Please goto stock adjustment and adjust your stock", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadProduct();
        }

        private void ProductStockIn_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                if (txtSearch.Text.Length > 0)
                {
                    txtSearch.Clear();
                    _ = txtSearch.Focus();
                }
                else
                {
                    btnClose.PerformClick();
                }
            }
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void dgvProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                //
                if (stock.txtstockby.Text == string.Empty)
                {
                    _ = MessageBox.Show("Please specify the name of stock entrant", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    Dispose();
                    _ = stock.dgvStock.Focus();
                    return;
                }
                if (_qty == 0)
                {
                    if (MessageBox.Show("Are you sure to add this item?", sTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        LoadCellClick(_pcode);
                    }
                }
                else
                {
                    _ = MessageBox.Show("Stock already in, Please goto stock adjustment and adjust your stock", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
        }

        private void dgvProduct_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvProduct.CurrentRow.Index;
            _pcode = dgvProduct[1, i].Value.ToString();
            _qty = Convert.ToDecimal(dgvProduct[3, i].Value.ToString());
        }

        private void ProductStockIn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
            {
                _ = txtSearch.Focus();
            }
        }
    }
}
