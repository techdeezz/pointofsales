﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System
{
    public partial class AddStockIn : Form
    {
        public string quantity = "0";
        public string actualprice = "0";
        public string sellingprice = "0";
        private Point mouseOffset;
        public AddStockIn(string qty, string actprice, string sellprice)
        {
            InitializeComponent();
            txtqty.Text = qty;
            txtaactualprice.Text = actprice;
            txtsellingprice.Text = sellprice;

            quantity = qty;
            actualprice = actprice;
            sellingprice = sellprice;
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            txtqty.Text = quantity;
            txtaactualprice.Text = actualprice;
            txtsellingprice.Text = sellingprice;
            Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if (txtqty.Text != string.Empty && txtaactualprice.Text != string.Empty && txtsellingprice.Text != string.Empty)
            {
                quantity = txtqty.Text;
                actualprice = txtaactualprice.Text;
                sellingprice = txtsellingprice.Text;
                Dispose();
            }
            else
            {
                _ = MessageBox.Show("All fields are required", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _ = txtqty.Focus();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            txtqty.Text = quantity;
            txtaactualprice.Text = actualprice;
            txtsellingprice.Text = sellingprice;
            Dispose();
        }
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void AddStockIn_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                btnCancel.PerformClick();
            }
        }

        private void txtsellingprice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAdd.PerformClick();
            }
        }

        private void txtqty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtaactualprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtsellingprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAdd.PerformClick();
            }
        }

        private void txtaactualprice_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAdd.PerformClick();
            }
        }
    }
}
