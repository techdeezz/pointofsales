﻿using POS_System.Common;
using POS_System.Stock.Module;
using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.Stock
{
    public partial class StockIn : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly MainForm main;
        private readonly string sTitle = "Point Of Sale";
        private string id;
        private string prdcode;
        public StockIn(MainForm main)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadSupplier();
            GetRefNo();
            dgvStock.Columns[13].Width = 25;
            dgvStock.Columns[14].Width = 25;
            this.main = main;
        }
        public void GetRefNo()
        {
            Random rnd = new Random();
            txtrefno.Clear();
            txtrefno.Text = rnd.Next(100000000, 999999999).ToString("D10");
        }
        public void LoadSupplier()
        {
            cmbsupplier.Items.Clear();
            cmbsupplier.DataSource = dbCon.getTable("SELECT * FROM tblSupplier ORDER BY case when supplier = 'Other' then 0 else 1 end, supplier");
            cmbsupplier.DisplayMember = "supplier";
        }
        public void ProductForSupplier(string pcode)
        {
            string supplier = "";
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM vwStockIn WHERE pcode LIKE '" + pcode + "'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                supplier = dr["supplier"].ToString();
            }
            dr.Close();
            cn.Close();
            cmbsupplier.Text = supplier;
        }
        public void LoadStockIn()
        {
            int i = 0;
            dgvStock.Rows.Clear();
            cn.Open();
            _ = txtrefno.Text;
            cmd = new SQLiteCommand("SELECT tblStock.id as id, tblStock.refno as refno, tblStock.pcode as pcode, tblProduct.pdesc as pdesc, tblStock.qty as qty,tblProduct.unit as unit, tblStock.sdate as sdate, tblStock.stockinby as stockinby, tblSupplier.supplier as supplier, tblStock.status as status, tblProduct.price as price, tblProduct.actualprice as actualprice FROM  tblStock INNER JOIN tblProduct ON tblStock.pcode = tblProduct.pcode INNER JOIN tblSupplier ON tblStock.supplierid = tblSupplier.id WHERE refno='" + txtrefno.Text + "' AND status LIKE 'Pending' AND tblProduct.isactive = 1", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvStock.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), double.Parse(dr[11].ToString()).ToString("#,##0.00"), double.Parse(dr[10].ToString()).ToString("#,##0.00"), dr[5].ToString(), DateTime.Parse(dr[6].ToString()).ToString("MM/dd/yyyy hh:mm tt"), dr[7].ToString(), dr[8].ToString(), dr[9].ToString());
            }
            dr.Close();
            cn.Close();
        }

        private void cmbsupplier_KeyPress(object sender, KeyPressEventArgs e)
        {
            e.Handled = true;
        }

        private void btngen_Click(object sender, EventArgs e)
        {
            GetRefNo();
        }

        private void btnBrowsePrd_Click(object sender, EventArgs e)
        {
            ProductStockIn prdstk = new ProductStockIn(this);
            prdstk.dgvProduct.Select();
            _ = prdstk.ShowDialog();
        }

        private void btnEntry_Click(object sender, EventArgs e)
        {
            try
            {
                if (dgvStock.Rows.Count > 0)
                {
                    if (MessageBox.Show("Are you sure want to save this record?", sTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        for (int i = 0; i < dgvStock.Rows.Count; i++)
                        {
                            // update stockin quantity

                            cn.Open();
                            cmd = new SQLiteCommand("UPDATE tblStock SET status='Done',actprice='" + double.Parse(dgvStock.Rows[i].Cells[6].Value.ToString()) + "',sellprice='" + double.Parse(dgvStock.Rows[i].Cells[7].Value.ToString()) + "' WHERE id LIKE '" + dgvStock.Rows[i].Cells[1].Value.ToString() + "'", cn);
                            _ = cmd.ExecuteNonQuery();
                            cn.Close();
                        }
                        Clear();
                        LoadStockIn();
                        cn.Open();
                        cmd = new SQLiteCommand("DELETE FROM tmpStock", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Successfully added", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }

                cn.Open();
                SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM vwCriticalItems", cn);

                object Exist = query.ExecuteScalar();
                cn.Close();
                if (Convert.ToInt32(Exist) > 0)
                {
                    NotificationSingle noti = new NotificationSingle(main);
                    noti.ShowAlert(Exist + " notifications received");
                    main.pictureBox3.Image = POS_System.Properties.Resources.notification_receive;
                }
                else
                {
                    main.pictureBox3.Image = POS_System.Properties.Resources.notification;
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, sTitle, MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void Clear()
        {
            GetRefNo();
            dtstock.Value = DateTime.Now;
        }

        private void dgvStock_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvStock.Columns[e.ColumnIndex].Name;
            if (colName == "Delete")
            {
                if (MessageBox.Show("Are you sure want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    decimal quantity = 0;
                    decimal sellprice = 0;
                    decimal actprice = 0;
                    cn.Open();
                    cmd = new SQLiteCommand("SELECT * FROM tmpStock WHERE pcode = @pcode", cn);
                    _ = cmd.Parameters.AddWithValue("@pcode", dgvStock.Rows[e.RowIndex].Cells[3].Value.ToString());
                    dr = cmd.ExecuteReader();
                    _ = dr.Read();
                    if (dr.HasRows)
                    {
                        quantity = Convert.ToDecimal(dr["qty"].ToString());
                        sellprice = Convert.ToDecimal(dr["sellingprice"].ToString());
                        actprice = Convert.ToDecimal(dr["actualprice"].ToString());
                    }
                    dr.Close();
                    cn.Close();

                    cn.Open();
                    cmd = new SQLiteCommand("UPDATE tblProduct SET qty = @qty, price = @price, actualprice = @actprice WHERE pcode ='" + dgvStock.Rows[e.RowIndex].Cells[3].Value.ToString() + "'", cn);

                    _ = cmd.Parameters.AddWithValue("@qty", quantity);
                    _ = cmd.Parameters.AddWithValue("@price", sellprice);
                    _ = cmd.Parameters.AddWithValue("@actprice", actprice);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();

                    cn.Open();
                    cmd = new SQLiteCommand("DELETE FROM tmpStock WHERE stockid = '" + dgvStock.Rows[e.RowIndex].Cells[1].Value.ToString() + "'", cn);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();

                    cn.Open();
                    cmd = new SQLiteCommand("DELETE FROM tblStock WHERE id='" + dgvStock.Rows[e.RowIndex].Cells[1].Value.ToString() + "'", cn);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();

                    _ = MessageBox.Show("Item has been successfully removed.", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    LoadStockIn();
                }
            }
            else if (colName == "Add")
            {
                //Common.Quantity qty = new Common.Quantity();
                //qty.ShowDialog();
                AddStockIn addStock = new AddStockIn(dgvStock.Rows[e.RowIndex].Cells[5].Value.ToString(), dgvStock.Rows[e.RowIndex].Cells[6].Value.ToString(), dgvStock.Rows[e.RowIndex].Cells[7].Value.ToString());
                _ = addStock.ShowDialog();
                dgvStock.Rows[e.RowIndex].Cells[5].Value = addStock.quantity;
                dgvStock.Rows[e.RowIndex].Cells[7].Value = addStock.sellingprice;
                dgvStock.Rows[e.RowIndex].Cells[6].Value = addStock.actualprice;
            }
        }

        private void btnBrowsePrd_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnBrowsePrd.PerformClick();
            }
        }

        private void txtrefno_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnBrowsePrd.PerformClick();
            }
        }

        private void StockIn_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnBrowsePrd.PerformClick();
            }
        }

        private void dgvStock_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnBrowsePrd.PerformClick();
            }
            if (e.KeyCode.ToString() == "Delete")
            {
                if (id != null)
                {
                    if (MessageBox.Show("Are you sure want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        decimal quantity = 0;
                        decimal sellprice = 0;
                        decimal actprice = 0;
                        cn.Open();
                        cmd = new SQLiteCommand("SELECT * FROM tmpStock WHERE pcode = @pcode", cn);
                        _ = cmd.Parameters.AddWithValue("@pcode", prdcode.ToString());
                        dr = cmd.ExecuteReader();
                        _ = dr.Read();
                        if (dr.HasRows)
                        {
                            quantity = Convert.ToDecimal(dr["qty"].ToString());
                            sellprice = Convert.ToDecimal(dr["sellingprice"].ToString());
                            actprice = Convert.ToDecimal(dr["actualprice"].ToString());
                        }
                        dr.Close();
                        cn.Close();

                        cn.Open();
                        cmd = new SQLiteCommand("UPDATE tblProduct SET qty = @qty, price = @price, actualprice = @actprice WHERE pcode ='" + prdcode.ToString() + "'", cn);

                        _ = cmd.Parameters.AddWithValue("@qty", quantity);
                        _ = cmd.Parameters.AddWithValue("@price", sellprice);
                        _ = cmd.Parameters.AddWithValue("@actprice", actprice);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();

                        cn.Open();
                        cmd = new SQLiteCommand("DELETE FROM tmpStock WHERE stockid = '" + id.ToString() + "'", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();

                        cn.Open();
                        cmd = new SQLiteCommand("DELETE FROM tblStock WHERE id='" + id.ToString() + "'", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Item has been successfully removed.", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        LoadStockIn();
                    }
                }
            }
            if (e.KeyCode == Keys.Enter)
            {
                int i = dgvStock.CurrentRow.Index;
                if (dgvStock[5, i].Value.ToString() == "0")
                {
                    if (id != null)
                    {
                        AddStockIn addStock = new AddStockIn(dgvStock[5, i].Value.ToString(), dgvStock[6, i].Value.ToString(), dgvStock[7, i].Value.ToString());
                        _ = addStock.ShowDialog();
                        dgvStock[5, i].Value = addStock.quantity;
                        dgvStock[7, i].Value = addStock.sellingprice;
                        dgvStock[6, i].Value = addStock.actualprice;
                    }
                }
                else
                {
                    btnEntry.PerformClick();
                }

            }
        }

        private void btnEntry_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnBrowsePrd.PerformClick();
            }
        }

        private void metroTabControl1_Selected(object sender, TabControlEventArgs e)
        {
            if (e.TabPage == tabPage1)
            {
                _ = dgvStock.Focus();
            }
        }

        private void metroTabControl1_KeyPress(object sender, KeyPressEventArgs e)
        {

        }

        private void dgvStock_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvStock.CurrentRow.Index;
            id = dgvStock[1, i].Value.ToString();
            prdcode = dgvStock[3, i].Value.ToString();
        }

        private void StockIn_Load(object sender, EventArgs e)
        {
            dgvStock.Columns[13].Width = 25;
            dgvStock.Columns[14].Width = 25;
            metroTabControl1.SelectedTab = tabPage1;
            if (main.lbluname.Text != string.Empty || main.lbluname.Text != string.Empty)
            {
                txtstockby.Text = main.lbluname.Text;
                txtstockby.ReadOnly = true;
            }
            else
            {
                txtstockby.ReadOnly = false;
            }
        }

        private void btnload_Click(object sender, EventArgs e)
        {
            try
            {
                int i = 0;
                dgvInStockHistory.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT tblStock.id as id, tblStock.refno as refno, tblStock.pcode as pcode, tblProduct.pdesc as pdesc, tblStock.qty as qty, tblProduct.unit as unit, tblStock.sdate as sdate, tblStock.stockinby as stockinby, tblSupplier.supplier as supplier, tblStock.status as status, tblStock.sellprice as price, tblStock.actprice as actualprice FROM  tblStock INNER JOIN tblProduct ON tblStock.pcode = tblProduct.pcode INNER JOIN tblSupplier ON tblStock.supplierid = tblSupplier.id WHERE status LIKE 'Done' AND date(sdate) BETWEEN '" + dtFrom.Value.ToString("yyyy-MM-dd") + "' AND '" + dtTo.Value.ToString("yyyy-MM-dd") + "'", cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    dgvInStockHistory.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), double.Parse(dr[11].ToString()).ToString("#,##0.00"), double.Parse(dr[10].ToString()).ToString("#,##0.00"), dr[5].ToString(), DateTime.Parse(dr[6].ToString()).ToShortDateString(), dr[7].ToString(), dr[8].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void metroTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabpage2.Focus())
            {
                dtFrom.Value = DateTime.Today;
                dtTo.Value = DateTime.Today;
                int i = 0;
                dgvInStockHistory.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT tblStock.id as id, tblStock.refno as refno, tblStock.pcode as pcode, tblProduct.pdesc as pdesc, tblStock.qty as qty,tblProduct.unit as unit, tblStock.sdate as sdate, tblStock.stockinby as stockinby, tblSupplier.supplier as supplier, tblStock.status as status, tblStock.sellprice as price, tblStock.actprice as actualprice FROM  tblStock INNER JOIN tblProduct ON tblStock.pcode = tblProduct.pcode INNER JOIN tblSupplier ON tblStock.supplierid = tblSupplier.id WHERE status LIKE 'Done'", cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    dgvInStockHistory.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), double.Parse(dr[11].ToString()).ToString("#,##0.00"), double.Parse(dr[10].ToString()).ToString("#,##0.00"), dr[5].ToString(), DateTime.Parse(dr[6].ToString()).ToShortDateString(), dr[7].ToString(), dr[8].ToString());
                }
                dr.Close();
                cn.Close();
            }
        }

        private void cmbsupplier_TextChanged(object sender, EventArgs e)
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblSupplier WHERE supplier LIKE '" + cmbsupplier.Text + "'", cn);
            dr = cmd.ExecuteReader();
            _ = dr.Read();
            if (dr.HasRows)
            {
                lblid.Text = dr["id"].ToString();
                txtcontactperson.Text = dr["contactPerson"].ToString();
                txtaddress.Text = dr["address"].ToString();
            }
            dr.Close();
            cn.Close();
        }

        private void StockIn_Leave(object sender, EventArgs e)
        {
            for (int i = 0; i < dgvStock.Rows.Count; i++)
            {
                // update product quantity
                decimal quantity = 0;
                decimal sellprice = 0;
                decimal actprice = 0;
                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM tmpStock WHERE pcode = @pcode", cn);
                _ = cmd.Parameters.AddWithValue("@pcode", dgvStock.Rows[i].Cells[3].Value.ToString());
                dr = cmd.ExecuteReader();
                _ = dr.Read();
                if (dr.HasRows)
                {
                    quantity = Convert.ToDecimal(dr["qty"].ToString());
                    sellprice = Convert.ToDecimal(dr["sellingprice"].ToString());
                    actprice = Convert.ToDecimal(dr["actualprice"].ToString());
                }
                dr.Close();
                cn.Close();

                cn.Open();
                cmd = new SQLiteCommand("UPDATE tblProduct SET qty = '" + quantity + "', price = '" + sellprice + "', actualprice = '" + actprice + "' WHERE pcode LIKE '" + dgvStock.Rows[i].Cells[3].Value.ToString() + "'", cn);

                _ = cmd.ExecuteNonQuery();
                cn.Close();
                // update stockin quantity

                cn.Open();
                cmd = new SQLiteCommand("DELETE FROM tblStock WHERE id LIKE '" + dgvStock.Rows[i].Cells[1].Value.ToString() + "' AND status LIKE 'Pending'", cn);
                _ = cmd.ExecuteNonQuery();
                cn.Close();
            }
            Clear();
            cn.Open();
            cmd = new SQLiteCommand("DELETE FROM tmpStock", cn);
            _ = cmd.ExecuteNonQuery();
            cn.Close();
            LoadStockIn();
        }


        private void dgvStock_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            // update product quantity
            if (dgvStock.CurrentRow != null)
            {
                cn.Open();
                cmd = new SQLiteCommand("UPDATE tblProduct SET qty = " + decimal.Parse(dgvStock.Rows[e.RowIndex].Cells[5].Value.ToString()) + ", price = " + decimal.Parse(dgvStock.Rows[e.RowIndex].Cells[7].Value.ToString()) + ", actualprice = " + decimal.Parse(dgvStock.Rows[e.RowIndex].Cells[6].Value.ToString()) + " WHERE pcode LIKE '" + dgvStock.Rows[e.RowIndex].Cells[3].Value.ToString() + "'", cn);

                _ = cmd.ExecuteNonQuery();
                cn.Close();
                // update stockin quantity

                cn.Open();
                cmd = new SQLiteCommand("UPDATE tblStock SET qty = " + decimal.Parse(dgvStock.Rows[e.RowIndex].Cells[5].Value.ToString()) + " WHERE id LIKE '" + dgvStock.Rows[e.RowIndex].Cells[1].Value.ToString() + "'", cn);
                _ = cmd.ExecuteNonQuery();
                cn.Close();
            }
        }
    }
}
