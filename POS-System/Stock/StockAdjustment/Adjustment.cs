﻿using POS_System.Common;
using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.Stock.StockAdjustment
{
    public partial class Adjustment : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly MainForm main;
        public string _pcode;
        private decimal _qty = 0;
        public Adjustment(MainForm main)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            dgvAdjustment.Columns[3].Width = 250;
            dgvAdjustment.Columns[9].Width = 30;
            ReferenceNo();
            LoadStock(false);
            this.main = main;
            lblusername.Text = main.lbluname.Text;
        }
        public void ReferenceNo()
        {
            Random rnd = new Random();
            lblrefno.Text = rnd.Next().ToString();
        }
        public void LoadStock(bool isNoti)
        {
            int i = 0;
            dgvAdjustment.Rows.Clear();
            cn.Open();
            cmd = isNoti == false
                ? new SQLiteCommand("SELECT p.pcode, p.barcode, p.pdesc, b.brand, c.category, p.actualprice, p.price, p.qty, p.unit FROM tblProduct AS p INNER JOIN tblBrand AS b ON b.id=p.bid INNER JOIN tblCategory AS c ON c.id=p.cid WHERE p.isactive = 1 AND p.pdesc || p.pcode || p.barcode || b.brand || c.category LIKE '%" + txtSearch.Text + "%' AND p.qty <> 0", cn)
                : new SQLiteCommand("SELECT p.pcode, p.barcode, p.pdesc, b.brand, c.category, p.actualprice, p.price, p.qty, p.unit FROM tblProduct AS p INNER JOIN tblBrand AS b ON b.id=p.bid INNER JOIN tblCategory AS c ON c.id=p.cid WHERE p.isactive = 1 AND p.pdesc || p.pcode || p.barcode || b.brand || c.category LIKE '%" + txtSearch.Text + "%' AND p.qty <> 0 AND p.pcode = '" + _pcode + "'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                string a = dr[0].ToString();
                string b = dr[1].ToString();
                string c = dr[2].ToString();
                string d = dr[3].ToString();
                string e = dr[4].ToString();
                string f = decimal.Parse(dr[5].ToString()).ToString("0.00");
                string g = decimal.Parse(dr[6].ToString()).ToString("0.00");
                string h = dr[7].ToString();
                string j = dr[8].ToString();

                i++;
                _ = dgvAdjustment.Rows.Add(i, a, b, c, d, e, f, g, h, j);
            }
            dr.Close();
            cn.Close();
        }


        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadStock(false);
        }

        public void Clear()
        {
            txtpcode.Clear();
            txtcat.Clear();
            txtbrand.Clear();
            txtproduct.Clear();
            txtqty.Clear();
            txtremark.Clear();
            txtactualprice.Clear();
            txtsellingprice.Clear();
            cmbaction.Text = "";
            ReferenceNo();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (cmbaction.Text == "")
                {
                    _ = MessageBox.Show("Please select action for add or reduce.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    _ = cmbaction.Focus();
                    return;
                }
                if (txtqty.Text == "")
                {
                    _ = MessageBox.Show("Please input quantity for add or reduce.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    _ = txtqty.Focus();
                    return;
                }
                if (cmbaction.Text == "Remove From Inventory")
                {
                    if (decimal.Parse(txtqty.Text) > _qty)
                    {
                        _ = MessageBox.Show("Stock on hand quantity should be grater than adjustment quantity.", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    }
                    else
                    {
                        dbCon.ExecuteQuery("UPDATE tblProduct SET qty=(qty-" + decimal.Parse(txtqty.Text) + "), actualprice=" + decimal.Parse(txtactualprice.Text) + ",price=" + decimal.Parse(txtsellingprice.Text) + " WHERE pcode LIKE '" + txtpcode.Text + "'");
                        InsertAdjustment();
                    }

                }
                else if (cmbaction.Text == "Add To Inventory")
                {
                    dbCon.ExecuteQuery("UPDATE tblProduct SET qty=(qty+" + decimal.Parse(txtqty.Text) + "), actualprice=" + decimal.Parse(txtactualprice.Text) + ",price=" + decimal.Parse(txtsellingprice.Text) + "" +
                        " WHERE pcode LIKE '" + txtpcode.Text + "'");
                    InsertAdjustment();
                }

                cn.Open();
                SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM vwCriticalItems", cn);

                object Exist = query.ExecuteScalar();
                cn.Close();
                if (Convert.ToInt32(Exist) > 0)
                {
                    NotificationSingle noti = new NotificationSingle(main);
                    noti.ShowAlert(Exist + " notifications received");
                    main.pictureBox3.Image = POS_System.Properties.Resources.notification_receive;
                }
                else
                {
                    main.pictureBox3.Image = POS_System.Properties.Resources.notification;
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void InsertAdjustment()
        {
            dbCon.ExecuteQuery("INSERT into tblAdjustment(referenceno,pcode,qty,action,remarks,sdate,user,actprice,sellprice) VALUES ('" + lblrefno.Text + "','" + txtpcode.Text + "','" + decimal.Parse(txtqty.Text) + "','" + cmbaction.Text + "','" + txtremark.Text + "',datetime('now','localtime'),'" + lblusername.Text + "','" + decimal.Parse(txtactualprice.Text) + "','" + decimal.Parse(txtsellingprice.Text) + "')");
            _ = MessageBox.Show("Stock has been successfully adjusted.", "Process completed", MessageBoxButtons.OK, MessageBoxIcon.Information);
            LoadStock(false);
            Clear();
            btnSave.Enabled = false;
            _ = dgvAdjustment.Focus();
        }

        private void dgvAdjustment_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvAdjustment.CurrentRow.Index;
            //_pcode = dgvAdjustment[1, i].Value.ToString();
            txtpcode.Text = dgvAdjustment[1, i].Value.ToString();
            txtproduct.Text = dgvAdjustment[3, i].Value.ToString();
            txtbrand.Text = dgvAdjustment[4, i].Value.ToString();
            txtcat.Text = dgvAdjustment[5, i].Value.ToString();
            txtactualprice.Text = dgvAdjustment[6, i].Value.ToString();
            txtsellingprice.Text = dgvAdjustment[7, i].Value.ToString();
            _qty = decimal.Parse(dgvAdjustment[8, i].Value.ToString());
            btnSave.Enabled = true;
        }

        private void Adjustment_Load(object sender, EventArgs e)
        {
            dgvAdjustment.ClearSelection();
            txtpcode.Clear();
            txtcat.Clear();
            txtbrand.Clear();
            txtproduct.Clear();
            txtactualprice.Clear();
            txtsellingprice.Clear();
            btnSave.Enabled = false;
        }

        private void txtqty_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _ = txtremark.Focus();
            }
        }

        private void txtremark_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.PerformClick();
            }
        }

        private void dgvAdjustment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _ = cmbaction.Focus();
                cmbaction.SelectedIndex = 0;
                e.Handled = true;
            }
        }

        private void dgvAdjustment_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            _ = cmbaction.Focus();
            cmbaction.SelectedIndex = 0;
        }

        private void Adjustment_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
            {
                _ = txtSearch.Focus();
            }
            if (e.KeyCode == Keys.Escape)
            {
                txtSearch.Clear();
                cmbaction.SelectedIndex = -1;
                txtqty.Clear();
                txtremark.Clear();
                _ = dgvAdjustment.Focus();
            }
        }
    }
}
