﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.IO;
using System.Windows.Forms;
using Zen.Barcode;

namespace POS_System.Settings.BarcodeGenerator
{
    public partial class Barcode : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private string fname;
        private readonly MainForm mf = new MainForm();
        public Barcode()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadProductData();
            dgvBarcode.Columns[9].Width = 25;
        }
        public void LoadProductData()
        {
            int i = 0;
            dgvBarcode.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT p.pcode, p.barcode, p.pdesc, b.brand, c.category, p.price, p.unit, p.reorder FROM tblProduct AS p INNER JOIN tblBrand AS b ON b.id=p.bid INNER JOIN tblCategory AS c ON c.id=p.cid WHERE isactive = 1 AND p.pdesc || b.brand || c.category || p.pcode || p.barcode LIKE '%" + txtSearch.Text + "%'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvBarcode.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), double.Parse(dr[5].ToString()).ToString("0.00"), dr[6].ToString(), dr[7].ToString());
            }
            dr.Close();
            cn.Close();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadProductData();
        }

        private void dgvBarcode_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvBarcode.Columns[e.ColumnIndex].Name;
            if (colName == "Select")
            {
                Code128BarcodeDraw barcode = BarcodeDrawFactory.Code128WithChecksum;
                if (dgvBarcode.Rows[e.RowIndex].Cells[2].Value.ToString() != string.Empty)
                {
                    string barcodeNumber = dgvBarcode.Rows[e.RowIndex].Cells[2].Value.ToString();
                    string pdesc = dgvBarcode.Rows[e.RowIndex].Cells[3].Value.ToString();
                    string price = dgvBarcode.Rows[e.RowIndex].Cells[6].Value.ToString();

                    BarcodeGen(barcode, barcodeNumber, price, pdesc);
                }
                else
                {
                    picBarcode.Image = null;
                    //lblbarcode.Text = string.Empty;
                }
                fname = dgvBarcode.Rows[e.RowIndex].Cells[1].Value.ToString();
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            SaveFileDialog file = new SaveFileDialog
            {
                Title = "Save Barcode Image As",
                FileName = fname,
                Filter = "Image File(*.jpg,*.png)| *.jpg, *.png"
            };
            ImageFormat format = ImageFormat.Png;
            if (file.ShowDialog() == DialogResult.OK)
            {
                string ftype = Path.GetExtension(file.FileName);
                switch (ftype)
                {
                    case ".jpg":
                        format = ImageFormat.Jpeg;
                        break;
                    case ".png":
                        format = ImageFormat.Png;
                        break;
                    default:
                        break;
                }
                picBarcode.Image.Save(file.FileName, format);
            }
        }

        private void dgvBarcode_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvBarcode.CurrentRow.Index;
            Code128BarcodeDraw barcode = BarcodeDrawFactory.Code128WithChecksum;
            if (dgvBarcode[2, i].Value.ToString() != string.Empty)
            {
                string barcodeNumber = dgvBarcode[2, i].Value.ToString();
                string pdesc = dgvBarcode[3, i].Value.ToString();
                string price = dgvBarcode[6, i].Value.ToString();

                BarcodeGen(barcode, barcodeNumber, price, pdesc);
            }
            else
            {
                picBarcode.Image = null;
                //lblbarcode.Text = string.Empty;
            }
            fname = dgvBarcode[2, i].Value.ToString();
        }
        public void BarcodeGen(Code128BarcodeDraw barcode, string barcodeNumber, string price, string pdesc)
        {
            picBarcode.Image = barcode.Draw(barcodeNumber, 60, 2);

            Bitmap bitmap = new Bitmap(picBarcode.Image.Width, picBarcode.Image.Height + 50);
            Font textFont = new Font("Arial", 12, FontStyle.Regular, GraphicsUnit.Pixel);
            using (Graphics g = Graphics.FromImage(bitmap))
            {
                float bNumTextX = (picBarcode.Image.Width - g.MeasureString(barcodeNumber, textFont).Width) / 2;
                float priceTextX = (picBarcode.Image.Width - g.MeasureString(price, textFont).Width) / 2;
                float pdescTextX = (picBarcode.Image.Width - g.MeasureString(pdesc, textFont).Width) / 2;

                g.DrawString(pdesc, textFont, Brushes.Black, new PointF(pdescTextX, 5));
                g.DrawImage(picBarcode.Image, new Point(0, 20));
                g.DrawString(barcodeNumber, textFont, Brushes.Black, new PointF(bNumTextX, picBarcode.Image.Height + 20));
                g.DrawString(price, textFont, Brushes.Black, new PointF(priceTextX, picBarcode.Image.Height + 35
                    ));
            }
            picBarcode.Image = bitmap;
        }

        private void btnprint_Click(object sender, EventArgs e)
        {
            PrintDialog pd = new PrintDialog();
            PrintDocument doc = new PrintDocument();
            doc.PrintPage += Doc_PrintPage;
            pd.Document = doc;
            if (pd.ShowDialog() == DialogResult.OK)
            {
                doc.Print();
            }
        }

        private void Doc_PrintPage(object sender, PrintPageEventArgs e)
        {
            Bitmap bmp = new Bitmap(picBarcode.Width, picBarcode.Height);
            picBarcode.DrawToBitmap(bmp, new Rectangle(0, 0, picBarcode.Width, picBarcode.Height));
            e.Graphics.DrawImage(bmp, 0, 0);
            bmp.Dispose();
        }

        private void Barcode_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.F8)
            {
                _ = txtSearch.Focus();
            }
            if (e.KeyCode == Keys.Escape)
            {
                txtSearch.Clear();
            }
        }
    }
}
