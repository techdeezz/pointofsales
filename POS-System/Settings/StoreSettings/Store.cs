﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace POS_System.Settings.StoreSettings
{
    public partial class Store : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private bool isStoreInfo = false;
        private string imageLocation = null;
        public Store()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadStore();
        }
        private static Regex email_validation()
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(pattern, RegexOptions.IgnoreCase);
        }

        private static readonly Regex validate_emailaddress = email_validation();
        public void LoadStore()
        {
            try
            {
                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM tblStore", cn);
                dr = cmd.ExecuteReader();
                _ = dr.Read();
                if (dr.HasRows)
                {
                    isStoreInfo = true;
                    txtStrName.Text = dr["store"].ToString();
                    txtAddress.Text = dr["address"].ToString();
                    txtmobile.Text = dr["mobile"].ToString();
                    txtemail.Text = dr["email"].ToString();
                    bkprint.Checked = dr["bookfield"].ToString() == "1";
                    paymentprint.Checked = dr["paymentfiled"].ToString() == "1";
                    pbStoreimage.Image = dr["filename"].ToString() != string.Empty ? Image.FromFile(Application.StartupPath + @"\logo\" + dr["filename"].ToString()) : Image.FromFile(Application.StartupPath + @"\logo\DefaultLogo.bmp");
                }
                else
                {
                    txtStrName.Clear();
                    txtAddress.Clear();
                    txtmobile.Clear();
                    txtemail.Clear();
                    pbStoreimage.Image = Image.FromFile(Application.StartupPath + @"\logo\DefaultLogo.bmp");
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtStrName.Text == string.Empty)
                {
                    _ = MessageBox.Show("Please enter valid name for your store!!", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                    _ = txtStrName.Focus();
                    txtStrName.Select();
                    return;
                }
                if ((txtmobile.Text.Length == 10 || string.IsNullOrWhiteSpace(txtmobile.Text)) &&
                    (validate_emailaddress.IsMatch(txtemail.Text) == true || string.IsNullOrWhiteSpace(txtemail.Text)))
                {
                    int bkprintopt = bkprint.Checked ? 1 : 0;
                    int paymentprintopt = paymentprint.Checked ? 1 : 0;
                    if (imageLocation != null)
                    {
                        if (MessageBox.Show("Are you sure to save store details", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            File.Copy(imageLocation, Path.Combine(Application.StartupPath + @"\logo\", Path.GetFileName(imageLocation)), true);
                            if (isStoreInfo)
                            {
                                dbCon.ExecuteQuery("UPDATE tblStore SET store='" + txtStrName.Text + "', address='" + txtAddress.Text + "', filename='" + Path.GetFileName(imageLocation) + "', mobile='" + txtmobile.Text + "' , email='" + txtemail.Text + "', bookfield='" + bkprintopt + "',paymentfiled='" + paymentprintopt + "'");
                            }
                            else
                            {
                                dbCon.ExecuteQuery("INSERT into tblStore (store,address,filename,mobile,email,bookfield,paymentfiled) VALUES ('" + txtStrName.Text + "','" + txtAddress.Text + "','" + Path.GetFileName(imageLocation) + "','" + txtmobile.Text + "', '" + txtemail.Text + "','" + bkprintopt + "','" + paymentprintopt + "')");
                            }
                            _ = MessageBox.Show("Store details saved successfully!!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LoadStore();
                        }
                    }
                    else
                    {
                        if (MessageBox.Show("Are you sure to save store details", "Confirm", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            if (isStoreInfo)
                            {
                                dbCon.ExecuteQuery("UPDATE tblStore SET store='" + txtStrName.Text + "', address='" + txtAddress.Text + "',mobile='" + txtmobile.Text + "' ,email='" + txtemail.Text + "', bookfield='" + bkprintopt + "',paymentfiled='" + paymentprintopt + "'");
                            }
                            else
                            {
                                dbCon.ExecuteQuery("INSERT into tblStore (store,address,mobile,email,bookfield,paymentfiled) VALUES ('" + txtStrName.Text + "','" + txtAddress.Text + "','" + txtmobile.Text + "', '" + txtemail.Text + "','" + bkprintopt + "','" + paymentprintopt + "')");
                            }
                            _ = MessageBox.Show("Store details saved successfully!!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            LoadStore();
                        }
                    }
                }
                else
                {
                    if (txtmobile.Text.Length < 10 && !string.IsNullOrWhiteSpace(txtmobile.Text))
                    {
                        _ = MessageBox.Show("Mobile number should be 10 digits", "Validation not match", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtmobile.Focus();
                    }
                    else if (validate_emailaddress.IsMatch(txtemail.Text) != true && !string.IsNullOrWhiteSpace(txtemail.Text))
                    {
                        _ = MessageBox.Show("Invalid Email Address!", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtemail.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtStrName_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.PerformClick();
            }
        }

        private void txtAddress_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnSave.PerformClick();
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                OpenFileDialog dialog = new OpenFileDialog
                {
                    Filter = "Image files(*.jpg; *.jpeg; *.bmp; *.png;)|*.jpg; *.jpeg; *.bmp; *.png"
                };
                if (dialog.ShowDialog() == DialogResult.OK)
                {
                    imageLocation = dialog.FileName;
                    pbStoreimage.Image = new Bitmap(imageLocation);
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void pbStoreimage_MouseClick(object sender, MouseEventArgs e)
        {
            button1.PerformClick();
        }

        private void txtmobile_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            if (char.IsDigit(e.KeyChar))
            {
                if ((sender as TextBox).Text.Count(char.IsDigit) >= 10)
                {
                    e.Handled = true;
                }
            }
            if (e.KeyChar == (char)13)
            {
                btnSave.PerformClick();
            }
        }
    }
}
