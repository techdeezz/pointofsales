﻿using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.Settings.UserAccount.Property
{
    public partial class UserProperties : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly UserAccount userAccount;
        public string username;
        public UserProperties(UserAccount userAccount)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            this.userAccount = userAccount;
            _ = txtname.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            try
            {
                cn.Open();
                cmd = new SQLiteCommand("UPDATE tblUser SET name=@name, role=@role, isactive=@isactive WHERE username='" + username + "'", cn);
                _ = cmd.Parameters.AddWithValue("@name", txtname.Text);
                _ = cmd.Parameters.AddWithValue("@role", cmbRole.Text);
                _ = cmd.Parameters.AddWithValue("@isactive", cmbstatus.Text == "True" ? 1 : 0);
                _ = cmd.ExecuteNonQuery();
                cn.Close();
                _ = MessageBox.Show("Account properties has been successfully updated", "Update properties", MessageBoxButtons.OK, MessageBoxIcon.Information);
                userAccount.LoadUsers();
                Dispose();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void txtname_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnApply.PerformClick();
            }
        }

        private void UserProperties_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Dispose();
            }
        }
    }
}
