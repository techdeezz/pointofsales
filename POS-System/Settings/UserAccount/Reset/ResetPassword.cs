﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System.Settings.UserAccount.Reset
{
    public partial class ResetPassword : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private readonly SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private Point mouseOffset;
        private readonly UserAccount user;
        public ResetPassword(UserAccount user)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            this.user = user;
            lblusername.Text = user.username;
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void ResetPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel.PerformClick();
            }
        }

        private void txtnewpass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                _ = txtnewpass.Focus();
            }
        }

        private void txtConfirmPass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnChange.PerformClick();
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {

            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void btnChange_Click(object sender, EventArgs e)
        {
            if (txtnewpass.Text != txtnewpass.Text)
            {
                _ = MessageBox.Show("New password and confirm password are not matched please try again!!", "Invalid password", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _ = txtnewpass.Focus();
                return;
            }
            else
            {
                if (MessageBox.Show("Are you sure to reset password?", "Reset password", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    dbCon.ExecuteQuery("UPDATE tblUser SET password ='" + txtnewpass.Text + "' WHERE username='" + user.username.ToUpper() + "'");
                    _ = MessageBox.Show("Password has been changed successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Dispose();
                }
            }
        }
    }
}
