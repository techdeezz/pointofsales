﻿using POS_System.Settings.UserAccount.Property;
using POS_System.Settings.UserAccount.Reset;
using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.Settings.UserAccount
{
    public partial class UserAccount : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly MainForm main;
        private readonly string sTitle = "Point Of Sale";
        public string username;
        private string name;
        private string role;
        private string accstatus;

        public UserAccount(MainForm main)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            metroTabControl1.SelectedTab = metroTabPage1;
            _ = txtusername.Focus();
            txtusername.Select();
            this.main = main;
            LoadUsers();
        }
        public void Clear()
        {
            txtFullname.Clear();
            txtPassword.Clear();
            txtConfirmPass.Clear();
            cmbRole.SelectedItem = null;
            txtusername.Clear();
            _ = txtusername.Focus();
        }
        public void LoadUsers()
        {
            int i = 0;
            dgvUser.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblUser", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvUser.Rows.Add(i, dr[1].ToString(), dr[4].ToString(), int.Parse(dr[5].ToString()) == 1 ? "True" : "False", dr[3].ToString());
            }
            dr.Close();
            cn.Close();
        }

        public bool CheckUserName(string username)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblUser WHERE username = @username", cn);

            _ = query.Parameters.AddWithValue("@username", username.ToUpper());
            object Exist = query.ExecuteScalar();
            cn.Close();

            return Convert.ToInt32(Exist) > 0;
        }

        private void btnAccSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                    !string.IsNullOrWhiteSpace(txtFullname.Text) &&
                    !string.IsNullOrWhiteSpace(txtPassword.Text) &&
                    !string.IsNullOrWhiteSpace(txtConfirmPass.Text) &&
                    !string.IsNullOrWhiteSpace(txtusername.Text) &&
                    (cmbRole.Text != null || cmbRole.Text != "") &&
                    txtPassword.Text == txtConfirmPass.Text
                    )
                {
                    if (!CheckUserName(txtusername.Text))
                    {
                        if (MessageBox.Show("Are you sure want to save this record?", "Insert Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            cn.Open();
                            cmd = new SQLiteCommand("INSERT INTO tblUser(username,password,role,name)VALUES(@username,@password,@role,@name)", cn);

                            _ = cmd.Parameters.AddWithValue("@username", txtusername.Text.ToUpper());
                            _ = cmd.Parameters.AddWithValue("@password", txtPassword.Text);
                            _ = cmd.Parameters.AddWithValue("@role", cmbRole.Text);
                            _ = cmd.Parameters.AddWithValue("@name", txtFullname.Text);

                            _ = cmd.ExecuteNonQuery();
                            cn.Close();
                            _ = MessageBox.Show("Record has been successfully saved", sTitle);
                            Clear();
                            LoadUsers();
                        }
                    }
                    else
                    {
                        _ = MessageBox.Show("Username is already exist in database", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(txtusername.Text))
                    {
                        _ = MessageBox.Show("Please enter username", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtusername.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtPassword.Text))
                    {
                        _ = MessageBox.Show("Please enter password", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPassword.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtConfirmPass.Text))
                    {
                        _ = MessageBox.Show("Please enter confirm password", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtConfirmPass.Focus();
                    }
                    else if (txtPassword.Text != txtConfirmPass.Text)
                    {
                        _ = MessageBox.Show("Password and confirm password not match", "Validation error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtConfirmPass.Focus();
                    }
                    else if (cmbRole.Text == null || cmbRole.Text == "")
                    {
                        _ = MessageBox.Show("Please select role", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = cmbRole.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtFullname.Text))
                    {
                        _ = MessageBox.Show("Please enter full name", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtFullname.Focus();
                    }
                    else
                    {
                        _ = MessageBox.Show("Record has not been successfully saved", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtusername.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning");
            }
        }

        private void btnAccCancel_Click(object sender, EventArgs e)
        {
            Clear();
        }

        private void UserAccount_Load(object sender, EventArgs e)
        {
            lblusername.Text = main.lbluname.Text;
        }

        private void txtusername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnAccSave.PerformClick();
            }
        }

        private void txtPassword_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnAccSave.PerformClick();
            }
        }

        private void txtConfirmPass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnAccSave.PerformClick();
            }
        }

        private void cmbRole_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnAccSave.PerformClick();
            }
        }

        private void txtFullname_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnAccSave.PerformClick();
            }
        }

        private void btnPassSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (txtCurrentpass.Text != main._pass)
                {
                    _ = MessageBox.Show("Current password did not match!", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _ = txtCurrentpass.Focus();
                    txtCurrentpass.Select();
                    return;
                }
                if (txtNewpass.Text != txtRepass.Text)
                {
                    _ = MessageBox.Show("New password and confirm password are not match!", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _ = txtNewpass.Focus();
                    txtNewpass.Select();
                    return;
                }
                dbCon.ExecuteQuery("UPDATE tblUser SET password ='" + txtNewpass.Text + "' WHERE username='" + lblusername.Text.ToUpper() + "'");
                _ = MessageBox.Show("Password has been changed successfully!", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                ClearCP();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnPassCancel_Click(object sender, EventArgs e)
        {
            ClearCP();
            _ = txtCurrentpass.Focus();
        }
        public void ClearCP()
        {
            txtCurrentpass.Clear();
            txtNewpass.Clear();
            txtRepass.Clear();
        }

        private void txtCurrentpass_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnPassSave.PerformClick();
            }
        }

        private void btnPassSave_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnPassSave.PerformClick();
            }
        }

        private void metroTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (metroTabPage1.Focus())
            {
                _ = txtusername.Focus();
            }
            else if (metroTabPage2.Focus())
            {
                _ = txtCurrentpass.Focus();
            }
            else if (metroTabPage3.Focus())
            {
                btnRemove.Enabled = false;
                btnresetpass.Enabled = false;
            }
        }

        private void dgvUser_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvUser.CurrentRow.Index;
            username = dgvUser[1, i].Value.ToString();
            name = dgvUser[2, i].Value.ToString();
            role = dgvUser[4, i].Value.ToString();
            accstatus = dgvUser[3, i].Value.ToString();
            if (lblusername.Text.ToUpper() == username)
            {
                btnRemove.Enabled = false;
                btnresetpass.Enabled = false;
            }
            else
            {
                btnRemove.Enabled = true;
                btnresetpass.Enabled = true;
            }
            gbUser.Text = "Change Password For " + username;
            lblAccNote.Text = "To change the password for " + username + ", click Reset Password";
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure to remove this user from your system?", "User account", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                dbCon.ExecuteQuery("DELETE FROM tblUser WHERE username = '" + username + "'");
                _ = MessageBox.Show("Account has been successfully removed", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                LoadUsers();
            }
        }

        private void btnresetpass_Click(object sender, EventArgs e)
        {
            ResetPassword reset = new ResetPassword(this);
            _ = reset.ShowDialog();
        }

        private void btnProperty_Click(object sender, EventArgs e)
        {
            UserProperties property = new UserProperties(this)
            {
                Text = name + "\\" + username + " Properties"
            };
            property.txtname.Text = name;
            property.cmbRole.Text = role;
            property.cmbstatus.Text = accstatus;
            property.username = username;
            _ = property.ShowDialog();
        }

        private void UserAccount_KeyDown(object sender, KeyEventArgs e)
        {
            if (metroTabControl1.SelectedTab == metroTabControl1.TabPages["metroTabPage1"])
            {
                if (e.KeyCode == Keys.Escape)
                {
                    btnAccCancel.PerformClick();
                }
            }
            else if (metroTabControl1.SelectedTab == metroTabControl1.TabPages["metroTabPage2"])
            {
                if (e.KeyCode == Keys.Escape)
                {
                    btnPassCancel.PerformClick();
                }
            }
        }
    }
}
