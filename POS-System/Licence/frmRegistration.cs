﻿using LicenseApp.License;
using POS_System.LoginModule;
using System;
using System.Windows.Forms;

namespace POS_System
{
    public partial class frmRegistration : Form
    {
        public frmRegistration()
        {
            InitializeComponent();
            _ = txtProductKey.Focus();
            txtProductKey.Select();
        }

        private const int ProductCode = 1;
        private void btnReg_Click(object sender, EventArgs e)
        {
            KeyManager key = new KeyManager(txtProductId.Text);
            string productKey = txtProductKey.Text;
            if (key.ValidKey(ref productKey))
            {
                KeyValuesClass kv = new KeyValuesClass();
                if (key.DisassembleKey(productKey, ref kv))
                {
                    LicenseInfo lic = new LicenseInfo
                    {
                        ProductKey = productKey,
                        FullName = "V4Codes"
                    };
                    if (kv.Type == LicenseType.TRIAL)
                    {
                        lic.Day = kv.Expiration.Day;
                        lic.Month = kv.Expiration.Month;
                        lic.Year = kv.Expiration.Year;
                    }
                    _ = key.SaveSuretyFile(string.Format(@"{0}\libdb\Key.lic", Application.StartupPath), lic);
                    _ = MessageBox.Show("You have been successfully registered", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    Hide();
                    Login log = new Login();
                    _ = log.ShowDialog();
                }
            }
            else
            {
                _ = MessageBox.Show("Product key is invalid please try again..", "Error Message", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmRegistration_Load(object sender, EventArgs e)
        {
            txtProductId.Text = ComputerInfo.GetComputerId();
        }

        private void txtProductKey_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnReg.PerformClick();
            }
        }
    }
}
