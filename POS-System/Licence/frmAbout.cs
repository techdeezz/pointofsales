﻿using LicenseApp.License;
using System;
using System.Windows.Forms;

namespace POS_System
{
    public partial class frmAbout : Form
    {
        public frmAbout()
        {
            InitializeComponent();
        }

        private const int ProductCode = 1;
        private void btnOk_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void frmAbout_Load(object sender, EventArgs e)
        {
            contactUs.Text = "Ramzy Ahmed,            \x0A 0767575917, \x0A ramzyahamed96@gmail.com";
            contactUs.ReadOnly = true;
            contactUs.BorderStyle = 0;
            contactUs.BackColor = BackColor;
            contactUs.TabStop = false;
            contactUs.Multiline = true;
            lblProdctId.Text = ComputerInfo.GetComputerId();
            KeyManager key = new KeyManager(lblProdctId.Text);
            LicenseInfo lic = new LicenseInfo();
            _ = key.LoadSuretyFile(string.Format(@"{0}\libdb\Key.lic", Application.StartupPath), ref lic);
            string productKey = lic.ProductKey;
            if (key.ValidKey(ref productKey))
            {
                KeyValuesClass kv = new KeyValuesClass();
                if (key.DisassembleKey(productKey, ref kv))
                {
                    lblProductName.Text = "V4Codes";
                    lblProductKey.Text = productKey;
                    lblLicenceType.Text = kv.Type == LicenseType.TRIAL ? string.Format("{0} days", (kv.Expiration - DateTime.Now.Date).Days) : "Full";
                }
            }

        }
    }
}
