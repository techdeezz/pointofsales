﻿using POS_System.Common;
using POS_System.Customer;
using POS_System.Dashboard;
using POS_System.LoginModule;
using POS_System.POS;
using POS_System.POS.Book_keeping;
using POS_System.POS.Report;
using POS_System.Record;
using POS_System.Settings.BarcodeGenerator;
using POS_System.Settings.StoreSettings;
using POS_System.Settings.UserAccount;
using POS_System.Stock;
using POS_System.Stock.StockAdjustment;
using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System
{
    public partial class MainForm : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        public string _pass;
        public MainForm()
        {
            InitializeComponent();
            customizeDesign();
            cn = new SQLiteConnection(dbCon.myConnection());
            MinimumSize = new System.Drawing.Size(Screen.PrimaryScreen.Bounds.Size.Width, Screen.PrimaryScreen.Bounds.Size.Height);
            MaximumSize = new System.Drawing.Size(Screen.PrimaryScreen.Bounds.Size.Width, Screen.PrimaryScreen.Bounds.Size.Height);
            //cn.Open();
        }

        #region PanelSlide
        private void customizeDesign()
        {
            panelSubProduct.Visible = false;
            panelSubRecord.Visible = false;
            panelSubStock.Visible = false;
            panelSubSettings.Visible = false;
            panelSubRecord.Visible = false;
        }
        private void Images(Button btn)
        {
            btn.Image = Properties.Resources.Arrow_20px;
            btn.ImageAlign = ContentAlignment.MiddleRight;
        }
        private void CImages(Button btn)
        {
            btn.Image = Properties.Resources.CArrow_20px;
            btn.ImageAlign = ContentAlignment.MiddleRight;
        }
        private void hideSubMenu()
        {
            if (panelSubProduct.Visible == true)
            {
                panelSubProduct.Visible = false;
                btnProduct.Image = Properties.Resources.Arrow_20px;
            }
            if (panelSubRecord.Visible == true)
            {
                panelSubRecord.Visible = false;
                btnRecords.Image = Properties.Resources.Arrow_20px;
            }
            if (panelSubSettings.Visible == true)
            {
                panelSubSettings.Visible = false;
                btnSetting.Image = Properties.Resources.Arrow_20px;
            }
            if (panelSubStock.Visible == true)
            {
                panelSubStock.Visible = false;
                btnInStock.Image = Properties.Resources.Arrow_20px;
            }
            if (panelVendors.Visible == true)
            {
                panelVendors.Visible = false;
                btnVendors.Image = Properties.Resources.Arrow_20px;
            }

        }
        private void showSubMenu(Panel submenu, Button btn)
        {
            if (submenu.Visible == false)
            {
                hideSubMenu();
                CImages(btn);
                submenu.Visible = true;
            }
            else
            {
                Images(btn);
                submenu.Visible = false;
            }
        }
        #endregion PanelSlide


        //public void CloseOthers(Form form)
        //{
        //    List<Form> openForms = new List<Form>();

        //    foreach (Form f in Application.OpenForms)
        //    {
        //        openForms.Add(f);
        //    }

        //    foreach (Form f in openForms)
        //    {
        //        if (f.Name != "MainForm" && f.Name != form.Name)
        //            f.Close();
        //    }
        //}
        //public void ChangeBtnClr(Button button)
        //{
        //    button.BackColor = Color.FromArgb(43, 57, 79);
        //}
        //public void ClearBtnClr(Button button)
        //{
        //    button.BackColor = Color.FromArgb(51, 71, 58);
        //}

        private Form activeForm = null;
        public void openChildrenForm(Form childrenForm, Button button)
        {

            activeForm?.Close();
            activeForm = childrenForm;
            childrenForm.TopLevel = false;
            childrenForm.FormBorderStyle = FormBorderStyle.None;
            childrenForm.Dock = DockStyle.Fill;
            lblTitle.Text = childrenForm.Text;
            panelMain.Controls.Add(childrenForm);
            panelMain.Tag = childrenForm;

            btnBrand.BackColor = Color.FromArgb(51, 71, 58);
            btnCategory.BackColor = Color.FromArgb(51, 71, 58);
            btnProductList.BackColor = Color.FromArgb(51, 71, 58);
            btnStockEntry.BackColor = Color.FromArgb(51, 71, 58);
            btnStockAdjustment.BackColor = Color.FromArgb(51, 71, 58);
            btnSalesHistory.BackColor = Color.FromArgb(51, 71, 58);
            btnPosRecord.BackColor = Color.FromArgb(51, 71, 58);
            btnbookkeeping.BackColor = Color.FromArgb(51, 71, 58);

            //btnSupplier.BackColor = Color.DarkSlateGray;
            btnDashboard.BackColor = Color.DarkSlateGray;
            btnUser.BackColor = Color.FromArgb(51, 71, 58);
            btnStore.BackColor = Color.FromArgb(51, 71, 58);
            btnbarcodegen.BackColor = Color.FromArgb(51, 71, 58);
            btnCust.BackColor = Color.FromArgb(51, 71, 58);
            btnSupp.BackColor = Color.FromArgb(51, 71, 58);

            button.BackColor = Color.FromArgb(43, 57, 79);
            childrenForm.BringToFront();
            childrenForm.Show();
        }
        private void btnDashboard_Click(object sender, EventArgs e)
        {
            MainDashboard dash = new MainDashboard();
            openChildrenForm(dash, btnDashboard);
            hideSubMenu();
        }

        private void btnProduct_Click(object sender, EventArgs e)
        {
            showSubMenu(panelSubProduct, btnProduct);
        }

        private void btnProductList_Click(object sender, EventArgs e)
        {
            Products prd = new Products();
            openChildrenForm(prd, btnProductList);
            prd.dgvProduct.Select();
            //hideSubMenu();
        }

        private void btnCategory_Click(object sender, EventArgs e)
        {
            Category cat = new Category();
            openChildrenForm(cat, btnCategory);
            cat.dgvCategory.Select();
            //ChangeBtnClr(btnCategory);
            //hideSubMenu();
        }

        private void btnBrand_Click(object sender, EventArgs e)
        {
            Brands brd = new Brands();
            openChildrenForm(brd, btnBrand);
            brd.dgvBrand.Select();
            //ChangeBtnClr(btnBrand);
            //hideSubMenu();
        }

        private void btnInStock_Click(object sender, EventArgs e)
        {
            showSubMenu(panelSubStock, btnInStock);
        }

        private void btnStockEntry_Click(object sender, EventArgs e)
        {
            //hideSubMenu();
            StockIn stck = new StockIn(this);
            openChildrenForm(stck, btnStockEntry);
            _ = stck.dgvStock.Focus();
        }

        private void btnStockAdjustment_Click(object sender, EventArgs e)
        {
            //hideSubMenu();
            Adjustment adj = new Adjustment(this);
            openChildrenForm(adj, btnStockAdjustment);
            _ = adj.dgvAdjustment.Focus();
        }

        private void btnRecords_Click(object sender, EventArgs e)
        {
            showSubMenu(panelSubRecord, btnRecords);
        }

        private void btnSalesHistory_Click(object sender, EventArgs e)
        {
            //hideSubMenu();
            DailySales sales = new DailySales
            {
                soldUser = lbluname.Text
            };
            openChildrenForm(sales, btnSalesHistory);
            _ = sales.txtSearch.Focus();
        }

        private void btnPosRecord_Click(object sender, EventArgs e)
        {
            //hideSubMenu();
            openChildrenForm(new POSRecord(), btnPosRecord);
        }

        private void btnSetting_Click(object sender, EventArgs e)
        {
            showSubMenu(panelSubSettings, btnSetting);
        }

        private void btnUser_Click(object sender, EventArgs e)
        {
            //hideSubMenu();
            UserAccount usr = new UserAccount(this);
            openChildrenForm(usr, btnUser);
            _ = usr.txtusername.Focus();
            //usr.dgvBrand.Select();
        }

        private void btnStore_Click(object sender, EventArgs e)
        {
            //hideSubMenu();
            Store store = new Store();
            openChildrenForm(store, btnStore);
        }

        private void btnLogout_Click(object sender, EventArgs e)
        {
            hideSubMenu();
            if (MessageBox.Show("Are sure to logout?", "Logout", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Hide();
                Login login = new Login();
                _ = login.ShowDialog();
            }
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Cashier cashier = new Cashier();
            cashier.btnchangepassword.Visible = false;
            cashier.btnlogout.Visible = false;
            cashier.label3.Text = label3.Text;
            cashier.lblusername.Text = lbluname.Text;
            cashier.Show();
        }

        private void MainForm_Load(object sender, EventArgs e)
        {
            btnDashboard.PerformClick();
            Noti();
        }
        //alert for critical items
        public void Noti()
        {
            //int i = 0;
            //cn.Open();
            //cmd = new SQLiteCommand("SELECT * FROM vwCriticalItems LIMIT 8", cn);
            //dr = cmd.ExecuteReader();
            //while (dr.Read())
            //{
            //    i++;
            //    NotificationAlert alert = new NotificationAlert(this);
            //    alert.lblpcode.Text = dr["pcode"].ToString();
            //    alert.qty = Convert.ToDecimal(dr["qty"].ToString());
            //    alert.btnreorder.Enabled = true;
            //    alert.ShowAlert(i + ". " + dr["pdesc"].ToString() + " - " + dr["qty"].ToString());
            //}
            //dr.Close();
            //cn.Close();
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM vwCriticalItems", cn);

            object Exist = query.ExecuteScalar();
            cn.Close();
            if (Convert.ToInt32(Exist) > 0)
            {
                NotificationSingle noti = new NotificationSingle(this);
                noti.ShowAlert(Exist + " notifications received");
                pictureBox3.Image = POS_System.Properties.Resources.notification_receive;
            }
            else
            {
                pictureBox3.Image = POS_System.Properties.Resources.notification;
            }
        }

        private void btnVendors_Click(object sender, EventArgs e)
        {
            showSubMenu(panelVendors, btnVendors);
        }

        private void btnbarcodegen_Click(object sender, EventArgs e)
        {
            //hideSubMenu();
            Barcode bar = new Barcode();
            openChildrenForm(bar, btnbarcodegen);
            _ = bar.dgvBarcode.Focus();
        }

        private void btnSupp_Click(object sender, EventArgs e)
        {
            //hideSubMenu();
            Suppliers sup = new Suppliers();
            openChildrenForm(sup, btnSupp);
            sup.dgvSupplier.Select();
        }

        private void btnCust_Click(object sender, EventArgs e)
        {
            Customers cust = new Customers();
            openChildrenForm(cust, btnCust);
            cust.dgvCustomer.Select();
        }

        //public bool CheckNotification()
        //{
        //    cn.Open();
        //    SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM vwCriticalItems", cn);

        //    var Exist = query.ExecuteScalar();
        //    cn.Close();

        //    if (Convert.ToInt32(Exist) > 0)
        //    {
        //        return true;
        //    }
        //    else
        //    {
        //        return false;
        //    }
        //}

        private void button2_Click(object sender, EventArgs e)
        {
            openChildrenForm(new BookKeeping(), btnbookkeeping);
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            Notifications notification = new Notifications();
            int i = 0;
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM vwCriticalItems", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                NotificationMsg alert = new NotificationMsg(this)
                {
                    TopLevel = false,
                    StartPosition = FormStartPosition.CenterParent
                };
                alert.lblpcode.Text = dr["pcode"].ToString();
                alert.qty = Convert.ToDecimal(dr["qty"].ToString());
                alert.lblmessage.Text = dr["qty"].ToString() == "0" ? i + ". " + dr["pdesc"].ToString() + " is out of stock - " + dr["qty"].ToString() : i + ". " + dr["pdesc"].ToString() + " is below your reorder level - " + dr["qty"].ToString();
                alert.pictureBox1.Image = dr["qty"].ToString() == "0" ? Properties.Resources.danger : Properties.Resources.warning;
                notification.flowLayoutPanel1.Controls.Add(alert);
                alert.Show();
            }
            dr.Close();
            cn.Close();
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM vwCriticalItems", cn);

            object Exist = query.ExecuteScalar();
            cn.Close();
            notification.label1.Visible = Convert.ToInt32(Exist) <= 0;
            notification.Show();
        }

        private void pictureBox3_MouseHover(object sender, EventArgs e)
        {
            pictureBox3.Cursor = Cursors.Hand;
        }
    }
}
