﻿using Microsoft.ReportingServices.Diagnostics.Internal;
using System;
using System.Configuration;
using System.Data;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System
{
    public class DBConnect
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private SQLiteDataReader dr;
        private string con;
        public string myConnection()
        {
            con = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            return con;
        }
        public DataTable getTable(string qry)
        {
            cn.ConnectionString = myConnection();
            cmd = new SQLiteCommand(qry, cn);
            SQLiteDataAdapter adapter = new SQLiteDataAdapter();
            DataTable dt = new DataTable();
            adapter.SelectCommand = cmd;
            _ = adapter.Fill(dt);
            return dt;
        }
        public void ExecuteQuery(string sql)
        {
            try
            {
                cn.ConnectionString = myConnection();
                cn.Open();
                cmd = new SQLiteCommand(sql, cn);
                _ = cmd.ExecuteNonQuery();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }
        public string getPassword(string username)
        {
            string password = "";
            cn.ConnectionString = myConnection();
            cn.Open();
            cmd = new SQLiteCommand("SELECT password FROM tblUser WHERE username='" + username + "'", cn);
            dr = cmd.ExecuteReader();
            _ = dr.Read();
            if (dr.HasRows)
            {
                password = dr["password"].ToString();
            }
            dr.Close();
            cn.Close();
            return password;
        }
        public double ExtractData(string sql)
        {
            cn.ConnectionString = myConnection();
            cn.Open();
            cmd = new SQLiteCommand(sql, cn);
            double data = double.Parse(cmd.ExecuteScalar().ToString());
            cn.Close();
            return data;
        }
    }
}
