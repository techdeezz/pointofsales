﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;
using static MetroFramework.Drawing.Html.CssLength;
using static System.Net.Mime.MediaTypeNames;

namespace POS_System
{
    public partial class Products : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly MainForm mf = new MainForm();
        private string id;
        public Products()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadProductData();
            dgvProduct.Columns[9].Width = 25;
            dgvProduct.Columns[10].Width = 25;
            dgvProduct.Columns[11].Width = 40;
            dgvProduct.ClearSelection();
        }

        public void LoadProductData()
        {
            int i = 0;
            dgvProduct.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT p.pcode, p.barcode, p.pdesc, b.brand, c.category, p.unit, p.reorder, p.isactive FROM tblProduct AS p INNER JOIN tblBrand AS b ON b.id=p.bid INNER JOIN tblCategory AS c ON c.id=p.cid WHERE p.pdesc || b.brand || c.category || p.pcode || p.barcode LIKE '%" + txtSearch.Text + "%'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvProduct.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), dr[5].ToString(), dr[6].ToString(), Convert.ToInt32(dr[7].ToString()));
                int imageColumnIndex = dgvProduct.Columns["Toggle"].Index;
                System.Drawing.Image active = Properties.Resources.toggle_on;
                System.Drawing.Image inactive = Properties.Resources.toggle_off;
                if (Convert.ToInt32(dr[7].ToString()) == 1)
                {
                    dgvProduct.Rows[i - 1].Cells[imageColumnIndex].Value = active;
                }
                else
                {
                    dgvProduct.Rows[i - 1].Cells[imageColumnIndex].Value = inactive;
                }
            }
            dr.Close();
            cn.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            ProductModule moduleForm = new ProductModule(this);
            _ = moduleForm.ShowDialog();
        }
        public void CellDelete(string prdcode)
        {
            cn.Open();
            SQLiteCommand querystock = new SQLiteCommand("SELECT COUNT(*) FROM tblStock WHERE pcode LIKE @pcode AND status = 'Done'", cn);

            _ = querystock.Parameters.AddWithValue("@pcode", prdcode);
            object ExistStock = querystock.ExecuteScalar();
            cn.Close();

            if (Convert.ToInt32(ExistStock) > 0)
            {
                Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                msg.cancelBtn.Text = "Ok";
                msg.title.Text = "Can not delete the record from the list";
                msg.header.Text = "Unable to delete this record";
                msg.body.Text = "There is a stock that belongs to this product";
                msg.nextBtn.Visible = false;
                msg.cancelBtn.BackColor = Color.DodgerBlue;
                _ = msg.ShowDialog();
            }
            else
            {
                if (MessageBox.Show("Are you sure want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cn.Open();
                    cmd = new SQLiteCommand("DELETE FROM tblProduct WHERE pcode LIKE '" + prdcode + "'", cn);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                    _ = MessageBox.Show("Record has been successfully deleted", "Point Of Sale", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void dgvProduct_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                string colName = dgvProduct.Columns[e.ColumnIndex].Name;
                if (colName == "Delete")
                {
                    CellDelete(dgvProduct[1, e.RowIndex].Value.ToString());
                }
                else if (colName == "Edit")
                {
                    ProductModule moduleForm = new ProductModule(this);
                    moduleForm.txtProductCode.Text = dgvProduct.Rows[e.RowIndex].Cells[1].Value.ToString();
                    moduleForm.hiddenlbl.Text = dgvProduct.Rows[e.RowIndex].Cells[2].Value.ToString();
                    moduleForm.txtBarCode.Text = dgvProduct.Rows[e.RowIndex].Cells[2].Value.ToString();
                    moduleForm.txtPdesc.Text = dgvProduct.Rows[e.RowIndex].Cells[3].Value.ToString();
                    moduleForm.cmbBrand.Text = dgvProduct.Rows[e.RowIndex].Cells[4].Value.ToString();
                    moduleForm.cmbCategory.Text = dgvProduct.Rows[e.RowIndex].Cells[5].Value.ToString();
                    moduleForm.cmbUnit.Text = dgvProduct.Rows[e.RowIndex].Cells[6].Value.ToString();
                    moduleForm.UPReOrder.Value = decimal.Parse(dgvProduct.Rows[e.RowIndex].Cells[7].Value.ToString());

                    moduleForm.txtProductCode.Enabled = false;
                    moduleForm.btnSave.Enabled = false;
                    moduleForm.btnUpdate.Enabled = true;
                    moduleForm.btngen.Enabled = false;
                    moduleForm.btngen.Visible = false;
                    _ = moduleForm.ShowDialog();
                }
                else if (colName == "Toggle")
                {
                    cn.Open();
                    cmd = new SQLiteCommand("UPDATE tblProduct SET isactive=@isactive WHERE pcode LIKE @pcode", cn);

                    _ = cmd.Parameters.AddWithValue("@pcode", dgvProduct[1, e.RowIndex].Value.ToString());
                    int status = Convert.ToInt32(dgvProduct.Rows[e.RowIndex].Cells[8].Value.ToString());
                    _ = cmd.Parameters.AddWithValue("@isactive", status == 1 ? 0 : 1);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                }
                else
                {
                    return;
                }
                LoadProductData();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadProductData();
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                txtSearch.Clear();
                _ = txtSearch.Focus();
            }
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }
        private void dgvProduct_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete")
            {
                if (id != null)
                {
                    CellDelete(id);
                    LoadProductData();
                }
            }
        }

        private void dgvProduct_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvProduct.CurrentRow.Index;
            id = dgvProduct[1, i].Value.ToString();
        }

        private void Products_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnAdd.PerformClick();
            }
            if (e.KeyCode.ToString() == "F8")  // F8 to focus search box
            {
                _ = txtSearch.Focus();
            }
        }

        private void dgvProduct_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            ProductModule moduleForm = new ProductModule(this);
            moduleForm.txtProductCode.Text = dgvProduct.Rows[e.RowIndex].Cells[1].Value.ToString();
            moduleForm.hiddenlbl.Text = dgvProduct.Rows[e.RowIndex].Cells[2].Value.ToString();
            moduleForm.txtBarCode.Text = dgvProduct.Rows[e.RowIndex].Cells[2].Value.ToString();
            moduleForm.txtPdesc.Text = dgvProduct.Rows[e.RowIndex].Cells[3].Value.ToString();
            moduleForm.textBox1.Text = dgvProduct.Rows[e.RowIndex].Cells[4].Value.ToString();
            moduleForm.textBox2.Text = dgvProduct.Rows[e.RowIndex].Cells[5].Value.ToString();
            moduleForm.textBox4.Text = dgvProduct.Rows[e.RowIndex].Cells[6].Value.ToString();
            moduleForm.textBox3.Text = dgvProduct.Rows[e.RowIndex].Cells[7].Value.ToString();

            moduleForm.txtProductCode.ReadOnly = true;
            moduleForm.txtBarCode.ReadOnly = true;
            moduleForm.txtPdesc.ReadOnly = true;
            moduleForm.cmbBrand.Visible = false;
            moduleForm.cmbUnit.Visible = false;
            moduleForm.textBox4.Visible = true;
            moduleForm.textBox4.ReadOnly = true;
            moduleForm.cmbCategory.Visible = false;
            moduleForm.UPReOrder.Visible = false;
            moduleForm.textBox1.Visible = true;
            moduleForm.textBox2.Visible = true;
            moduleForm.textBox3.Visible = true;

            moduleForm.txtProductCode.Enabled = true;
            moduleForm.btnSave.Visible = false;
            moduleForm.btnUpdate.Visible = false;
            moduleForm.btngen.Enabled = false;
            moduleForm.btngen.Visible = false;
            _ = moduleForm.ShowDialog();
        }
    }
}
