﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System
{
    public partial class Category : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly MainForm mf = new MainForm();
        private string id;
        public Category()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadCategoryData();
            dgvCategory.Columns[3].Width = 25;
            dgvCategory.Columns[4].Width = 25;
            dgvCategory.ClearSelection();
        }

        private bool IsCursorOverCellText(DataGridView dgv, int columnIndex, int rowIndex)
        {
            if (dgv[columnIndex, rowIndex] is DataGridViewCell cell)
            {
                Point cursorPosition = dgv.PointToClient(Cursor.Position);
                Rectangle cellAreaWithTextInIt =
                    new Rectangle(dgv.GetCellDisplayRectangle(columnIndex, rowIndex, true).Location, cell.GetContentBounds(rowIndex).Size);

                return cellAreaWithTextInIt.Contains(cursorPosition);
            }
            return false;
        }
        public void LoadCategoryData()
        {
            int i = 0;
            dgvCategory.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblCategory WHERE category LIKE '%" + txtSearch.Text + "%' ORDER BY case when category = 'No Category' then 0 else 1 end, category LIMIT -1 OFFSET 1", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvCategory.Rows.Add(i, dr["id"].ToString(), dr["category"].ToString());
            }
            dr.Close();
            cn.Close();
        }
        private void btnAdd_Click(object sender, EventArgs e)
        {
            CategoryModule moduleForm = new CategoryModule(this);
            _ = moduleForm.ShowDialog();
        }
        public void CellDelete(string catid)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblProduct WHERE cid LIKE @cid", cn);

            _ = query.Parameters.AddWithValue("@cid", catid);
            object Exist = query.ExecuteScalar();
            cn.Close();
            if (catid == "1")
            {
                Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                msg.cancelBtn.Text = "Ok";
                msg.title.Text = "Error";
                msg.header.Top = 80;
                msg.header.Text = "Default value is not deletable";
                msg.nextBtn.Visible = false;
                msg.cancelBtn.BackColor = Color.DodgerBlue;
                _ = msg.ShowDialog();
            }
            else
            {
                if (Convert.ToInt32(Exist) > 0)
                {
                    Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                    msg.cancelBtn.Text = "Ok";
                    msg.title.Text = "Can not delete the record from the list";
                    msg.header.Text = "Unable to delete this record";
                    msg.body.Text = Convert.ToInt32(Exist) == 1 ? "There is a product that belongs to this category" : "There are " + Convert.ToInt32(Exist) + " products that belongs to this category";
                    msg.nextBtn.Visible = false;
                    msg.cancelBtn.BackColor = Color.DodgerBlue;
                    _ = msg.ShowDialog();
                }
                else
                {
                    if (MessageBox.Show("Are you sure want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("DELETE FROM tblCategory WHERE id LIKE '" + catid + "'", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully deleted", "Point Of Sale", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void dgvCategory_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvCategory.Columns[e.ColumnIndex].Name;
            if (colName == "Delete")
            {
                CellDelete(dgvCategory[1, e.RowIndex].Value.ToString());
            }
            else if (colName == "Edit")
            {
                if (dgvCategory[1, e.RowIndex].Value.ToString() == "1")
                {
                    Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                    msg.cancelBtn.Text = "Ok";
                    msg.title.Text = "Error";
                    msg.header.Top = 80;
                    msg.header.Text = "Default value is not editable";
                    msg.nextBtn.Visible = false;
                    msg.cancelBtn.BackColor = Color.DodgerBlue;
                    _ = msg.ShowDialog();
                }
                else
                {
                    CategoryModule moduleForm = new CategoryModule(this);
                    moduleForm.lblId.Text = dgvCategory[1, e.RowIndex].Value.ToString();
                    moduleForm.txtCategoryName.Text = dgvCategory[2, e.RowIndex].Value.ToString();
                    moduleForm.btnSave.Enabled = false;
                    moduleForm.btnUpdate.Enabled = true;
                    _ = moduleForm.ShowDialog();
                }
            }
            else
            {
                return;
            }
            LoadCategoryData();
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                txtSearch.Clear();
                _ = txtSearch.Focus();
            }
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadCategoryData();
        }

        private void dgvCategory_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete")
            {
                CellDelete(id);
                LoadCategoryData();
            }
        }

        private void dgvCategory_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvCategory.CurrentRow.Index;
            id = dgvCategory[1, i].Value.ToString();
        }

        private void Category_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnAdd.PerformClick();
            }
            if (e.KeyCode.ToString() == "F8")  // F8 to focus search box
            {
                _ = txtSearch.Focus();
            }
        }
    }
}
