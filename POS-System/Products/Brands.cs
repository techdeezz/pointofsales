﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System
{
    public partial class Brands : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly MainForm mf = new MainForm();
        private string id;
        public Brands()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadBrandData();
            dgvBrand.Columns[3].Width = 25;
            dgvBrand.Columns[4].Width = 25;
            dgvBrand.ClearSelection();
        }

        public void LoadBrandData()
        {
            int i = 0;
            dgvBrand.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblBrand WHERE brand LIKE '%" + txtSearch.Text + "%' ORDER BY case when brand = 'No Brand' then 0 else 1 end, brand LIMIT -1 OFFSET 1", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvBrand.Rows.Add(i, dr["id"].ToString(), dr["brand"].ToString());
            }
            dr.Close();
            cn.Close();
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            BrandModule moduleForm = new BrandModule(this);
            _ = moduleForm.ShowDialog();
        }
        public void CellDelete(string brandid)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblProduct WHERE bid LIKE @bid", cn);

            _ = query.Parameters.AddWithValue("@bid", brandid);
            object Exist = query.ExecuteScalar();
            cn.Close();

            if (brandid == "1")
            {
                Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                msg.cancelBtn.Text = "Ok";
                msg.title.Text = "Error";
                msg.header.Top = 80;
                msg.header.Text = "Default value is not deletable";
                msg.nextBtn.Visible = false;
                msg.cancelBtn.BackColor = Color.DodgerBlue;
                _ = msg.ShowDialog();
            }
            else
            {
                if (Convert.ToInt32(Exist) > 0)
                {
                    Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                    msg.cancelBtn.Text = "Ok";
                    msg.title.Text = "Can not delete the record from the list";
                    msg.header.Text = "Unable to delete this record";
                    msg.body.Text = Convert.ToInt32(Exist) == 1 ? "There is a product that belongs to this brand" : "There are " + Convert.ToInt32(Exist) + " products that belongs to this brand";
                    msg.nextBtn.Visible = false;
                    msg.cancelBtn.BackColor = Color.DodgerBlue;
                    _ = msg.ShowDialog();
                }
                else
                {
                    if (MessageBox.Show("Are you sure want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("DELETE FROM tblBrand WHERE id LIKE '" + brandid + "'", cn);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully deleted", "Point Of Sale", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
            }
        }

        private void dgvBrand_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvBrand.Columns[e.ColumnIndex].Name;
            if (colName == "Delete")
            {
                CellDelete(dgvBrand[1, e.RowIndex].Value.ToString());
            }
            else if (colName == "Edit")
            {
                if (dgvBrand[1, e.RowIndex].Value.ToString() == "1")
                {
                    Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                    msg.cancelBtn.Text = "Ok";
                    msg.title.Text = "Error";
                    msg.header.Top = 80;
                    msg.header.Text = "Default value is not editable";
                    msg.nextBtn.Visible = false;
                    msg.cancelBtn.BackColor = Color.DodgerBlue;
                    _ = msg.ShowDialog();
                }
                else
                {
                    BrandModule moduleForm = new BrandModule(this);
                    moduleForm.lblId.Text = dgvBrand[1, e.RowIndex].Value.ToString();
                    moduleForm.txtBrandName.Text = dgvBrand[2, e.RowIndex].Value.ToString();
                    moduleForm.btnSave.Enabled = false;
                    moduleForm.btnUpdate.Enabled = true;
                    _ = moduleForm.ShowDialog();
                }
            }
            LoadBrandData();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadBrandData();
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                txtSearch.Clear();
                _ = txtSearch.Focus();
            }
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void dgvBrand_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete")
            {
                if (id != null)
                {
                    CellDelete(id);
                    LoadBrandData();
                }
            }
        }
        private void dgvBrand_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvBrand.CurrentRow.Index;
            id = dgvBrand[1, i].Value.ToString();
        }

        private void Brands_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnAdd.PerformClick();
            }
            if (e.KeyCode.ToString() == "F8")  // F8 to focus search box
            {
                _ = txtSearch.Focus();
            }
        }
    }
}
