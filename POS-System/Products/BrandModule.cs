﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System
{
    public partial class BrandModule : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly Brands br;
        private Point mouseOffset;
        public BrandModule(Brands br)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            btnUpdate.Enabled = false;
            this.br = br;
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtBrandName.Text) && !CheckBrand(txtBrandName.Text))
                {
                    if (MessageBox.Show("Are you sure want to save this record?", "Insert Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("INSERT INTO tblBrand(brand)VALUES(@brand)", cn);
                        _ = cmd.Parameters.AddWithValue("@brand", txtBrandName.Text);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully saved", "Point Of Sale");
                        Clear();
                        br.LoadBrandData();
                    }
                }
                else if (CheckBrand(txtBrandName.Text))
                {
                    {
                        _ = MessageBox.Show("This record is already exists", "Record Exists", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtBrandName.Focus();
                    }
                }
                else
                {
                    _ = MessageBox.Show("Record hasn't been saved", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _ = txtBrandName.Focus();
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtBrandName.Text))
            {
                Clear();
            }
            else
            {
                Dispose();
            }
        }
        public void Clear()
        {
            txtBrandName.Clear();
            btnUpdate.Enabled = false;
            btnSave.Enabled = true;
            _ = txtBrandName.Focus();
        }
        public bool CheckBrand(string brand)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblBrand WHERE UPPER(brand) = @brand ", cn);

            _ = query.Parameters.AddWithValue("@brand", brand.ToUpper());
            object Exist = query.ExecuteScalar();
            cn.Close();

            return Convert.ToInt32(Exist) > 0;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtBrandName.Text) && !CheckBrand(txtBrandName.Text))
            {
                if (MessageBox.Show("Are you sure want to update this record?", "Update Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cn.Open();
                    cmd = new SQLiteCommand("UPDATE tblBrand SET brand = @brand WHERE id LIKE '" + lblId.Text + "'", cn);
                    _ = cmd.Parameters.AddWithValue("@brand", txtBrandName.Text);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                    _ = MessageBox.Show("Record has been successfully updated", "POS");
                    Clear();
                    Dispose();
                    br.LoadBrandData();
                }
            }
            else if (CheckBrand(txtBrandName.Text))
            {
                {
                    _ = MessageBox.Show("This record is already exists", "Record Exists", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _ = txtBrandName.Focus();
                }
            }
            else
            {
                _ = MessageBox.Show("Record hasn't been saved", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _ = txtBrandName.Focus();
            }
        }

        private void txtBrandName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void BrandModule_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                btnCancel.PerformClick();
            }
        }
    }
}
