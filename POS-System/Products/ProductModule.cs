﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System
{
    public partial class ProductModule : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly string sTitle = "Point Of Sale";
        private readonly Products prd;
        private SQLiteDataReader dr;
        private Point mouseOffset;
        public ProductModule(Products prd)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            btnUpdate.Enabled = false;
            LoadCategory();
            LoadCBrand();
            txtBarCode.Select();
            cmbUnit.SelectedIndex = 0;
            this.prd = prd;
            RandomNumber();
        }
        public void RandomNumber()
        {
            Random rnd = new Random();
            string gnr;
            cn.Open();
            cmd = new SQLiteCommand("SELECT id FROM tblProduct ORDER BY id DESC LIMIT 1", cn);
            object lastID = cmd.ExecuteScalar();
            int count = Convert.ToInt32(lastID);
            dr = cmd.ExecuteReader();
            _ = dr.Read();
            if (dr.HasRows)
            {
                gnr = "P-" + rnd.Next(0, 1000).ToString("D3") + (count + 1);
                txtProductCode.Text = gnr;
            }
            else
            {
                gnr = "P-" + rnd.Next(0, 1000).ToString("D3") + "1";
                txtProductCode.Text = gnr;
            }
            dr.Close();
            cn.Close();

        }

        public void LoadCategory()
        {
            cmbCategory.Items.Clear();
            cmbCategory.DataSource = dbCon.getTable("SELECT * FROM tblCategory ORDER BY case when category = 'No Category' then 0 else 1 end, category");
            cmbCategory.DisplayMember = "category";
            cmbCategory.ValueMember = "id";
        }
        public void LoadCBrand()
        {
            cmbBrand.Items.Clear();
            cmbBrand.DataSource = dbCon.getTable("SELECT * FROM tblBrand ORDER BY case when brand = 'No Brand' then 0 else 1 end, brand");
            cmbBrand.DisplayMember = "brand";
            cmbBrand.ValueMember = "id";

        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        public void Clear()
        {
            txtPdesc.Clear();
            txtBarCode.Clear();
            cmbUnit.SelectedIndex = 0;
            RandomNumber();
            btnUpdate.Enabled = false;
            btnSave.Enabled = true;
            cmbBrand.SelectedIndex = 0;
            cmbCategory.SelectedIndex = 0;
            UPReOrder.Value = 1;
            _ = txtBarCode.Focus();
        }
        public bool CheckPRD(string pcode)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblProduct WHERE pcode = @pcode ", cn);

            _ = query.Parameters.AddWithValue("@pcode", pcode);
            object Exist = query.ExecuteScalar();
            cn.Close();

            return Convert.ToInt32(Exist) > 0;
        }

        public bool CheckBRC(string barcode)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblProduct WHERE barcode = @barcode AND barcode <> '' AND barcode IS NOT NULL", cn);

            _ = query.Parameters.AddWithValue("@barcode", barcode);
            object Exist = query.ExecuteScalar();
            cn.Close();

            return Convert.ToInt32(Exist) > 0;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                    !string.IsNullOrWhiteSpace(txtProductCode.Text) &&
                    cmbBrand.SelectedItem != null &&
                    cmbCategory.SelectedItem != null &&
                    !string.IsNullOrWhiteSpace(txtPdesc.Text)
                    )
                {
                    if (!CheckPRD(txtProductCode.Text) && !CheckBRC(txtBarCode.Text))
                    {
                        if (MessageBox.Show("Are you sure want to save this record?", "Insert Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            cn.Open();
                            cmd = new SQLiteCommand("INSERT INTO tblProduct(pcode,barcode,pdesc,bid,cid,price,actualprice,reorder,unit)VALUES(@pcode,@barcode,@pdesc,@bid,@cid,0.00,0.00,@reorder,@unit)", cn);
                            _ = cmd.Parameters.AddWithValue("@pcode", txtProductCode.Text.ToUpper());
                            _ = cmd.Parameters.AddWithValue("@barcode", txtBarCode.Text);
                            _ = cmd.Parameters.AddWithValue("@pdesc", txtPdesc.Text);
                            _ = cmd.Parameters.AddWithValue("@bid", cmbBrand.SelectedValue);
                            _ = cmd.Parameters.AddWithValue("@cid", cmbCategory.SelectedValue);
                            _ = cmd.Parameters.AddWithValue("@reorder", UPReOrder.Value);
                            string unitVal = cmbUnit.Text == "KG" ? "KG" : cmbUnit.Text == "Liter" ? "L" : "";
                            _ = cmd.Parameters.AddWithValue("@unit", unitVal);

                            _ = cmd.ExecuteNonQuery();
                            cn.Close();
                            _ = MessageBox.Show("Record has been successfully saved", sTitle);
                            Clear();
                            prd.LoadProductData();
                        }
                    }
                    else
                    {
                        if (CheckPRD(txtProductCode.Text))
                        {
                            _ = MessageBox.Show("Product code is already exist in database", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            _ = txtProductCode.Focus();
                        }
                        else

                        if (CheckBRC(txtBarCode.Text))
                        {
                            _ = MessageBox.Show("Bar code is already exist in database", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                            _ = txtBarCode.Focus();
                        }
                        else
                        {
                            _ = MessageBox.Show("Something went wrong", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }

                }
                else
                {
                    if (string.IsNullOrWhiteSpace(txtProductCode.Text))
                    {
                        _ = MessageBox.Show("Please enter product code", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtProductCode.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtPdesc.Text))
                    {
                        _ = MessageBox.Show("Please enter product name", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPdesc.Focus();
                    }
                    else if (cmbBrand.SelectedValue == null)
                    {
                        _ = MessageBox.Show("Please select brand", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (cmbCategory.SelectedValue == null)
                    {
                        _ = MessageBox.Show("Please select category", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else
                    {
                        _ = MessageBox.Show("Record has not been successfully saved", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtBarCode.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void txtPprice_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            // only allow one decimal point
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void txtProductCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void txtBarCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void txtPdesc_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void cmbBrand_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void cmbCategory_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void UPReOrder_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (
                !string.IsNullOrWhiteSpace(txtBarCode.Text) ||
                cmbUnit.SelectedIndex != 0 ||
                !string.IsNullOrWhiteSpace(txtPdesc.Text) ||
                cmbBrand.SelectedIndex == 1 ||
                cmbCategory.SelectedIndex == 1
                )
            {
                Clear();
            }
            else
            {
                Dispose();
            }
            if (btnSave.Visible == false && btnUpdate.Visible == false)
            {
                Dispose();
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                   !string.IsNullOrWhiteSpace(txtProductCode.Text) &&
                    cmbBrand.SelectedItem != null &&
                    cmbCategory.SelectedItem != null &&
                    !string.IsNullOrWhiteSpace(txtPdesc.Text)
                    )
                {
                    cn.Open();
                    SQLiteCommand query = new SQLiteCommand("SELECT barcode FROM tblProduct WHERE barcode = @barcode", cn);

                    _ = query.Parameters.AddWithValue("@barcode", hiddenlbl.Text);
                    object BRC = query.ExecuteScalar();
                    cn.Close();
                    if (Convert.ToString(BRC) == txtBarCode.Text || !CheckBRC(txtBarCode.Text))
                    {
                        if (MessageBox.Show("Are you sure want to update this record?", "Update Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            cn.Open();
                            cmd = new SQLiteCommand("UPDATE tblProduct SET barcode=@barcode, pdesc=@pdesc, bid=@bid, cid=@cid, reorder=@reorder, unit=@unit WHERE pcode LIKE @pcode", cn);

                            _ = cmd.Parameters.AddWithValue("@pcode", txtProductCode.Text);
                            _ = cmd.Parameters.AddWithValue("@barcode", txtBarCode.Text);
                            _ = cmd.Parameters.AddWithValue("@pdesc", txtPdesc.Text);
                            _ = cmd.Parameters.AddWithValue("@bid", cmbBrand.SelectedValue);
                            _ = cmd.Parameters.AddWithValue("@cid", cmbCategory.SelectedValue);
                            string unitVal = cmbUnit.Text == "KG" ? "KG" : cmbUnit.Text == "Liter" ? "L" : "";
                            _ = cmd.Parameters.AddWithValue("@unit", unitVal);
                            _ = cmd.Parameters.AddWithValue("@reorder", UPReOrder.Value);

                            _ = cmd.ExecuteNonQuery();
                            cn.Close();
                            _ = MessageBox.Show("Record has been successfully updated", sTitle);
                            Clear();
                            Dispose();
                            prd.LoadProductData();
                        }
                    }
                    else
                    {
                        _ = MessageBox.Show("Bar code is already exist in database", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtBarCode.Focus();
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(txtProductCode.Text))
                    {
                        _ = MessageBox.Show("Please enter product code", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtProductCode.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtPdesc.Text))
                    {
                        _ = MessageBox.Show("Please enter product name", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPdesc.Focus();
                    }
                    else
                    {
                        _ = cmbBrand.SelectedValue == null
                            ? MessageBox.Show("Please select brand", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error)
                            : cmbCategory.SelectedValue == null
                                                    ? MessageBox.Show("Please select category", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error)
                                                    : MessageBox.Show("Record has been successfully updated", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void ProductModule_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                btnCancel.PerformClick();
            }
        }

        private void ProductModule_Load(object sender, EventArgs e)
        {
            if (btnSave.Enabled == true)
            {
                cmbCategory.SelectedItem = "No Category";
                cmbBrand.SelectedItem = "No Brand";
            }
            cmbBrand.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbBrand.AutoCompleteSource = AutoCompleteSource.ListItems;
            cmbCategory.AutoCompleteMode = AutoCompleteMode.Suggest;
            cmbCategory.AutoCompleteSource = AutoCompleteSource.ListItems;
        }

        private void btngen_Click(object sender, EventArgs e)
        {
            RandomNumber();
        }
    }
}
