﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System
{
    public partial class CategoryModule : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly Category ct;
        private Point mouseOffset;
        public CategoryModule(Category ct)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            btnUpdate.Enabled = false;
            this.ct = ct;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrWhiteSpace(txtCategoryName.Text) && !CheckCat(txtCategoryName.Text))
                {
                    if (MessageBox.Show("Are you sure want to save this record?", "Insert Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("INSERT INTO tblCategory(category)VALUES(@category)", cn);
                        _ = cmd.Parameters.AddWithValue("@category", txtCategoryName.Text);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully saved", "Point Of Sale");
                        Clear();
                        ct.LoadCategoryData();
                    }
                }
                else if (CheckCat(txtCategoryName.Text))
                {
                    {
                        _ = MessageBox.Show("This record is already exists", "Record Exists", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtCategoryName.Focus();
                    }
                }
                else
                {
                    _ = MessageBox.Show("Record hasn't been saved", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _ = txtCategoryName.Focus();
                }

            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }
        public void Clear()
        {
            txtCategoryName.Clear();
            btnUpdate.Enabled = false;
            btnSave.Enabled = true;
            _ = txtCategoryName.Focus();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtCategoryName.Text))
            {
                Clear();
            }
            else
            {
                Dispose();
            }
        }
        public bool CheckCat(string category)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblcategory WHERE UPPER(category) = @category ", cn);

            _ = query.Parameters.AddWithValue("@category", category.ToUpper());
            object Exist = query.ExecuteScalar();
            cn.Close();

            return Convert.ToInt32(Exist) > 0;
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrWhiteSpace(txtCategoryName.Text) && !CheckCat(txtCategoryName.Text))
            {
                if (MessageBox.Show("Are you sure want to update this record?", "Update Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cn.Open();
                    cmd = new SQLiteCommand("UPDATE tblCategory SET category = @category WHERE id LIKE '" + lblId.Text + "'", cn);
                    _ = cmd.Parameters.AddWithValue("@category", txtCategoryName.Text);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                    _ = MessageBox.Show("Record has been successfully updated", "POS");
                    Clear();
                    Dispose();
                    ct.LoadCategoryData();
                }
            }
            else if (CheckCat(txtCategoryName.Text))
            {
                {
                    _ = MessageBox.Show("This record is already exists", "Record Exists", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _ = txtCategoryName.Focus();
                }
            }
            else
            {
                _ = MessageBox.Show("Record hasn't been saved", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                _ = txtCategoryName.Focus();
            }
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void txtCategoryName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void CategoryModule_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                btnCancel.PerformClick();
            }
        }
    }
}
