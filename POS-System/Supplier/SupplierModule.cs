﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace POS_System.Supplier
{
    public partial class SupplierModule : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly string sTitle = "Point Of Sale";
        private Point mouseOffset;
        private readonly Suppliers sup;
        public SupplierModule(Suppliers sup)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            txtSupName.Select();
            btnUpdate.Enabled = false;
            this.sup = sup;
        }

        public void Clear()
        {
            btnUpdate.Enabled = false;
            btnSave.Enabled = true;
            txtSupName.Select();
            txtSupName.Clear();
            txtAddress.Clear();
            txtContactPerson.Clear();
            txtPhone.Clear();
            txtFax.Clear();
            txtEmail.Clear();
        }

        private static Regex email_validation()
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(pattern, RegexOptions.IgnoreCase);
        }

        private static readonly Regex validate_emailaddress = email_validation();

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (
                !string.IsNullOrWhiteSpace(txtSupName.Text) ||
                !string.IsNullOrWhiteSpace(txtAddress.Text) ||
                !string.IsNullOrWhiteSpace(txtContactPerson.Text) ||
                !string.IsNullOrWhiteSpace(txtPhone.Text) ||
                !string.IsNullOrWhiteSpace(txtFax.Text) ||
                !string.IsNullOrWhiteSpace(txtEmail.Text)
                )
            {
                Clear();
            }
            else
            {
                Dispose();
            }
            if (btnSave.Visible == false && btnUpdate.Visible == false)
            {
                Dispose();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                   !string.IsNullOrWhiteSpace(txtSupName.Text) &&
                    !string.IsNullOrWhiteSpace(txtAddress.Text) &&
                    !string.IsNullOrWhiteSpace(txtContactPerson.Text) &&
                    !string.IsNullOrWhiteSpace(txtPhone.Text) &&
                    (txtPhone.Text.Length == 10 || string.IsNullOrWhiteSpace(txtPhone.Text)) &&
                    (txtFax.Text.Length == 10 || string.IsNullOrWhiteSpace(txtFax.Text)) &&
                    (validate_emailaddress.IsMatch(txtEmail.Text) == true || string.IsNullOrWhiteSpace(txtEmail.Text))
                    )
                {

                    if (MessageBox.Show("Are you sure want to update this record?", "Update Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        cn.Open();

                        cmd = new SQLiteCommand("UPDATE tblSupplier SET supplier = @supplier, address = @address, contactPerson = @contactPerson, phone = @phone, email = @email, fax = @fax", cn);

                        _ = cmd.Parameters.AddWithValue("@supplier", txtSupName.Text);
                        _ = cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                        _ = cmd.Parameters.AddWithValue("@contactPerson", txtContactPerson.Text);
                        _ = cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
                        _ = cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                        _ = cmd.Parameters.AddWithValue("@fax", txtFax.Text);

                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully updated", sTitle);
                        Clear();
                        Dispose();
                        sup.LoadSupplierData();
                    }

                }
                else
                {
                    if (string.IsNullOrWhiteSpace(txtSupName.Text))
                    {
                        _ = MessageBox.Show("Please enter supplier", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtSupName.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtAddress.Text))
                    {
                        _ = MessageBox.Show("Please enter address of the supplier", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtAddress.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtContactPerson.Text))
                    {
                        _ = MessageBox.Show("Please enter the supplying person name", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtContactPerson.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtPhone.Text))
                    {
                        _ = MessageBox.Show("Please enter the number of the supplying person", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPhone.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtContactPerson.Text))
                    {
                        _ = MessageBox.Show("Please select category", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                    else if (txtPhone.Text.Length < 10)
                    {
                        _ = MessageBox.Show("Mobile number should be 10 digits", "Validation not match", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPhone.Focus();
                    }
                    else if (txtFax.Text.Length < 10 && !string.IsNullOrWhiteSpace(txtFax.Text))
                    {
                        _ = MessageBox.Show("Fax number should be 10 digits", "Validation not match", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtFax.Focus();
                    }
                    else if (validate_emailaddress.IsMatch(txtEmail.Text) != true && !string.IsNullOrWhiteSpace(txtEmail.Text))
                    {
                        _ = MessageBox.Show("Invalid Email Address!", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtEmail.Focus();
                    }
                    else
                    {
                        _ = MessageBox.Show("Record has not been successfully saved", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtSupName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                    !string.IsNullOrWhiteSpace(txtSupName.Text) &&
                    !string.IsNullOrWhiteSpace(txtAddress.Text) &&
                    !string.IsNullOrWhiteSpace(txtContactPerson.Text) &&
                    !string.IsNullOrWhiteSpace(txtPhone.Text) &&
                    (txtPhone.Text.Length == 10 || string.IsNullOrWhiteSpace(txtPhone.Text)) &&
                    (txtFax.Text.Length == 10 || string.IsNullOrWhiteSpace(txtFax.Text)) &&
                    (validate_emailaddress.IsMatch(txtEmail.Text) == true || string.IsNullOrWhiteSpace(txtEmail.Text))
                    )
                {

                    if (MessageBox.Show("Are you sure want to save this record?", "Insert Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("INSERT INTO tblSupplier(supplier,address,contactPerson,phone,email,fax)VALUES(@supplier,@address,@contactPerson,@phone,@email,@fax)", cn);

                        _ = cmd.Parameters.AddWithValue("@supplier", txtSupName.Text);
                        _ = cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                        _ = cmd.Parameters.AddWithValue("@contactPerson", txtContactPerson.Text);
                        _ = cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
                        _ = cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                        _ = cmd.Parameters.AddWithValue("@fax", txtFax.Text);

                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully saved", sTitle);
                        Clear();
                        sup.LoadSupplierData();
                    }

                }
                else
                {
                    if (string.IsNullOrWhiteSpace(txtSupName.Text))
                    {
                        _ = MessageBox.Show("Please enter supplier", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtSupName.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtAddress.Text))
                    {
                        _ = MessageBox.Show("Please enter address of the supplier", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtAddress.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtContactPerson.Text))
                    {
                        _ = MessageBox.Show("Please enter the supplying person name", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtContactPerson.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtPhone.Text))
                    {
                        _ = MessageBox.Show("Please enter the number of the supplying person", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPhone.Focus();
                    }
                    else if (txtPhone.Text.Length < 10)
                    {
                        _ = MessageBox.Show("Mobile number should be 10 digits", "Validation not match", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPhone.Focus();
                    }
                    else if (txtFax.Text.Length < 10 && !string.IsNullOrWhiteSpace(txtFax.Text))
                    {
                        _ = MessageBox.Show("Fax number should be 10 digits", "Validation not match", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtFax.Focus();
                    }
                    else if (validate_emailaddress.IsMatch(txtEmail.Text) != true && !string.IsNullOrWhiteSpace(txtEmail.Text))
                    {
                        _ = MessageBox.Show("Invalid Email Address!", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtEmail.Focus();
                    }
                    else
                    {
                        _ = MessageBox.Show("Record has not been successfully saved", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtSupName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            if (char.IsDigit(e.KeyChar))
            {
                if ((sender as TextBox).Text.Count(char.IsDigit) >= 10)
                {
                    e.Handled = true;
                }
            }
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void txtFax_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            if (char.IsDigit(e.KeyChar))
            {
                if ((sender as TextBox).Text.Count(char.IsDigit) >= 10)
                {
                    e.Handled = true;
                }
            }
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void txtSupName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void txtContactPerson_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void SupplierModule_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel.PerformClick();
            }
        }
    }
}
