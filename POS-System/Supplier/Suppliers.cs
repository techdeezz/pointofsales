﻿using POS_System.Supplier;
using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System
{
    public partial class Suppliers : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private readonly MainForm mf = new MainForm();
        private string id;
        public Suppliers()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadSupplierData();
            dgvSupplier.Columns[8].Width = 25;
            dgvSupplier.Columns[9].Width = 25;
            dgvSupplier.Columns[10].Width = 25;
        }

        public void LoadSupplierData()
        {
            int i = 0;
            dgvSupplier.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblSupplier WHERE supplier || contactPerson || phone || email || fax LIKE '%" + txtSearch.Text + "%'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvSupplier.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), dr[5].ToString(), dr[6].ToString());
            }
            dr.Close();
            cn.Close();
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                txtSearch.Clear();
                _ = txtSearch.Focus();
            }
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            SupplierModule moduleForm = new SupplierModule(this);
            _ = moduleForm.ShowDialog();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadSupplierData();
        }

        public void CellDelete(string supid)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblStock WHERE supplierid LIKE @supplierid", cn);

            _ = query.Parameters.AddWithValue("@supplierid", supid);
            object Exist = query.ExecuteScalar();
            cn.Close();
            if (supid == "1")
            {
                Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                msg.cancelBtn.Text = "Ok";
                msg.title.Text = "Error";
                msg.header.Top = 80;
                msg.header.Text = "Default value is not deletable";
                msg.nextBtn.Visible = false;
                msg.cancelBtn.BackColor = Color.DodgerBlue;
                _ = msg.ShowDialog();
            }
            else
            {
                if (Convert.ToInt32(Exist) > 0)
                {
                    Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                    msg.cancelBtn.Text = "Ok";
                    msg.title.Text = "Can not delete the record from the list";
                    msg.header.Text = "Unable to delete this record";
                    msg.body.Text = Convert.ToInt32(Exist) == 1 ? "There is a stock that belongs to this supplier" : "There are " + Convert.ToInt32(Exist) + " stocks that belongs to this supplier";
                    msg.nextBtn.Visible = false;
                    msg.cancelBtn.BackColor = Color.DodgerBlue;
                    _ = msg.ShowDialog();
                    return;
                }

                if (MessageBox.Show("Are you sure want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    cn.Open();
                    cmd = new SQLiteCommand("DELETE FROM tblSupplier WHERE id LIKE '" + supid + "'", cn);
                    _ = cmd.ExecuteNonQuery();
                    cn.Close();
                    _ = MessageBox.Show("Record has been successfully deleted", "Point Of Sale", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void dgvSupplier_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvSupplier.Columns[e.ColumnIndex].Name;
            if (colName == "Delete")
            {
                CellDelete(dgvSupplier[1, e.RowIndex].Value.ToString());
            }
            else if (colName == "Edit")
            {
                if (dgvSupplier[1, e.RowIndex].Value.ToString() == "1")
                {
                    Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                    msg.cancelBtn.Text = "Ok";
                    msg.title.Text = "Error";
                    msg.header.Top = 80;
                    msg.header.Text = "Default value is not editable";
                    msg.nextBtn.Visible = false;
                    msg.cancelBtn.BackColor = Color.DodgerBlue;
                    _ = msg.ShowDialog();
                }
                else
                {
                    SupplierModule moduleForm = new SupplierModule(this);
                    moduleForm.lblId.Text = dgvSupplier.Rows[e.RowIndex].Cells[1].Value.ToString();
                    moduleForm.txtSupName.Text = dgvSupplier.Rows[e.RowIndex].Cells[2].Value.ToString();
                    moduleForm.txtAddress.Text = dgvSupplier.Rows[e.RowIndex].Cells[3].Value.ToString();
                    moduleForm.txtContactPerson.Text = dgvSupplier.Rows[e.RowIndex].Cells[4].Value.ToString();
                    moduleForm.txtPhone.Text = dgvSupplier.Rows[e.RowIndex].Cells[5].Value.ToString();
                    moduleForm.txtEmail.Text = dgvSupplier.Rows[e.RowIndex].Cells[6].Value.ToString();
                    moduleForm.txtFax.Text = dgvSupplier.Rows[e.RowIndex].Cells[7].Value.ToString();

                    moduleForm.btnSave.Enabled = false;
                    moduleForm.btnUpdate.Enabled = true;
                    _ = moduleForm.ShowDialog();
                }

            }
            else if (colName == "View")
            {
                SupplierModule moduleForm = new SupplierModule(this);
                moduleForm.txtSupName.Text = dgvSupplier.Rows[e.RowIndex].Cells[2].Value.ToString();
                moduleForm.txtAddress.Text = dgvSupplier.Rows[e.RowIndex].Cells[3].Value.ToString();
                moduleForm.txtContactPerson.Text = dgvSupplier.Rows[e.RowIndex].Cells[4].Value.ToString();
                moduleForm.txtPhone.Text = dgvSupplier.Rows[e.RowIndex].Cells[5].Value.ToString();
                moduleForm.txtEmail.Text = dgvSupplier.Rows[e.RowIndex].Cells[6].Value.ToString();
                moduleForm.txtFax.Text = dgvSupplier.Rows[e.RowIndex].Cells[7].Value.ToString();

                moduleForm.txtSupName.ReadOnly = true;
                moduleForm.txtAddress.ReadOnly = true;
                moduleForm.txtContactPerson.ReadOnly = true;
                moduleForm.txtPhone.ReadOnly = true;
                moduleForm.txtEmail.ReadOnly = true;
                moduleForm.txtFax.ReadOnly = true;

                moduleForm.btnSave.Visible = false;
                moduleForm.btnUpdate.Visible = false;
                _ = moduleForm.ShowDialog();
            }
            else
            {
                return;
            }
            LoadSupplierData();
        }

        private void dgvSupplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete")
            {
                if (id != null)
                {
                    CellDelete(id);
                    LoadSupplierData();
                }
            }
        }

        private void dgvSupplier_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvSupplier.CurrentRow.Index;
            id = dgvSupplier[1, i].Value.ToString();
        }

        private void Suppliers_Load(object sender, EventArgs e)
        {
            dgvSupplier.ClearSelection();
        }

        private void Suppliers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnAdd.PerformClick();
            }
            if (e.KeyCode.ToString() == "F8")  // F8 to focus search box
            {
                _ = txtSearch.Focus();
            }
        }
    }
}
