﻿using POS_System.Record.Print;
using System;
using System.Data.SQLite;
using System.Windows.Forms;

namespace POS_System.Record
{
    public partial class POSRecord : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private bool buttonClickTopsell = false;
        private bool buttonClickSold = false;
        private bool buttonClickCancelled = false;
        private bool buttonClickStockIn = false;
        public POSRecord()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            metroTabControl1.SelectedTab = tabPage1;
            LoadTopSellingOnLoadForm();
            LoadInventoryList();
        }
        #region Custome methods
        public void LoadTopSelling()
        {
            try
            {
                int i = 0;
                dgvTopSelling.Rows.Clear();
                cn.Open();

                // sortby total amount
                if (cmbTopSell.Text == "Sort By Qty")
                {
                    cmd = new SQLiteCommand("SELECT pcode, pdesc, ifnull(sum(qty),0) AS qty, ifnull(sum(total),0) AS total, unit FROM vwTopSelling WHERE date(sdate) BETWEEN '" + dtFromTopSell.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToTopSell.Value.ToString("yyyy-MM-dd") + "' AND status LIKE 'Sold' GROUP BY pcode, pdesc,unit ORDER BY qty DESC", cn);
                }
                else if (cmbTopSell.Text == "Sort By Total Amount")
                {
                    cmd = new SQLiteCommand("SELECT pcode, pdesc, ifnull(sum(qty),0) AS qty, ifnull(sum(total),0) AS total, unit FROM vwTopSelling WHERE date(sdate) BETWEEN '" + dtFromTopSell.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToTopSell.Value.ToString("yyyy-MM-dd") + "' AND status LIKE 'Sold' GROUP BY pcode, pdesc,unit ORDER BY total DESC", cn);
                }
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvTopSelling.Rows.Add(i, dr["pcode"].ToString(), dr["pdesc"].ToString(), dr["qty"].ToString(), dr["unit"].ToString(), double.Parse(dr["total"].ToString()).ToString("#,##0.00"));
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        public void LoadCriticalItem()
        {
            try
            {
                int i = 0;
                dgvCriticalItems.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM vwCriticalItems", cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvCriticalItems.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), double.Parse(dr[6].ToString()).ToString("#,##0.00"), double.Parse(dr[5].ToString()).ToString("#,##0.00"), dr[7].ToString(), dr[8].ToString() + " " + dr[9].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void LoadTopSellingOnLoadForm()
        {
            try
            {
                int i = 0;
                dgvTopSelling.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT pcode, pdesc, ifnull(sum(qty),0) AS qty, ifnull(sum(total),0) AS total, unit FROM vwTopSelling WHERE status LIKE 'Sold' GROUP BY pcode, pdesc,unit ORDER BY qty DESC LIMIT 20", cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvTopSelling.Rows.Add(i, dr["pcode"].ToString(), dr["pdesc"].ToString(), dr["qty"].ToString(), dr["unit"].ToString(), double.Parse(dr["total"].ToString()).ToString("#,##0.00"));
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }

        }
        public void LoadSoldItems()
        {
            try
            {
                int i = 0;
                dgvSoldItems.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM vwSoldItem WHERE date(sdate) BETWEEN '" + dtFromSoldItem.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToSoldItem.Value.ToString("yyyy-MM-dd") + "'", cn);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvSoldItems.Rows.Add(i, dr["pcode"].ToString(), dr["pdesc"].ToString(), dr["qty"].ToString(), dr["unit"].ToString(), dr["disc"].ToString(), double.Parse(dr["total"].ToString()).ToString("#,##0.00"));
                }
                dr.Close();
                cn.Close();

                cn.Open();
                cmd = new SQLiteCommand("SELECT IFNULL(SUM(total),0) FROM tblCart WHERE status LIKE 'Sold' AND date(sdate) BETWEEN '" + dtFromSoldItem.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToSoldItem.Value.ToString("yyyy-MM-dd") + "'", cn);
                lblTotal.Text = double.Parse(cmd.ExecuteScalar().ToString()).ToString("#,##0.00");
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void LoadInventoryList()
        {
            try
            {
                int i = 0;
                dgvInventory.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM vwInventoryList", cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvInventory.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), double.Parse(dr[6].ToString()).ToString("#,##0.00"), double.Parse(dr[5].ToString()).ToString("#,##0.00"), dr[7].ToString() + " " + dr[9].ToString(), dr[8].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void LoadCancelItems()
        {
            try
            {
                int i = 0;
                dgvCancelledOrder.Rows.Clear();
                cn.Open();

                cmd = new SQLiteCommand("SELECT * FROM vwCancelItems WHERE date(sdate) BETWEEN '" + dtFromCancelled.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToCancelled.Value.ToString("yyyy-MM-dd") + "'", cn);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvCancelledOrder.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), double.Parse(dr[3].ToString()).ToString("#,##0.00"), dr[4].ToString() + " " + dr[11].ToString(), double.Parse(dr[5].ToString()).ToString("#,##0.00"), DateTime.Parse(dr[6].ToString()).ToShortDateString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString(), dr[10].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void LoadCancelItemsOnLoad()
        {
            try
            {
                int i = 0;
                dgvCancelledOrder.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM vwCancelItems LIMIT 20", cn);
                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvCancelledOrder.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), double.Parse(dr[3].ToString()).ToString("#,##0.00"), dr[4].ToString() + " " + dr[11].ToString(), double.Parse(dr[5].ToString()).ToString("#,##0.00"), DateTime.Parse(dr[6].ToString()).ToShortDateString(), dr[7].ToString(), dr[8].ToString(), dr[9].ToString(), dr[10].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void LoadStockInHistory()
        {
            try
            {
                int i = 0;
                dgvStockInHistory.Rows.Clear();
                cn.Open();

                cmd = new SQLiteCommand("SELECT * FROM vwStockIn WHERE date(sdate) BETWEEN '" + dtFromStockIn.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToStockIn.Value.ToString("yyyy-MM-dd") + "' AND status LIKE 'Done'", cn);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvStockInHistory.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString() + " " + dr[11].ToString(), double.Parse(dr[5].ToString()).ToString("#,##0.00"), double.Parse(dr[6].ToString()).ToString("#,##0.00"), DateTime.Parse(dr[7].ToString()).ToShortDateString(), dr[8].ToString(), dr[9].ToString(), dr[10].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void LoadStockInHistoryOnLoad()
        {
            try
            {
                int i = 0;
                dgvStockInHistory.Rows.Clear();
                cn.Open();

                cmd = new SQLiteCommand("SELECT * FROM vwStockIn WHERE status LIKE 'Done' LIMIT 20", cn);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvStockInHistory.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString() + " " + dr[11].ToString(), double.Parse(dr[5].ToString()).ToString("#,##0.00"), double.Parse(dr[6].ToString()).ToString("#,##0.00"), DateTime.Parse(dr[7].ToString()).ToShortDateString(), dr[8].ToString(), dr[9].ToString(), dr[10].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        #endregion

        public void LoadStockAdjustmentOnLoad()
        {
            try
            {
                int i = 0;
                dgvAdjustmentHistory.Rows.Clear();
                cn.Open();

                cmd = new SQLiteCommand("SELECT * FROM vwStockAdjustment ORDER BY id DESC LIMIT 20", cn);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DateTime dDate;
                    i++;
                    dgvAdjustmentHistory.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[8].ToString(), dr[3].ToString() + " " + dr[10].ToString(), double.Parse(dr[4].ToString()).ToString("#,##0.00"), double.Parse(dr[5].ToString()).ToString("#,##0.00"), DateTime.TryParse(dr[6].ToString(), out dDate) ? DateTime.Parse(dr[6].ToString()).ToShortDateString() : dr[6].ToString(), dr[7].ToString(), dr[9].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }
        public void LoadStockAdjustment()
        {
            try
            {
                int i = 0;
                dgvAdjustmentHistory.Rows.Clear();
                cn.Open();

                cmd = new SQLiteCommand("SELECT * FROM vwStockAdjustment WHERE date(sdate) BETWEEN '" + dtFromAdjustment.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToAdjustment.Value.ToString("yyyy-MM-dd") + "' ORDER BY id DESC", cn);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    DateTime dDate;
                    i++;
                    dgvAdjustmentHistory.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[8].ToString(), dr[3].ToString() + " " + dr[10].ToString(), double.Parse(dr[4].ToString()).ToString("#,##0.00"), double.Parse(dr[5].ToString()).ToString("#,##0.00"), DateTime.TryParse(dr[6].ToString(), out dDate) ? DateTime.Parse(dr[6].ToString()).ToShortDateString() : dr[6].ToString(), dr[7].ToString(), dr[9].ToString());
                }
                dr.Close();
                cn.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message, "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            }
        }

        private void btnLoad_Click(object sender, EventArgs e)
        {
            if (cmbTopSell.Text == "")
            {
                _ = MessageBox.Show("Please select a sort type from the dropdown", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                _ = cmbTopSell.Focus();
                return;
            }
            lbldisplayTopSelling.Text = "Display Records From " + dtFromTopSell.Value.ToString("yyyy/MM/dd") + " To " + dtToTopSell.Value.ToString("yyyy/MM/dd");
            LoadTopSelling();
            buttonClickTopsell = true;
        }

        private void metroTabControl1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabPage1.Focus())
            {
                LoadTopSellingOnLoadForm();
            }
            if (tabPage2.Focus())
            {
                int i = 0;
                dgvSoldItems.Rows.Clear();
                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM vwSoldItem", cn);

                dr = cmd.ExecuteReader();
                while (dr.Read())
                {
                    i++;
                    _ = dgvSoldItems.Rows.Add(i, dr["pcode"].ToString(), dr["pdesc"].ToString(), dr["qty"].ToString(), dr["unit"].ToString(), dr["disc"].ToString(), double.Parse(dr["total"].ToString()).ToString("#,##0.00"));
                }
                dr.Close();
                cn.Close();

                cn.Open();
                cmd = new SQLiteCommand("SELECT IFNULL(SUM(total),0) FROM tblCart WHERE status LIKE 'Sold' LIMIT 20", cn);
                lblTotal.Text = double.Parse(cmd.ExecuteScalar().ToString()).ToString("#,##0.00");
                cn.Close();
            }
            if (tabPage3.Focus())
            {
                LoadCriticalItem();
            }
            if (tabPage5.Focus())
            {
                LoadCancelItemsOnLoad();
            }
            if (tabPage6.Focus())
            {
                LoadStockInHistoryOnLoad();
            }
            if (tabPage7.Focus())
            {
                LoadStockAdjustmentOnLoad();
            }
        }

        private void btnLoadSoldItem_Click(object sender, EventArgs e)
        {
            lbldisplaySoldItem.Text = "Display Records From " + dtFromSoldItem.Value.ToString("yyyy/MM/dd") + " To " + dtToSoldItem.Value.ToString("yyyy/MM/dd");
            LoadSoldItems();
            buttonClickSold = true;
        }

        private void btnLoadCancelled_Click(object sender, EventArgs e)
        {
            lblDisplayCanclled.Text = "Display Records From " + dtFromCancelled.Value.ToString("yyyy/MM/dd") + " To " + dtToCancelled.Value.ToString("yyyy/MM/dd");
            LoadCancelItems();
            buttonClickCancelled = true;
        }

        private void btnStockIn_Click(object sender, EventArgs e)
        {
            lblDisplayStockIn.Text = "Display Records From " + dtFromStockIn.Value.ToString("yyyy/MM/dd") + " To " + dtToStockIn.Value.ToString("yyyy/MM/dd");
            LoadStockInHistory();
            buttonClickStockIn = true;
        }

        private void btnprintTopsell_Click(object sender, EventArgs e)
        {
            TopSelling topsell = new TopSelling();
            string param = "Date From: " + dtFromTopSell.Value.ToString("yyyy-MM-dd") + " And " + dtToTopSell.Value.ToString("yyyy-MM-dd");

            if (buttonClickTopsell)
            {
                topsell.LoadDTopSellingReport("SELECT pcode, pdesc, ifnull(sum(qty),0) AS qty, ifnull(sum(total),0) AS total, unit FROM vwTopSelling WHERE date(sdate) BETWEEN '" + dtFromTopSell.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToTopSell.Value.ToString("yyyy-MM-dd") + "' AND status LIKE 'Sold' GROUP BY pcode, pdesc, unit", param);
            }
            else
            {
                topsell.LoadDTopSellingReport("SELECT pcode, pdesc, ifnull(sum(qty),0) AS qty, ifnull(sum(total),0) AS total, unit FROM vwTopSelling WHERE status LIKE 'Sold' GROUP BY pcode, pdesc, unit ORDER BY qty DESC LIMIT 20", "Display Last 20 Records");
            }
            _ = topsell.ShowDialog();
        }

        private void btnPrintSoldItem_Click(object sender, EventArgs e)
        {
            SoldItem item = new SoldItem();
            string param = "Date From: " + dtFromSoldItem.Value.ToString("yyyy-MM-dd") + " And " + dtToSoldItem.Value.ToString("yyyy-MM-dd");
            if (buttonClickSold)
            {
                item.LoadSoldItemsReport("SELECT * FROM vwSoldItem WHERE date(sdate) BETWEEN '" + dtFromSoldItem.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToSoldItem.Value.ToString("yyyy-MM-dd") + "'", param);
            }
            else
            {
                item.LoadSoldItemsReport("SELECT * FROM vwSoldItem LIMIT 20", "Display Last 20 Records");
            }
            _ = item.ShowDialog();
        }

        private void btnPrintCriticalStock_Click(object sender, EventArgs e)
        {
            CriticalStock critical = new CriticalStock();
            critical.LoadCriticalStockReport("SELECT * FROM vwCriticalItems");
            _ = critical.ShowDialog();
        }

        private void btnPrintInventory_Click(object sender, EventArgs e)
        {
            InventoryList inventory = new InventoryList();
            inventory.LoadInvntoryReport("SELECT * FROM vwInventoryList");
            _ = inventory.ShowDialog();
        }

        private void btnPrintCancelled_Click(object sender, EventArgs e)
        {
            CancelledOrders order = new CancelledOrders();
            string param = "Date From: " + dtFromCancelled.Value.ToString("yyyy-MM-dd") + " And " + dtToCancelled.Value.ToString("yyyy-MM-dd");
            if (buttonClickCancelled)
            {
                order.LoadCancelledOrderReport("SELECT * FROM vwCancelItems WHERE date(sdate) BETWEEN '" + dtFromCancelled.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToCancelled.Value.ToString("yyyy-MM-dd") + "'", param);
            }
            else
            {
                order.LoadCancelledOrderReport("SELECT * FROM vwCancelItems LIMIT 20", "Display Last 20 Records");
            }
            _ = order.ShowDialog();
        }

        private void btnPrintStockin_Click(object sender, EventArgs e)
        {
            //buttonClickStockIn

            StockIn stock = new StockIn();
            string param = "Date From: " + dtFromStockIn.Value.ToString("yyyy-MM-dd") + " And " + dtToStockIn.Value.ToString("yyyy-MM-dd");
            if (buttonClickStockIn)
            {
                stock.LoadCancelledOrderReport("SELECT * FROM vwStockIn WHERE date(sdate) BETWEEN '" + dtFromStockIn.Value.ToString("yyyy-MM-dd") + "' AND '" + dtToStockIn.Value.ToString("yyyy-MM-dd") + "' AND status LIKE 'Done'", param);
            }
            else
            {
                stock.LoadCancelledOrderReport("SELECT * FROM vwStockIn WHERE status LIKE 'Done' LIMIT 20", "Display Last 20 Records");
            }
            _ = stock.ShowDialog();
        }

        private void btnLoadAdjust_Click(object sender, EventArgs e)
        {
            lblDisplayAdjustment.Text = "Display Records From " + dtFromStockIn.Value.ToString("yyyy/MM/dd") + " To " + dtToStockIn.Value.ToString("yyyy/MM/dd");
            LoadStockAdjustment();
        }
    }
}
