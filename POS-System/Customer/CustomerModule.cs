﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace POS_System.Customer
{
    public partial class CustomerModule : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private readonly string sTitle = "Point Of Sale";
        private Point mouseOffset;
        private readonly Customers cus;
        public CustomerModule(Customers cus)
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            txtCustomerName.Select();
            btnUpdate.Enabled = false;
            this.cus = cus;
            cmbActive.Text = "Yes";
        }

        public void Clear()
        {
            btnUpdate.Enabled = false;
            btnSave.Enabled = true;
            txtCustomerName.Select();
            txtCustomerName.Clear();
            txtAddress.Clear();
            txtPhone.Clear();
            txtNic.Clear();
            txtEmail.Clear();
            cmbActive.Text = "Yes";
        }

        private static Regex email_validation()
        {
            string pattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(pattern, RegexOptions.IgnoreCase);
        }

        private static readonly Regex validate_emailaddress = email_validation();

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (
                !string.IsNullOrWhiteSpace(txtCustomerName.Text) ||
                !string.IsNullOrWhiteSpace(txtAddress.Text) ||
                !string.IsNullOrWhiteSpace(txtPhone.Text) ||
                !string.IsNullOrWhiteSpace(txtNic.Text) ||
                !string.IsNullOrWhiteSpace(txtEmail.Text) ||
                cmbActive.Text != "Yes"
                )
            {
                Clear();
            }
            else
            {
                Dispose();
            }
            if (btnSave.Visible == false && btnUpdate.Visible == false)
            {
                Dispose();
            }
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                     !string.IsNullOrWhiteSpace(txtCustomerName.Text) &&
                    !string.IsNullOrWhiteSpace(txtAddress.Text) &&
                    (validate_emailaddress.IsMatch(txtEmail.Text) == true || string.IsNullOrWhiteSpace(txtEmail.Text)) &&
                    !string.IsNullOrWhiteSpace(txtPhone.Text) &&
                    (txtPhone.Text.Length == 10 || string.IsNullOrWhiteSpace(txtPhone.Text)) &&
                    !string.IsNullOrWhiteSpace(txtNic.Text)
                    )
                {
                    if (MessageBox.Show("Are you sure want to update this record?", "Update Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        cn.Open();

                        cmd = new SQLiteCommand("UPDATE tblCustomer SET customer = @customer, address = @address, phone = @phone, email = @email, isactive = @isactive, nic = @nic", cn);
                        _ = cmd.Parameters.AddWithValue("@customer", txtCustomerName.Text);
                        _ = cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                        _ = cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
                        _ = cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                        _ = cmd.Parameters.AddWithValue("@isactive", cmbActive.Text);
                        _ = cmd.Parameters.AddWithValue("@nic", txtNic.Text);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully updated", sTitle);
                        Clear();
                        Dispose();
                        cus.LoadCustomerData();
                    }
                }
                else
                {
                    if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
                    {
                        _ = MessageBox.Show("Please enter customer name", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtCustomerName.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtAddress.Text))
                    {
                        _ = MessageBox.Show("Please enter address of the customer", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtAddress.Focus();
                    }
                    else if (validate_emailaddress.IsMatch(txtEmail.Text) != true && !string.IsNullOrWhiteSpace(txtEmail.Text))
                    {
                        _ = MessageBox.Show("Invalid Email Address!", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtEmail.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtPhone.Text))
                    {
                        _ = MessageBox.Show("Please enter the number of the customer", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPhone.Focus();
                    }
                    else if (txtPhone.Text.Length < 10)
                    {
                        _ = MessageBox.Show("Mobile number should be 10 digits", "Validation not match", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPhone.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtNic.Text))
                    {
                        _ = MessageBox.Show("Please enter NIC number", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtNic.Focus();
                    }
                    else
                    {
                        _ = MessageBox.Show("Record has not been successfully updated", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtCustomerName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                if (
                    !string.IsNullOrWhiteSpace(txtCustomerName.Text) &&
                    !string.IsNullOrWhiteSpace(txtAddress.Text) &&
                    (validate_emailaddress.IsMatch(txtEmail.Text) == true || string.IsNullOrWhiteSpace(txtEmail.Text)) &&
                    !string.IsNullOrWhiteSpace(txtPhone.Text) &&
                    (txtPhone.Text.Length == 10 || string.IsNullOrWhiteSpace(txtPhone.Text)) &&
                    !string.IsNullOrWhiteSpace(txtNic.Text)
                    )
                {

                    if (MessageBox.Show("Are you sure want to save this record?", "Insert Record", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        cn.Open();
                        cmd = new SQLiteCommand("INSERT INTO tblCustomer(customer,address,email,phone,isactive,nic)VALUES(@customer,@address,@email,@phone,@isactive,@nic)", cn);
                        _ = cmd.Parameters.AddWithValue("@customer", txtCustomerName.Text);
                        _ = cmd.Parameters.AddWithValue("@address", txtAddress.Text);
                        _ = cmd.Parameters.AddWithValue("@phone", txtPhone.Text);
                        _ = cmd.Parameters.AddWithValue("@email", txtEmail.Text);
                        _ = cmd.Parameters.AddWithValue("@isactive", cmbActive.Text);
                        _ = cmd.Parameters.AddWithValue("@nic", txtNic.Text);
                        _ = cmd.ExecuteNonQuery();
                        cn.Close();
                        _ = MessageBox.Show("Record has been successfully saved", sTitle);
                        Clear();
                        cus.LoadCustomerData();
                    }

                }
                else
                {
                    if (string.IsNullOrWhiteSpace(txtCustomerName.Text))
                    {
                        _ = MessageBox.Show("Please enter customer name", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtCustomerName.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtAddress.Text))
                    {
                        _ = MessageBox.Show("Please enter address of the customer", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtAddress.Focus();
                    }
                    else if (validate_emailaddress.IsMatch(txtEmail.Text) != true && !string.IsNullOrWhiteSpace(txtEmail.Text))
                    {
                        _ = MessageBox.Show("Invalid Email Address!", "Invalid", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtEmail.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtPhone.Text))
                    {
                        _ = MessageBox.Show("Please enter the number of the customer", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPhone.Focus();
                    }
                    else if (txtPhone.Text.Length < 10)
                    {
                        _ = MessageBox.Show("Mobile number should be 10 digits", "Validation not match", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtPhone.Focus();
                    }
                    else if (string.IsNullOrWhiteSpace(txtNic.Text))
                    {
                        _ = MessageBox.Show("Please enter NIC number", "Empy value not allowed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtNic.Focus();
                    }
                    else
                    {
                        _ = MessageBox.Show("Record has not been successfully saved", sTitle, MessageBoxButtons.OK, MessageBoxIcon.Error);
                        _ = txtCustomerName.Focus();
                    }
                }
            }
            catch (Exception ex)
            {
                _ = MessageBox.Show(ex.Message);
            }
        }

        private void txtPhone_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
            if (char.IsDigit(e.KeyChar))
            {
                if ((sender as TextBox).Text.Count(char.IsDigit) >= 10)
                {
                    e.Handled = true;
                }
            }
            if (e.KeyChar == (char)13)
            {
                _ = txtNic.Focus();
            }
        }

        private void txtSupName_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                _ = txtAddress.Focus();
            }
        }

        private void txtEmail_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                _ = txtPhone.Focus();
            }
        }
        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }

        private void SupplierModule_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                btnCancel.PerformClick();
            }
        }

        private void txtNic_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                if (btnSave.Enabled)
                {
                    btnSave.PerformClick();
                }
                else if (btnUpdate.Enabled)
                {
                    btnUpdate.PerformClick();
                }
            }
        }
    }
}
