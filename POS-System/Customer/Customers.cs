﻿using System;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System.Customer
{
    public partial class Customers : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private string id;
        public Customers()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            LoadCustomerData();
            dgvCustomer.Columns[8].Width = 25;
            dgvCustomer.Columns[9].Width = 25;
            dgvCustomer.Columns[10].Width = 25;
        }

        public void LoadCustomerData()
        {
            int i = 0;
            dgvCustomer.Rows.Clear();
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblCustomer WHERE customer || nic || phone LIKE '%" + txtSearch.Text + "%'", cn);
            dr = cmd.ExecuteReader();
            while (dr.Read())
            {
                i++;
                _ = dgvCustomer.Rows.Add(i, dr[0].ToString(), dr[1].ToString(), dr[2].ToString(), dr[3].ToString(), dr[4].ToString(), dr[5].ToString(), dr[6].ToString());
            }
            dr.Close();
            cn.Close();
        }

        private void txtSearch_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                txtSearch.Clear();
                _ = txtSearch.Focus();
            }
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
            }
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            CustomerModule moduleForm = new CustomerModule(this);
            _ = moduleForm.ShowDialog();
        }

        private void txtSearch_TextChanged(object sender, EventArgs e)
        {
            LoadCustomerData();
        }

        public void CellDelete(string cusid)
        {
            cn.Open();
            SQLiteCommand query = new SQLiteCommand("SELECT COUNT(*) FROM tblBookKeepinPayments WHERE cutomerid LIKE @cutomerid", cn);

            _ = query.Parameters.AddWithValue("@cutomerid", cusid);
            object Exist = query.ExecuteScalar();
            cn.Close();

            if (Convert.ToInt32(Exist) > 0)
            {
                Common.ErrorMessageBox msg = new Common.ErrorMessageBox();
                msg.cancelBtn.Text = "Ok";
                msg.title.Text = "Can not delete the record from the list";
                msg.header.Text = "Unable to delete this record";
                msg.body.Text = "This customer added to book keeping module";
                msg.nextBtn.Visible = false;
                msg.cancelBtn.BackColor = Color.DodgerBlue;
                _ = msg.ShowDialog();
                return;
            }

            if (MessageBox.Show("Are you sure want to delete this record?", "Delete Record", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                cn.Open();
                cmd = new SQLiteCommand("DELETE FROM tblCustomer WHERE id LIKE '" + cusid + "'", cn);
                _ = cmd.ExecuteNonQuery();
                cn.Close();
                _ = MessageBox.Show("Record has been successfully deleted", "Point Of Sale", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }

        private void dgvSupplier_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            string colName = dgvCustomer.Columns[e.ColumnIndex].Name;
            if (colName == "Delete")
            {
                CellDelete(dgvCustomer[1, e.RowIndex].Value.ToString());
            }
            else if (colName == "Edit")
            {
                CustomerModule moduleForm = new CustomerModule(this);
                moduleForm.lblId.Text = dgvCustomer.Rows[e.RowIndex].Cells[1].Value.ToString();
                moduleForm.txtCustomerName.Text = dgvCustomer.Rows[e.RowIndex].Cells[2].Value.ToString();
                moduleForm.txtAddress.Text = dgvCustomer.Rows[e.RowIndex].Cells[3].Value.ToString();
                moduleForm.txtEmail.Text = dgvCustomer.Rows[e.RowIndex].Cells[4].Value.ToString();
                moduleForm.txtPhone.Text = dgvCustomer.Rows[e.RowIndex].Cells[5].Value.ToString();
                moduleForm.cmbActive.Text = dgvCustomer.Rows[e.RowIndex].Cells[6].Value.ToString();
                moduleForm.txtNic.Text = dgvCustomer.Rows[e.RowIndex].Cells[7].Value.ToString();

                moduleForm.btnSave.Enabled = false;
                moduleForm.btnUpdate.Enabled = true;
                _ = moduleForm.ShowDialog();
            }
            else if (colName == "View")
            {
                CustomerModule moduleForm = new CustomerModule(this);
                moduleForm.txtCustomerName.Text = dgvCustomer.Rows[e.RowIndex].Cells[2].Value.ToString();
                moduleForm.txtAddress.Text = dgvCustomer.Rows[e.RowIndex].Cells[3].Value.ToString();
                moduleForm.txtEmail.Text = dgvCustomer.Rows[e.RowIndex].Cells[4].Value.ToString();
                moduleForm.txtPhone.Text = dgvCustomer.Rows[e.RowIndex].Cells[5].Value.ToString();
                moduleForm.cmbActive.Text = dgvCustomer.Rows[e.RowIndex].Cells[6].Value.ToString();
                moduleForm.txtNic.Text = dgvCustomer.Rows[e.RowIndex].Cells[7].Value.ToString();

                moduleForm.txtCustomerName.ReadOnly = true;
                moduleForm.txtAddress.ReadOnly = true;
                moduleForm.txtEmail.ReadOnly = true;
                moduleForm.txtPhone.ReadOnly = true;
                moduleForm.txtNic.ReadOnly = true;

                moduleForm.btnSave.Visible = false;
                moduleForm.btnUpdate.Visible = false;
                _ = moduleForm.ShowDialog();
            }
            else
            {
                return;
            }
            LoadCustomerData();
        }

        private void dgvSupplier_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "Delete")
            {
                if (id != null)
                {
                    CellDelete(id);
                    LoadCustomerData();
                }
            }
        }

        private void dgvSupplier_SelectionChanged(object sender, EventArgs e)
        {
            int i = dgvCustomer.CurrentRow.Index;
            id = dgvCustomer[1, i].Value.ToString();
        }

        private void Suppliers_Load(object sender, EventArgs e)
        {
            dgvCustomer.ClearSelection();
        }

        private void Suppliers_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode.ToString() == "F2")  // F2 to open module box
            {
                btnAdd.PerformClick();
            }
            if (e.KeyCode.ToString() == "F8")  // F8 to focus search box
            {
                _ = txtSearch.Focus();
            }
        }
    }
}
