﻿using POS_System.Stock;
using POS_System.Stock.Module;
using POS_System.Stock.StockAdjustment;
using System;
using System.Windows.Forms;

namespace POS_System.Common
{
    public partial class NotificationMsg : Form
    {
        public decimal qty;
        private readonly MainForm main;
        public NotificationMsg(MainForm main)
        {
            InitializeComponent();
            this.main = main;
        }

        private void lblmessage_MouseHover(object sender, EventArgs e)
        {
            lblmessage.Cursor = Cursors.Hand;
        }
        private void lblmessage_Click(object sender, EventArgs e)
        {
            if (qty == 0)
            {
                StockIn stk = new StockIn(main);
                main.openChildrenForm(stk, main.btnStockEntry);
                ProductStockIn stockIn = new ProductStockIn(stk);
                stk.ProductForSupplier(lblpcode.Text);
                stockIn.LoadCellClick(lblpcode.Text);
                _ = main.Focus();
                main.Select();
            }
            else
            {
                Adjustment adj = new Adjustment(main)
                {
                    _pcode = lblpcode.Text
                };
                main.openChildrenForm(adj, main.btnStockAdjustment);
                adj.LoadStock(true);
                _ = main.Focus();
                main.Select();
            }
        }

        private void pictureBox1_Click(object sender, EventArgs e)
        {
            if (qty == 0)
            {
                StockIn stk = new StockIn(main);
                main.openChildrenForm(stk, main.btnStockEntry);
                ProductStockIn stockIn = new ProductStockIn(stk);
                stk.ProductForSupplier(lblpcode.Text);
                stockIn.LoadCellClick(lblpcode.Text);
                _ = main.Focus();
                main.Select();
            }
            else
            {
                Adjustment adj = new Adjustment(main)
                {
                    _pcode = lblpcode.Text
                };
                main.openChildrenForm(adj, main.btnStockAdjustment);
                adj.LoadStock(true);
                _ = main.Focus();
                main.Select();
            }
        }

        private void pictureBox1_MouseHover(object sender, EventArgs e)
        {
            pictureBox1.Cursor = Cursors.Hand;
        }
    }
}
