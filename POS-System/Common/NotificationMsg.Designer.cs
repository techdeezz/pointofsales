﻿namespace POS_System.Common
{
    partial class NotificationMsg
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblpcode = new System.Windows.Forms.Label();
            this.lblmessage = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.lbldevider = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lbldevider);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Controls.Add(this.lblpcode);
            this.panel1.Controls.Add(this.lblmessage);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(300, 47);
            this.panel1.TabIndex = 0;
            // 
            // lblpcode
            // 
            this.lblpcode.AutoSize = true;
            this.lblpcode.Location = new System.Drawing.Point(283, 22);
            this.lblpcode.Name = "lblpcode";
            this.lblpcode.Size = new System.Drawing.Size(22, 20);
            this.lblpcode.TabIndex = 7;
            this.lblpcode.Text = "id";
            this.lblpcode.Visible = false;
            // 
            // lblmessage
            // 
            this.lblmessage.Font = new System.Drawing.Font("Consolas", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblmessage.ForeColor = System.Drawing.Color.Black;
            this.lblmessage.Location = new System.Drawing.Point(32, 0);
            this.lblmessage.Name = "lblmessage";
            this.lblmessage.Size = new System.Drawing.Size(268, 41);
            this.lblmessage.TabIndex = 6;
            this.lblmessage.Text = "Message";
            this.lblmessage.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblmessage.Click += new System.EventHandler(this.lblmessage_Click);
            this.lblmessage.MouseHover += new System.EventHandler(this.lblmessage_MouseHover);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::POS_System.Properties.Resources._return;
            this.pictureBox1.Location = new System.Drawing.Point(0, 5);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(30, 30);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 8;
            this.pictureBox1.TabStop = false;
            this.pictureBox1.Click += new System.EventHandler(this.pictureBox1_Click);
            this.pictureBox1.MouseHover += new System.EventHandler(this.pictureBox1_MouseHover);
            // 
            // lbldevider
            // 
            this.lbldevider.BackColor = System.Drawing.Color.Black;
            this.lbldevider.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.lbldevider.Location = new System.Drawing.Point(0, 44);
            this.lbldevider.Name = "lbldevider";
            this.lbldevider.Size = new System.Drawing.Size(300, 2);
            this.lbldevider.TabIndex = 9;
            // 
            // NotificationMsg
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(300, 47);
            this.Controls.Add(this.panel1);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.KeyPreview = true;
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "NotificationMsg";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NotificationAlert";
            this.TopMost = true;
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Label lblpcode;
        public System.Windows.Forms.Label lblmessage;
        public System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Label lbldevider;
    }
}