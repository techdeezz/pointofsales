﻿using System.Collections.Generic;
using System.Drawing;

namespace POS_System.Common
{
    public static class ThemeColors
    {
        public static List<string> ColorList = new List<string>
        {
            "#C0392B",
            "#922B21",
            "#76448A",
            "#512E5F",
            "#1F618D",
            "#154360",
            "#117A65",
            "#0B5345",
            "#7D6608",
            "#873600",
            "#D35400",
            "#1B2631",
            "#273746",
            "#641E16",
            "#148F77",
            "#34495E"
        };
        public static Color ChangeColorBrightness(Color color, double correctionFactor)
        {
            double red = color.R;
            double green = color.G;
            double blue = color.B;
            if (correctionFactor < 0)
            {
                correctionFactor = 1 + correctionFactor;
                red *= correctionFactor;
                green *= correctionFactor;
                blue *= correctionFactor;
            }
            else
            {
                red = ((255 - red) * correctionFactor) + red;
                green = ((255 - green) * correctionFactor) + green;
                blue = ((255 - blue) * correctionFactor) + blue;
            }
            return Color.FromArgb(color.A, (byte)red, (byte)green, (byte)blue);
        }
    }
}
