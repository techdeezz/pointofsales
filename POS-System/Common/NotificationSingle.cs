﻿using POS_System.Stock;
using POS_System.Stock.Module;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System.Common
{
    public partial class NotificationSingle : Form
    {
        private readonly Random random;
        private int tempIndex;
        public decimal qty;
        private readonly MainForm main;
        public NotificationSingle(MainForm main)
        {
            InitializeComponent();
            random = new Random();
            Color color = SelectThemeColor();
            panel1.BackColor = color;
            this.main = main;
        }
        public enum EnumAction
        {
            Wait,
            Start,
            Close
        }
        private NotificationSingle.EnumAction action;
        private int x, y;

        private void timer1_Tick(object sender, EventArgs e)
        {
            switch (action)
            {
                case EnumAction.Wait:
                    timer1.Interval = 10000;
                    action = EnumAction.Close;
                    break;
                case EnumAction.Start:
                    timer1.Interval = 1;
                    Opacity += 0.1;
                    if (x < Location.X)
                    {
                        Left--;
                    }
                    else
                    {
                        if (Opacity == 1.0)
                        {
                            action = EnumAction.Wait;
                        }
                    }
                    break;
                case EnumAction.Close:
                    timer1.Interval = 1;
                    Opacity -= 0.1;
                    Left -= 3;
                    if (base.Opacity == 0.00)
                    {
                        base.Close();
                    }
                    break;
                default:
                    break;
            }
        }
        public void ShowAlert(string msg)
        {
            Opacity = 0.0;
            StartPosition = FormStartPosition.Manual;
            string fname;
            for (int i = 1; i < 10; i++)
            {
                fname = "alert" + i.ToString();
                NotificationSingle frm = (NotificationSingle)Application.OpenForms[fname];
                if (frm == null)
                {
                    Name = fname;
                    x = Screen.PrimaryScreen.WorkingArea.Width - Width + 15;
                    y = Screen.PrimaryScreen.WorkingArea.Height - (Height * i) - (5 * i);
                    Location = new Point(x, y);
                    break;
                }
            }
            x = Screen.PrimaryScreen.WorkingArea.Width - Width - 5;
            lblmessage.Text = msg;
            Show();
            action = EnumAction.Start;
            timer1.Interval = 1;
            timer1.Start();
        }

        private void btnclose_Click(object sender, EventArgs e)
        {
            timer1.Interval = 1;
            action = EnumAction.Close;
        }

        private Color SelectThemeColor()
        {
            int index = random.Next(ThemeColors.ColorList.Count);
            while (tempIndex == index)
            {
                index = random.Next(ThemeColors.ColorList.Count);
            }
            tempIndex = index;
            string color = ThemeColors.ColorList[index];
            return ColorTranslator.FromHtml(color);
        }
    }
}
