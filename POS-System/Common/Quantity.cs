﻿using System.Windows.Forms;

namespace POS_System.Common
{
    public partial class Quantity : Form
    {
        public string text = "0";
        public Quantity()
        {
            InitializeComponent();
        }

        private void txtQty_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)27)
            {
                Dispose();
            }
            else if ((e.KeyChar == (char)13) && (txtQty.Text != string.Empty))
            {
                text = txtQty.Text;
                Dispose();
            }
        }
    }
}
