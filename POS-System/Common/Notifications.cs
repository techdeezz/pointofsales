﻿using System;
using System.Drawing;
using System.Windows.Forms;

namespace POS_System.Common
{
    public partial class Notifications : Form
    {
        public Notifications()
        {
            InitializeComponent();
        }

        private void Notifications_Deactivate(object sender, EventArgs e)
        {
            _ = new MainForm();
            Close();
        }

        private void Notifications_Load(object sender, EventArgs e)
        {
            MainForm main = new MainForm();
            int mainwidth = Screen.GetWorkingArea(main).Width;
            _ = Screen.PrimaryScreen.Bounds.Size.Width;
            int formwidth = Width;
            Location = new Point(mainwidth - formwidth - 30, 65);
        }

        private void Notifications_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape)
            {
                Dispose();
            }
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            Dispose();
        }
    }
}
