﻿using POS_System.POS;
using System;
using System.Data.SQLite;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace POS_System.LoginModule
{
    public partial class Login : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        private Point mouseOffset;

        public string _pass = "";
        public bool _isactive;
        public Login()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
            lbldt.Text = "© " + DateTime.Now.Year;
            LoadStore();
        }

        private void picClose_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are sure to exist the application?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string _username = "", _name = "", _role = "";
            try
            {
                bool found;
                cn.Open();
                cmd = new SQLiteCommand("SELECT * FROM tblUser WHERE username = @username AND password = @password", cn);
                _ = cmd.Parameters.AddWithValue("@username", txtusername.Text.ToUpper());
                _ = cmd.Parameters.AddWithValue("@password", txtpass.Text);
                dr = cmd.ExecuteReader();
                _ = dr.Read();
                if (dr.HasRows)
                {
                    found = true;
                    _username = dr["username"].ToString();
                    _name = dr["name"].ToString();
                    _role = dr["role"].ToString();
                    _pass = dr["password"].ToString();
                    _isactive = int.Parse(dr["isactive"].ToString()) == 1 ? bool.Parse("true") : bool.Parse("false");
                }
                else
                {
                    found = false;
                }
                dr.Close();
                cn.Close();
                if (found)
                {
                    if (!_isactive)
                    {
                        _ = MessageBox.Show("Account is inactive please inform your admin to reactivate it", "Inactive account", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        _ = txtusername.Focus();
                        return;
                    }
                    if (_role == "Cashier")
                    {
                        _ = MessageBox.Show("Welcome " + _name + "|", "Access Granted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtusername.Clear();
                        txtusername.Clear();
                        Hide();
                        Cashier cashier = new Cashier();
                        cashier.lblusername.Text = char.ToUpper(_username.First()) + _username.Substring(1).ToLower();
                        cashier.label3.Text = _role;
                        _ = cashier.ShowDialog();
                    }
                    else
                    {
                        _ = MessageBox.Show("Welcome " + _name + "|", "Access Granted", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtusername.Clear();
                        txtusername.Clear();
                        Hide();
                        MainForm form = new MainForm();
                        form.lbluname.Text = char.ToUpper(_username.First()) + _username.Substring(1).ToLower();
                        form.lblName.Text = char.ToUpper(_name.First()) + _name.Substring(1).ToLower();
                        form.label3.Text = _role;
                        form._pass = _pass;
                        form.Show();
                    }
                }
                else
                {
                    _ = MessageBox.Show("Invalid username and password", "Access Denied", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    _ = txtusername.Focus();
                }
            }
            catch (Exception ex)
            {
                cn.Close();
                _ = MessageBox.Show(ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtusername_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                e.Handled = true;
                _ = txtpass.Focus();
            }
        }

        private void txtpass_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == (char)13)
            {
                btnLogin.PerformClick();
            }

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are sure to exist the application?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                Application.Exit();
            }
        }

        private void pictureBox3_Click(object sender, EventArgs e)
        {
            frmAbout abt = new frmAbout();
            _ = abt.ShowDialog();
        }

        private void picMinimize_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }

        private void panel1_MouseDown(object sender, MouseEventArgs e)
        {
            mouseOffset = new Point(-e.X, -e.Y);
            panel1.Cursor = Cursors.SizeAll;
        }

        private void panel1_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Left)
            {
                Point mousePos = Control.MousePosition;
                mousePos.Offset(mouseOffset.X, mouseOffset.Y);
                Location = mousePos; //move the form to the desired location
            }
        }

        private void panel1_MouseUp(object sender, MouseEventArgs e)
        {
            panel1.Cursor = Cursors.Default;
        }
        public void LoadStore()
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT * FROM tblStore LIMIT 1", cn);
            dr = cmd.ExecuteReader();
            _ = dr.Read();
            if (dr.HasRows)
            {
                string str = dr["store"].ToString();
                storename.Text = char.ToUpper(str.First()) + str.Substring(1).ToLower();
                pictureBox2.Image = dr["filename"].ToString() != string.Empty ? Image.FromFile(Application.StartupPath + @"\logo\" + dr["filename"].ToString()) : Image.FromFile(Application.StartupPath + @"\logo\DefaultLogo.bmp");
            }
            else
            {
                storename.Text = "V4Codes Super Market";
                pictureBox2.Image = Image.FromFile(Application.StartupPath + @"\logo\DefaultLogo.bmp");
            }
            dr.Close();
            cn.Close();
        }
    }
}
