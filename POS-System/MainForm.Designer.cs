﻿namespace POS_System
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.panelTitle = new System.Windows.Forms.Panel();
            this.lblTitle = new System.Windows.Forms.Label();
            this.panelMain = new System.Windows.Forms.Panel();
            this.tp = new System.Windows.Forms.ToolTip(this.components);
            this.panelLogo = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblName = new System.Windows.Forms.Label();
            this.lbluname = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.lblUsername = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btnDashboard = new System.Windows.Forms.Button();
            this.panelSubProduct = new System.Windows.Forms.Panel();
            this.btnbarcodegen = new System.Windows.Forms.Button();
            this.btnBrand = new System.Windows.Forms.Button();
            this.btnCategory = new System.Windows.Forms.Button();
            this.btnProductList = new System.Windows.Forms.Button();
            this.panelSubStock = new System.Windows.Forms.Panel();
            this.btnStockAdjustment = new System.Windows.Forms.Button();
            this.btnStockEntry = new System.Windows.Forms.Button();
            this.panelSlide = new System.Windows.Forms.Panel();
            this.panelSubSettings = new System.Windows.Forms.Panel();
            this.btnStore = new System.Windows.Forms.Button();
            this.btnUser = new System.Windows.Forms.Button();
            this.panelSubRecord = new System.Windows.Forms.Panel();
            this.btnbookkeeping = new System.Windows.Forms.Button();
            this.btnPosRecord = new System.Windows.Forms.Button();
            this.btnSalesHistory = new System.Windows.Forms.Button();
            this.panelVendors = new System.Windows.Forms.Panel();
            this.btnCust = new System.Windows.Forms.Button();
            this.btnSupp = new System.Windows.Forms.Button();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.btnSetting = new System.Windows.Forms.Button();
            this.btnRecords = new System.Windows.Forms.Button();
            this.btnVendors = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.btnLogout = new System.Windows.Forms.Button();
            this.btnInStock = new System.Windows.Forms.Button();
            this.btnProduct = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelTitle.SuspendLayout();
            this.panelLogo.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panelSubProduct.SuspendLayout();
            this.panelSubStock.SuspendLayout();
            this.panelSlide.SuspendLayout();
            this.panelSubSettings.SuspendLayout();
            this.panelSubRecord.SuspendLayout();
            this.panelVendors.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelTitle
            // 
            this.panelTitle.Controls.Add(this.pictureBox3);
            this.panelTitle.Controls.Add(this.lblTitle);
            this.panelTitle.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelTitle.Location = new System.Drawing.Point(200, 0);
            this.panelTitle.Name = "panelTitle";
            this.panelTitle.Size = new System.Drawing.Size(984, 40);
            this.panelTitle.TabIndex = 1;
            // 
            // lblTitle
            // 
            this.lblTitle.BackColor = System.Drawing.Color.DarkSlateGray;
            this.lblTitle.Font = new System.Drawing.Font("Cascadia Mono PL", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTitle.ForeColor = System.Drawing.Color.White;
            this.lblTitle.Location = new System.Drawing.Point(238, 0);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(389, 37);
            this.lblTitle.TabIndex = 1;
            this.lblTitle.Text = "Title Name";
            this.lblTitle.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panelMain
            // 
            this.panelMain.BackColor = System.Drawing.Color.White;
            this.panelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelMain.Location = new System.Drawing.Point(200, 40);
            this.panelMain.Name = "panelMain";
            this.panelMain.Size = new System.Drawing.Size(984, 621);
            this.panelMain.TabIndex = 2;
            // 
            // panelLogo
            // 
            this.panelLogo.Controls.Add(this.panel3);
            this.panelLogo.Controls.Add(this.lblUsername);
            this.panelLogo.Controls.Add(this.label1);
            this.panelLogo.Controls.Add(this.pictureBox1);
            this.panelLogo.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelLogo.Location = new System.Drawing.Point(0, 0);
            this.panelLogo.Name = "panelLogo";
            this.panelLogo.Size = new System.Drawing.Size(183, 170);
            this.panelLogo.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.lblName);
            this.panel3.Controls.Add(this.lbluname);
            this.panel3.Controls.Add(this.label3);
            this.panel3.Controls.Add(this.pictureBox2);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(183, 170);
            this.panel3.TabIndex = 2;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.ForeColor = System.Drawing.Color.White;
            this.lblName.Location = new System.Drawing.Point(3, 76);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(25, 20);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Ln";
            this.lblName.Visible = false;
            // 
            // lbluname
            // 
            this.lbluname.ForeColor = System.Drawing.Color.White;
            this.lbluname.Location = new System.Drawing.Point(3, 116);
            this.lbluname.Name = "lbluname";
            this.lbluname.Size = new System.Drawing.Size(177, 20);
            this.lbluname.TabIndex = 1;
            this.lbluname.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Consolas", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Underline))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(0, 140);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(183, 22);
            this.label3.TabIndex = 0;
            this.label3.Text = "Administrator";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblUsername
            // 
            this.lblUsername.AutoSize = true;
            this.lblUsername.ForeColor = System.Drawing.Color.White;
            this.lblUsername.Location = new System.Drawing.Point(43, 124);
            this.lblUsername.Name = "lblUsername";
            this.lblUsername.Size = new System.Drawing.Size(85, 20);
            this.lblUsername.TabIndex = 1;
            this.lblUsername.Text = "UserName";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(35, 144);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(104, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Administrator";
            // 
            // btnDashboard
            // 
            this.btnDashboard.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnDashboard.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnDashboard.FlatAppearance.BorderSize = 0;
            this.btnDashboard.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDashboard.ForeColor = System.Drawing.Color.White;
            this.btnDashboard.Location = new System.Drawing.Point(0, 170);
            this.btnDashboard.Name = "btnDashboard";
            this.btnDashboard.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnDashboard.Size = new System.Drawing.Size(183, 45);
            this.btnDashboard.TabIndex = 0;
            this.btnDashboard.Text = "DashBoard";
            this.btnDashboard.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnDashboard.UseVisualStyleBackColor = true;
            this.btnDashboard.Click += new System.EventHandler(this.btnDashboard_Click);
            // 
            // panelSubProduct
            // 
            this.panelSubProduct.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(71)))), ((int)(((byte)(58)))));
            this.panelSubProduct.Controls.Add(this.btnbarcodegen);
            this.panelSubProduct.Controls.Add(this.btnBrand);
            this.panelSubProduct.Controls.Add(this.btnCategory);
            this.panelSubProduct.Controls.Add(this.btnProductList);
            this.panelSubProduct.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubProduct.Location = new System.Drawing.Point(0, 260);
            this.panelSubProduct.Name = "panelSubProduct";
            this.panelSubProduct.Size = new System.Drawing.Size(183, 180);
            this.panelSubProduct.TabIndex = 0;
            // 
            // btnbarcodegen
            // 
            this.btnbarcodegen.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnbarcodegen.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnbarcodegen.FlatAppearance.BorderSize = 0;
            this.btnbarcodegen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbarcodegen.ForeColor = System.Drawing.Color.White;
            this.btnbarcodegen.Location = new System.Drawing.Point(0, 135);
            this.btnbarcodegen.Name = "btnbarcodegen";
            this.btnbarcodegen.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnbarcodegen.Size = new System.Drawing.Size(183, 45);
            this.btnbarcodegen.TabIndex = 7;
            this.btnbarcodegen.Text = "Barcode Gen";
            this.btnbarcodegen.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnbarcodegen.UseVisualStyleBackColor = true;
            this.btnbarcodegen.Click += new System.EventHandler(this.btnbarcodegen_Click);
            // 
            // btnBrand
            // 
            this.btnBrand.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnBrand.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnBrand.FlatAppearance.BorderSize = 0;
            this.btnBrand.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnBrand.ForeColor = System.Drawing.Color.White;
            this.btnBrand.Location = new System.Drawing.Point(0, 90);
            this.btnBrand.Name = "btnBrand";
            this.btnBrand.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnBrand.Size = new System.Drawing.Size(183, 45);
            this.btnBrand.TabIndex = 5;
            this.btnBrand.Text = "Brands";
            this.btnBrand.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnBrand.UseVisualStyleBackColor = true;
            this.btnBrand.Click += new System.EventHandler(this.btnBrand_Click);
            // 
            // btnCategory
            // 
            this.btnCategory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCategory.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCategory.FlatAppearance.BorderSize = 0;
            this.btnCategory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCategory.ForeColor = System.Drawing.Color.White;
            this.btnCategory.Location = new System.Drawing.Point(0, 45);
            this.btnCategory.Name = "btnCategory";
            this.btnCategory.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnCategory.Size = new System.Drawing.Size(183, 45);
            this.btnCategory.TabIndex = 4;
            this.btnCategory.Text = "Category";
            this.btnCategory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCategory.UseVisualStyleBackColor = true;
            this.btnCategory.Click += new System.EventHandler(this.btnCategory_Click);
            // 
            // btnProductList
            // 
            this.btnProductList.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProductList.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProductList.FlatAppearance.BorderSize = 0;
            this.btnProductList.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProductList.ForeColor = System.Drawing.Color.White;
            this.btnProductList.Location = new System.Drawing.Point(0, 0);
            this.btnProductList.Name = "btnProductList";
            this.btnProductList.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnProductList.Size = new System.Drawing.Size(183, 45);
            this.btnProductList.TabIndex = 3;
            this.btnProductList.Text = "Product List";
            this.btnProductList.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProductList.UseVisualStyleBackColor = true;
            this.btnProductList.Click += new System.EventHandler(this.btnProductList_Click);
            // 
            // panelSubStock
            // 
            this.panelSubStock.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(71)))), ((int)(((byte)(58)))));
            this.panelSubStock.Controls.Add(this.btnStockAdjustment);
            this.panelSubStock.Controls.Add(this.btnStockEntry);
            this.panelSubStock.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubStock.Location = new System.Drawing.Point(0, 485);
            this.panelSubStock.Name = "panelSubStock";
            this.panelSubStock.Size = new System.Drawing.Size(183, 90);
            this.panelSubStock.TabIndex = 4;
            // 
            // btnStockAdjustment
            // 
            this.btnStockAdjustment.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStockAdjustment.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnStockAdjustment.FlatAppearance.BorderSize = 0;
            this.btnStockAdjustment.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockAdjustment.ForeColor = System.Drawing.Color.White;
            this.btnStockAdjustment.Location = new System.Drawing.Point(0, 45);
            this.btnStockAdjustment.Name = "btnStockAdjustment";
            this.btnStockAdjustment.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnStockAdjustment.Size = new System.Drawing.Size(183, 45);
            this.btnStockAdjustment.TabIndex = 5;
            this.btnStockAdjustment.Text = "Stock Adjustment";
            this.btnStockAdjustment.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStockAdjustment.UseVisualStyleBackColor = true;
            this.btnStockAdjustment.Click += new System.EventHandler(this.btnStockAdjustment_Click);
            // 
            // btnStockEntry
            // 
            this.btnStockEntry.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStockEntry.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnStockEntry.FlatAppearance.BorderSize = 0;
            this.btnStockEntry.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStockEntry.ForeColor = System.Drawing.Color.White;
            this.btnStockEntry.Location = new System.Drawing.Point(0, 0);
            this.btnStockEntry.Name = "btnStockEntry";
            this.btnStockEntry.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnStockEntry.Size = new System.Drawing.Size(183, 45);
            this.btnStockEntry.TabIndex = 4;
            this.btnStockEntry.Text = "Stock Entry";
            this.btnStockEntry.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStockEntry.UseVisualStyleBackColor = true;
            this.btnStockEntry.Click += new System.EventHandler(this.btnStockEntry_Click);
            // 
            // panelSlide
            // 
            this.panelSlide.AutoScroll = true;
            this.panelSlide.BackColor = System.Drawing.Color.DarkSlateGray;
            this.panelSlide.Controls.Add(this.panelSubSettings);
            this.panelSlide.Controls.Add(this.btnSetting);
            this.panelSlide.Controls.Add(this.panelSubRecord);
            this.panelSlide.Controls.Add(this.btnRecords);
            this.panelSlide.Controls.Add(this.panelVendors);
            this.panelSlide.Controls.Add(this.btnVendors);
            this.panelSlide.Controls.Add(this.button1);
            this.panelSlide.Controls.Add(this.btnLogout);
            this.panelSlide.Controls.Add(this.panelSubStock);
            this.panelSlide.Controls.Add(this.btnInStock);
            this.panelSlide.Controls.Add(this.panelSubProduct);
            this.panelSlide.Controls.Add(this.btnProduct);
            this.panelSlide.Controls.Add(this.btnDashboard);
            this.panelSlide.Controls.Add(this.panelLogo);
            this.panelSlide.Dock = System.Windows.Forms.DockStyle.Left;
            this.panelSlide.Location = new System.Drawing.Point(0, 0);
            this.panelSlide.Name = "panelSlide";
            this.panelSlide.Size = new System.Drawing.Size(200, 661);
            this.panelSlide.TabIndex = 0;
            // 
            // panelSubSettings
            // 
            this.panelSubSettings.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(71)))), ((int)(((byte)(58)))));
            this.panelSubSettings.Controls.Add(this.btnStore);
            this.panelSubSettings.Controls.Add(this.btnUser);
            this.panelSubSettings.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubSettings.Location = new System.Drawing.Point(0, 935);
            this.panelSubSettings.Name = "panelSubSettings";
            this.panelSubSettings.Size = new System.Drawing.Size(183, 90);
            this.panelSubSettings.TabIndex = 20;
            // 
            // btnStore
            // 
            this.btnStore.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnStore.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnStore.FlatAppearance.BorderSize = 0;
            this.btnStore.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStore.ForeColor = System.Drawing.Color.White;
            this.btnStore.Location = new System.Drawing.Point(0, 45);
            this.btnStore.Name = "btnStore";
            this.btnStore.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnStore.Size = new System.Drawing.Size(183, 45);
            this.btnStore.TabIndex = 5;
            this.btnStore.Text = "Store";
            this.btnStore.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnStore.UseVisualStyleBackColor = true;
            this.btnStore.Click += new System.EventHandler(this.btnStore_Click);
            // 
            // btnUser
            // 
            this.btnUser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnUser.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnUser.FlatAppearance.BorderSize = 0;
            this.btnUser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnUser.ForeColor = System.Drawing.Color.White;
            this.btnUser.Location = new System.Drawing.Point(0, 0);
            this.btnUser.Name = "btnUser";
            this.btnUser.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnUser.Size = new System.Drawing.Size(183, 45);
            this.btnUser.TabIndex = 4;
            this.btnUser.Text = "User";
            this.btnUser.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnUser.UseVisualStyleBackColor = true;
            this.btnUser.Click += new System.EventHandler(this.btnUser_Click);
            // 
            // panelSubRecord
            // 
            this.panelSubRecord.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(71)))), ((int)(((byte)(58)))));
            this.panelSubRecord.Controls.Add(this.btnbookkeeping);
            this.panelSubRecord.Controls.Add(this.btnPosRecord);
            this.panelSubRecord.Controls.Add(this.btnSalesHistory);
            this.panelSubRecord.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSubRecord.Location = new System.Drawing.Point(0, 755);
            this.panelSubRecord.Name = "panelSubRecord";
            this.panelSubRecord.Size = new System.Drawing.Size(183, 135);
            this.panelSubRecord.TabIndex = 18;
            // 
            // btnbookkeeping
            // 
            this.btnbookkeeping.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnbookkeeping.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnbookkeeping.FlatAppearance.BorderSize = 0;
            this.btnbookkeeping.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnbookkeeping.ForeColor = System.Drawing.Color.White;
            this.btnbookkeeping.Location = new System.Drawing.Point(0, 90);
            this.btnbookkeeping.Name = "btnbookkeeping";
            this.btnbookkeeping.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnbookkeeping.Size = new System.Drawing.Size(183, 45);
            this.btnbookkeeping.TabIndex = 6;
            this.btnbookkeeping.Text = "Book Keeping";
            this.btnbookkeeping.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnbookkeeping.UseVisualStyleBackColor = true;
            this.btnbookkeeping.Click += new System.EventHandler(this.button2_Click);
            // 
            // btnPosRecord
            // 
            this.btnPosRecord.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnPosRecord.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnPosRecord.FlatAppearance.BorderSize = 0;
            this.btnPosRecord.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPosRecord.ForeColor = System.Drawing.Color.White;
            this.btnPosRecord.Location = new System.Drawing.Point(0, 45);
            this.btnPosRecord.Name = "btnPosRecord";
            this.btnPosRecord.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnPosRecord.Size = new System.Drawing.Size(183, 45);
            this.btnPosRecord.TabIndex = 5;
            this.btnPosRecord.Text = "POS Record";
            this.btnPosRecord.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnPosRecord.UseVisualStyleBackColor = true;
            this.btnPosRecord.Click += new System.EventHandler(this.btnPosRecord_Click);
            // 
            // btnSalesHistory
            // 
            this.btnSalesHistory.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSalesHistory.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSalesHistory.FlatAppearance.BorderSize = 0;
            this.btnSalesHistory.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSalesHistory.ForeColor = System.Drawing.Color.White;
            this.btnSalesHistory.Location = new System.Drawing.Point(0, 0);
            this.btnSalesHistory.Name = "btnSalesHistory";
            this.btnSalesHistory.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnSalesHistory.Size = new System.Drawing.Size(183, 45);
            this.btnSalesHistory.TabIndex = 4;
            this.btnSalesHistory.Text = "Sales History";
            this.btnSalesHistory.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSalesHistory.UseVisualStyleBackColor = true;
            this.btnSalesHistory.Click += new System.EventHandler(this.btnSalesHistory_Click);
            // 
            // panelVendors
            // 
            this.panelVendors.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(51)))), ((int)(((byte)(71)))), ((int)(((byte)(58)))));
            this.panelVendors.Controls.Add(this.btnCust);
            this.panelVendors.Controls.Add(this.btnSupp);
            this.panelVendors.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelVendors.Location = new System.Drawing.Point(0, 620);
            this.panelVendors.Name = "panelVendors";
            this.panelVendors.Size = new System.Drawing.Size(183, 90);
            this.panelVendors.TabIndex = 16;
            // 
            // btnCust
            // 
            this.btnCust.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnCust.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnCust.FlatAppearance.BorderSize = 0;
            this.btnCust.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCust.ForeColor = System.Drawing.Color.White;
            this.btnCust.Location = new System.Drawing.Point(0, 45);
            this.btnCust.Name = "btnCust";
            this.btnCust.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnCust.Size = new System.Drawing.Size(183, 45);
            this.btnCust.TabIndex = 5;
            this.btnCust.Text = "Customers";
            this.btnCust.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCust.UseVisualStyleBackColor = true;
            this.btnCust.Click += new System.EventHandler(this.btnCust_Click);
            // 
            // btnSupp
            // 
            this.btnSupp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSupp.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSupp.FlatAppearance.BorderSize = 0;
            this.btnSupp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSupp.ForeColor = System.Drawing.Color.White;
            this.btnSupp.Location = new System.Drawing.Point(0, 0);
            this.btnSupp.Name = "btnSupp";
            this.btnSupp.Padding = new System.Windows.Forms.Padding(35, 0, 0, 0);
            this.btnSupp.Size = new System.Drawing.Size(183, 45);
            this.btnSupp.TabIndex = 4;
            this.btnSupp.Text = "Suppliers";
            this.btnSupp.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSupp.UseVisualStyleBackColor = true;
            this.btnSupp.Click += new System.EventHandler(this.btnSupp_Click);
            // 
            // pictureBox3
            // 
            this.pictureBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBox3.Image = global::POS_System.Properties.Resources.notification;
            this.pictureBox3.Location = new System.Drawing.Point(892, 6);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(30, 30);
            this.pictureBox3.TabIndex = 2;
            this.pictureBox3.TabStop = false;
            this.pictureBox3.Click += new System.EventHandler(this.pictureBox3_Click);
            this.pictureBox3.MouseHover += new System.EventHandler(this.pictureBox3_MouseHover);
            // 
            // btnSetting
            // 
            this.btnSetting.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSetting.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnSetting.FlatAppearance.BorderSize = 0;
            this.btnSetting.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSetting.ForeColor = System.Drawing.Color.White;
            this.btnSetting.Image = global::POS_System.Properties.Resources.Arrow_20px;
            this.btnSetting.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnSetting.Location = new System.Drawing.Point(0, 890);
            this.btnSetting.Name = "btnSetting";
            this.btnSetting.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnSetting.Size = new System.Drawing.Size(183, 45);
            this.btnSetting.TabIndex = 19;
            this.btnSetting.Text = "Settings";
            this.btnSetting.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnSetting.UseVisualStyleBackColor = true;
            this.btnSetting.Click += new System.EventHandler(this.btnSetting_Click);
            // 
            // btnRecords
            // 
            this.btnRecords.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnRecords.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnRecords.FlatAppearance.BorderSize = 0;
            this.btnRecords.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRecords.ForeColor = System.Drawing.Color.White;
            this.btnRecords.Image = global::POS_System.Properties.Resources.Arrow_20px;
            this.btnRecords.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnRecords.Location = new System.Drawing.Point(0, 710);
            this.btnRecords.Name = "btnRecords";
            this.btnRecords.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnRecords.Size = new System.Drawing.Size(183, 45);
            this.btnRecords.TabIndex = 17;
            this.btnRecords.Text = "Records";
            this.btnRecords.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnRecords.UseVisualStyleBackColor = true;
            this.btnRecords.Click += new System.EventHandler(this.btnRecords_Click);
            // 
            // btnVendors
            // 
            this.btnVendors.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnVendors.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnVendors.FlatAppearance.BorderSize = 0;
            this.btnVendors.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnVendors.ForeColor = System.Drawing.Color.White;
            this.btnVendors.Image = global::POS_System.Properties.Resources.Arrow_20px;
            this.btnVendors.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnVendors.Location = new System.Drawing.Point(0, 575);
            this.btnVendors.Name = "btnVendors";
            this.btnVendors.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnVendors.Size = new System.Drawing.Size(183, 45);
            this.btnVendors.TabIndex = 15;
            this.btnVendors.Text = "Vendors";
            this.btnVendors.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnVendors.UseVisualStyleBackColor = true;
            this.btnVendors.Click += new System.EventHandler(this.btnVendors_Click);
            // 
            // button1
            // 
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.ForeColor = System.Drawing.Color.White;
            this.button1.Image = global::POS_System.Properties.Resources.receipt_35px;
            this.button1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button1.Location = new System.Drawing.Point(0, 1025);
            this.button1.Name = "button1";
            this.button1.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.button1.Size = new System.Drawing.Size(183, 45);
            this.button1.TabIndex = 11;
            this.button1.Text = "            Cashier Panel";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnLogout
            // 
            this.btnLogout.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnLogout.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.btnLogout.FlatAppearance.BorderSize = 0;
            this.btnLogout.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnLogout.ForeColor = System.Drawing.Color.White;
            this.btnLogout.Image = global::POS_System.Properties.Resources.logout_35px;
            this.btnLogout.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnLogout.Location = new System.Drawing.Point(0, 1070);
            this.btnLogout.Name = "btnLogout";
            this.btnLogout.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnLogout.Size = new System.Drawing.Size(183, 45);
            this.btnLogout.TabIndex = 10;
            this.btnLogout.Text = "Logout";
            this.btnLogout.UseVisualStyleBackColor = true;
            this.btnLogout.Click += new System.EventHandler(this.btnLogout_Click);
            // 
            // btnInStock
            // 
            this.btnInStock.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnInStock.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnInStock.FlatAppearance.BorderSize = 0;
            this.btnInStock.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnInStock.ForeColor = System.Drawing.Color.White;
            this.btnInStock.Image = global::POS_System.Properties.Resources.Arrow_20px;
            this.btnInStock.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnInStock.Location = new System.Drawing.Point(0, 440);
            this.btnInStock.Name = "btnInStock";
            this.btnInStock.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnInStock.Size = new System.Drawing.Size(183, 45);
            this.btnInStock.TabIndex = 3;
            this.btnInStock.Text = "In Stocks";
            this.btnInStock.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnInStock.UseVisualStyleBackColor = true;
            this.btnInStock.Click += new System.EventHandler(this.btnInStock_Click);
            // 
            // btnProduct
            // 
            this.btnProduct.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnProduct.Dock = System.Windows.Forms.DockStyle.Top;
            this.btnProduct.FlatAppearance.BorderSize = 0;
            this.btnProduct.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnProduct.ForeColor = System.Drawing.Color.White;
            this.btnProduct.Image = global::POS_System.Properties.Resources.Arrow_20px;
            this.btnProduct.ImageAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnProduct.Location = new System.Drawing.Point(0, 215);
            this.btnProduct.Name = "btnProduct";
            this.btnProduct.Padding = new System.Windows.Forms.Padding(5, 0, 0, 0);
            this.btnProduct.Size = new System.Drawing.Size(183, 45);
            this.btnProduct.TabIndex = 2;
            this.btnProduct.Text = "Product";
            this.btnProduct.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnProduct.UseVisualStyleBackColor = true;
            this.btnProduct.Click += new System.EventHandler(this.btnProduct_Click);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::POS_System.Properties.Resources.person_100px;
            this.pictureBox2.Location = new System.Drawing.Point(41, 6);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(100, 100);
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(47, 21);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(75, 75);
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.DarkSlateGray;
            this.ClientSize = new System.Drawing.Size(1184, 661);
            this.Controls.Add(this.panelMain);
            this.Controls.Add(this.panelTitle);
            this.Controls.Add(this.panelSlide);
            this.Font = new System.Drawing.Font("Century Gothic", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 5, 4, 5);
            this.MinimumSize = new System.Drawing.Size(1200, 700);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Point Of Sales";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.MainForm_FormClosing);
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.panelTitle.ResumeLayout(false);
            this.panelLogo.ResumeLayout(false);
            this.panelLogo.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panelSubProduct.ResumeLayout(false);
            this.panelSubStock.ResumeLayout(false);
            this.panelSlide.ResumeLayout(false);
            this.panelSubSettings.ResumeLayout(false);
            this.panelSubRecord.ResumeLayout(false);
            this.panelVendors.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Panel panelMain;
        private System.Windows.Forms.Label lblTitle;
        public System.Windows.Forms.ToolTip tp;
        private System.Windows.Forms.Panel panelLogo;
        private System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label lblName;
        public System.Windows.Forms.Label lbluname;
        public System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label lblUsername;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnDashboard;
        private System.Windows.Forms.Button btnProduct;
        private System.Windows.Forms.Panel panelSubProduct;
        private System.Windows.Forms.Button btnBrand;
        private System.Windows.Forms.Button btnCategory;
        private System.Windows.Forms.Button btnProductList;
        private System.Windows.Forms.Button btnInStock;
        private System.Windows.Forms.Panel panelSubStock;
        public System.Windows.Forms.Button btnStockEntry;
        private System.Windows.Forms.Button btnLogout;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Panel panelSlide;
        private System.Windows.Forms.Panel panelSubSettings;
        private System.Windows.Forms.Button btnStore;
        private System.Windows.Forms.Button btnUser;
        private System.Windows.Forms.Button btnSetting;
        private System.Windows.Forms.Panel panelSubRecord;
        private System.Windows.Forms.Button btnPosRecord;
        private System.Windows.Forms.Button btnSalesHistory;
        private System.Windows.Forms.Button btnRecords;
        private System.Windows.Forms.Panel panelVendors;
        private System.Windows.Forms.Button btnCust;
        private System.Windows.Forms.Button btnSupp;
        private System.Windows.Forms.Button btnVendors;
        private System.Windows.Forms.Button btnbarcodegen;
        private System.Windows.Forms.Button btnbookkeeping;
        public System.Windows.Forms.PictureBox pictureBox3;
        public System.Windows.Forms.Button btnStockAdjustment;
    }
}

