﻿using System;
using System.Data;
using System.Data.SQLite;
using System.Drawing;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace POS_System.Dashboard
{
    public partial class MainDashboard : Form
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private readonly SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        public MainDashboard()
        {
            InitializeComponent();
            cn = new SQLiteConnection(dbCon.myConnection());
        }

        private void MainDashboard_Load(object sender, EventArgs e)
        {
            string sdate = DateTime.Now.ToShortDateString();
            lbldailysales.Text = dbCon.ExtractData("SELECT IFNULL(SUM(total),0) AS total FROM tblCart WHERE status LIKE 'Sold' AND sdate BETWEEN '" + sdate + "' AND '" + sdate + "'").ToString("#,##0.00");
            lbltotalproduct.Text = dbCon.ExtractData("SELECT COUNT(*) FROM tblProduct WHERE isactive = 1").ToString("#,##0");
            lblstockonhand.Text = dbCon.ExtractData("SELECT IFNULL(SUM(qty),0) AS qty FROM tblProduct WHERE isactive = 1").ToString("#,##0");
            lblcriticalstocks.Text = dbCon.ExtractData("SELECT COUNT(*) FROM vwCriticalItems").ToString("#,##0");
            FillChart();
            FillPIChart();
        }
        public void FillChart()
        {
            DataTable dt = dbCon.getTable("SELECT IFNULL(SUM(total),0) AS total, case cast (strftime('%w', sdate) as integer) when 0 then 'Sunday' when 1 then 'Monday' when 2 then 'Tuesday'  when 3 then 'Wednesday'  when 4 then 'Thursday'  when 5 then 'Friday' else 'Saturday' end AS dayss, date(sdate) as sdate FROM tblCart WHERE status LIKE 'Sold' GROUP BY date(sdate) ORDER BY date(sdate) LIMIT 7");
            chart1.DataSource = dt;
            chart1.Series["Sales"].XValueMember = "sdate";
            chart1.Series["Sales"].YValueMembers = "total";
            chart1.Series["Sales"].ToolTip = "#VAL";

            Title title = new Title
            {
                Font = new Font("Consolas", 14, FontStyle.Bold)
            };
            title.Font = new Font("Consolas", 14, FontStyle.Underline);
            title.Text = "Daily sales last 7 days";
            chart1.Titles.Add(title);
        }
        public void FillPIChart()
        {
            DataTable dt = dbCon.getTable("SELECT pcode, pdesc, ifnull(sum(qty),0) AS qty, ifnull(sum(total),0) AS total FROM vwTopSelling WHERE status LIKE 'Sold' GROUP BY pcode, pdesc ORDER BY qty DESC LIMIT 10");
            chart2.DataSource = dt;
            chart2.Series["Top Selling"].XValueMember = "pdesc";
            chart2.Series["Top Selling"].ToolTip = "#VAL";
            chart2.Series["Top Selling"].YValueMembers = "qty";
            Title title = new Title
            {
                Font = new Font("Consolas", 14, FontStyle.Bold)
            };
            title.Font = new Font("Consolas", 14, FontStyle.Underline);
            title.Text = "Top 10 most selling products";
            chart2.Titles.Add(title);
        }
    }
}
