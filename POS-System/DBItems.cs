﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace POS_System
{
    public class DBItems
    {
        private readonly SQLiteConnection cn = new SQLiteConnection();
        private SQLiteCommand cmd = new SQLiteCommand();
        private readonly DBConnect dbCon = new DBConnect();
        private SQLiteDataReader dr;
        public DBItems()
        {
            cn = new SQLiteConnection(dbCon.myConnection());
        }
        public void SetupDB()
        {
            ProductTable();
            TempTable();
            StoreTable();
            CriticalItemsViews();
            InventoryItemsViews();
            StockInTable();
            StockInView();
            StockAdjustTable();
            StockAdjustmentView();
        }
        public void ProductTable()
        {
            cn.Open();
            cmd = new SQLiteCommand("PRAGMA table_info(tblProduct)", cn);
            dr = cmd.ExecuteReader();
            bool actualpriceExists = false;
            bool isactiveExists = false;

            while (dr.Read())
            {
                string columnName = dr.GetString(1);
                if (columnName == "actualprice")
                {
                    actualpriceExists = true;
                }
                if (columnName == "isactive")
                {
                    isactiveExists = true;
                }
            }

            if (!actualpriceExists)
            {
                cmd = new SQLiteCommand("ALTER TABLE tblProduct ADD COLUMN actualprice REAL DEFAULT 0.00", cn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message, "SQL Exception error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (!isactiveExists)
            {
                cmd = new SQLiteCommand("ALTER TABLE tblProduct ADD COLUMN isactive INTEGER DEFAULT 1", cn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message, "SQL Exception error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            dr.Close();
            cn.Close();
        }
        public void StoreTable()
        {
            cn.Open();
            cmd = new SQLiteCommand("PRAGMA table_info(tblStore)", cn);
            dr = cmd.ExecuteReader();
            bool bookfieldExists = false;
            bool paymentfiledExists = false;

            while (dr.Read())
            {
                string columnName = dr.GetString(1);
                if (columnName == "bookfield")
                {
                    bookfieldExists = true;
                }
                if (columnName == "paymentfiled")
                {
                    paymentfiledExists = true;
                }
            }

            if (!bookfieldExists)
            {
                cmd = new SQLiteCommand("ALTER TABLE tblStore ADD COLUMN bookfield INTEGER DEFAULT 1", cn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message, "SQL Exception error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (!paymentfiledExists)
            {
                cmd = new SQLiteCommand("ALTER TABLE tblStore ADD COLUMN paymentfiled INTEGER DEFAULT 1", cn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message, "SQL Exception error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            dr.Close();
            cn.Close();
        }
        public void TempTable()
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT name FROM sqlite_master WHERE type='table' AND name='tmpStock'", cn);
            dr = cmd.ExecuteReader();

            if (!dr.Read())
            {
                cmd = new SQLiteCommand("CREATE TABLE tmpStock (stockid INTEGER,pcode TEXT,qty NUMERIC,sellingprice REAL,actualprice REAL)", cn);
                cmd.ExecuteNonQuery();
            }
            dr.Close();
            cn.Close();
        }
        public void CriticalItemsViews()
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT sql FROM sqlite_master WHERE type='view' AND name='vwCriticalItems'", cn);
            dr = cmd.ExecuteReader();
            bool needtomodify = false;


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    var stringsql = "CREATE VIEW vwCriticalItems AS SELECT tblProduct.pcode, tblProduct.barcode, tblProduct.pdesc, tblBrand.brand, tblCategory.category, tblProduct.price, tblProduct.reorder, tblProduct.qty, tblProduct.unit FROM tblProduct INNER JOIN tblCategory ON tblProduct.cid = tblCategory.id INNER JOIN tblBrand ON tblProduct.bid = tblBrand.id WHERE (tblProduct.qty <= tblProduct.reorder)";
                    if (dr[0].ToString() == stringsql)
                    {
                        needtomodify = false;
                    }
                    else
                    {
                        needtomodify = true;
                    }
                }
                if (needtomodify)
                {
                    SQLiteCommand Dropcmd = new SQLiteCommand("DROP VIEW vwCriticalItems", cn);
                    Dropcmd.ExecuteNonQuery();
                    SQLiteCommand Createcmd = new SQLiteCommand("CREATE VIEW vwCriticalItems AS SELECT tblProduct.pcode, tblProduct.barcode, tblProduct.pdesc, tblBrand.brand, tblCategory.category, tblProduct.price,tblProduct.actualprice, tblProduct.reorder, tblProduct.qty, tblProduct.unit FROM tblProduct INNER JOIN tblCategory ON tblProduct.cid = tblCategory.id INNER JOIN tblBrand ON tblProduct.bid = tblBrand.id WHERE (tblProduct.qty <= tblProduct.reorder)", cn);
                    Createcmd.ExecuteNonQuery();
                }

            }
            else
            {
                cmd = new SQLiteCommand("CREATE VIEW vwCriticalItems AS SELECT tblProduct.pcode, tblProduct.barcode, tblProduct.pdesc, tblBrand.brand, tblCategory.category, tblProduct.price,tblProduct.actualprice, tblProduct.reorder, tblProduct.qty, tblProduct.unit FROM tblProduct INNER JOIN tblCategory ON tblProduct.cid = tblCategory.id INNER JOIN tblBrand ON tblProduct.bid = tblBrand.id WHERE (tblProduct.qty <= tblProduct.reorder)", cn);
                cmd.ExecuteNonQuery();
            }
            dr.Close();
            cn.Close();
        }
        public void InventoryItemsViews()
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT sql FROM sqlite_master WHERE type='view' AND name='vwInventoryList'", cn);
            dr = cmd.ExecuteReader();
            bool needtomodify = false;


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    var stringsql = "CREATE VIEW vwInventoryList AS SELECT tblProduct.pcode, tblProduct.barcode, tblProduct.pdesc, tblBrand.brand,	tblCategory.category, tblProduct.price,	tblProduct.actualprice,	tblProduct.qty,	tblProduct.reorder,	tblProduct.unit FROM tblProduct INNER JOIN tblCategory ON tblProduct.cid = tblCategory.id INNER JOIN tblBrand ON tblProduct.bid=tblBrand.id";
                    if (dr[0].ToString() == stringsql)
                    {
                        needtomodify = false;
                    }
                    else
                    {
                        needtomodify = true;
                    }
                }
                if (needtomodify)
                {
                    SQLiteCommand Dropcmd = new SQLiteCommand("DROP VIEW vwInventoryList", cn);
                    Dropcmd.ExecuteNonQuery();
                    SQLiteCommand Createcmd = new SQLiteCommand("CREATE VIEW vwInventoryList AS SELECT tblProduct.pcode, tblProduct.barcode, tblProduct.pdesc, tblBrand.brand,	tblCategory.category, tblProduct.price,	tblProduct.actualprice,	tblProduct.qty,	tblProduct.reorder,	tblProduct.unit FROM tblProduct INNER JOIN tblCategory ON tblProduct.cid = tblCategory.id INNER JOIN tblBrand ON tblProduct.bid=tblBrand.id", cn);
                    Createcmd.ExecuteNonQuery();
                }

            }
            else
            {
                cmd = new SQLiteCommand("CREATE VIEW vwInventoryList AS SELECT tblProduct.pcode, tblProduct.barcode, tblProduct.pdesc, tblBrand.brand,	tblCategory.category, tblProduct.price,	tblProduct.actualprice,	tblProduct.qty,	tblProduct.reorder,	tblProduct.unit FROM tblProduct INNER JOIN tblCategory ON tblProduct.cid = tblCategory.id INNER JOIN tblBrand ON tblProduct.bid=tblBrand.id", cn);
                cmd.ExecuteNonQuery();
            }
            dr.Close();
            cn.Close();
        }
        public void StockInTable()
        {
            cn.Open();
            cmd = new SQLiteCommand("PRAGMA table_info(tblStock)", cn);
            dr = cmd.ExecuteReader();
            bool actprice = false;
            bool sellprice = false;

            while (dr.Read())
            {
                string columnName = dr.GetString(1);
                if (columnName == "actprice")
                {
                    actprice = true;
                }
                if (columnName == "sellprice")
                {
                    sellprice = true;
                }
            }

            if (!actprice)
            {
                cmd = new SQLiteCommand("ALTER TABLE tblStock ADD COLUMN actprice REAL DEFAULT 0.00", cn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message, "SQL Exception error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (!sellprice)
            {
                cmd = new SQLiteCommand("ALTER TABLE tblStock ADD COLUMN sellprice REAL DEFAULT 0.00", cn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message, "SQL Exception error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            dr.Close();
            cn.Close();
        }
        public void StockInView()
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT sql FROM sqlite_master WHERE type='view' AND name='vwStockIn'", cn);
            dr = cmd.ExecuteReader();
            bool needtomodify = false;


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    var stringsql = @"CREATE VIEW vwStockIn
                                    AS 
                                    SELECT
	                                    tblStock.id,
	                                    tblStock.refno,
	                                    tblStock.pcode,
	                                    tblProduct.pdesc,
	                                    tblStock.qty,
	                                    tblStock.actprice,
	                                    tblStock.sellprice as price,
	                                    date(tblStock.sdate) as sdate,
	                                    tblStock.stockinby,
	                                    tblSupplier.supplier,
	                                    tblStock.status,
	                                    tblProduct.unit
                                    FROM tblStock
                                    INNER JOIN tblProduct ON tblStock.pcode = tblProduct.pcode
                                    INNER JOIN tblSupplier ON tblStock.supplierid = tblSupplier.id";
                    if (dr[0].ToString() == stringsql)
                    {
                        needtomodify = false;
                    }
                    else
                    {
                        needtomodify = true;
                    }
                }
                if (needtomodify)
                {
                    SQLiteCommand Dropcmd = new SQLiteCommand("DROP VIEW vwStockIn", cn);
                    Dropcmd.ExecuteNonQuery();
                    SQLiteCommand Createcmd = new SQLiteCommand(@"CREATE VIEW vwStockIn
                                                                AS 
                                                                SELECT
	                                                                tblStock.id,
	                                                                tblStock.refno,
	                                                                tblStock.pcode,
	                                                                tblProduct.pdesc,
	                                                                tblStock.qty,
	                                                                tblStock.actprice,
	                                                                tblStock.sellprice as price,
	                                                                date(tblStock.sdate) as sdate,
	                                                                tblStock.stockinby,
	                                                                tblSupplier.supplier,
	                                                                tblStock.status,
	                                                                tblProduct.unit
                                                                FROM tblStock
                                                                INNER JOIN tblProduct ON tblStock.pcode = tblProduct.pcode
                                                                INNER JOIN tblSupplier ON tblStock.supplierid = tblSupplier.id", cn);
                    Createcmd.ExecuteNonQuery();
                }
            }
            else
            {
                cmd = new SQLiteCommand(@"CREATE VIEW vwStockIn
                                        AS 
                                        SELECT
	                                        tblStock.id,
	                                        tblStock.refno,
	                                        tblStock.pcode,
	                                        tblProduct.pdesc,
	                                        tblStock.qty,
	                                        tblStock.actprice,
	                                        tblStock.sellprice as price,
	                                        date(tblStock.sdate) as sdate,
	                                        tblStock.stockinby,
	                                        tblSupplier.supplier,
	                                        tblStock.status,
	                                        tblProduct.unit
                                        FROM tblStock
                                        INNER JOIN tblProduct ON tblStock.pcode = tblProduct.pcode
                                        INNER JOIN tblSupplier ON tblStock.supplierid = tblSupplier.id", cn);
                cmd.ExecuteNonQuery();
            }
            dr.Close();
            cn.Close();
        }
        public void StockAdjustTable()
        {
            cn.Open();
            cmd = new SQLiteCommand("PRAGMA table_info(tblAdjustment)", cn);
            dr = cmd.ExecuteReader();
            bool actprice = false;
            bool sellprice = false;

            while (dr.Read())
            {
                string columnName = dr.GetString(1);
                if (columnName == "actprice")
                {
                    actprice = true;
                }
                if (columnName == "sellprice")
                {
                    sellprice = true;
                }
            }

            if (!actprice)
            {
                cmd = new SQLiteCommand("ALTER TABLE tblAdjustment ADD COLUMN actprice REAL DEFAULT 0.00", cn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message, "SQL Exception error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            if (!sellprice)
            {
                cmd = new SQLiteCommand("ALTER TABLE tblAdjustment ADD COLUMN sellprice REAL DEFAULT 0.00", cn);
                try
                {
                    cmd.ExecuteNonQuery();
                }
                catch (SQLiteException ex)
                {
                    MessageBox.Show(ex.Message, "SQL Exception error", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            }
            dr.Close();
            cn.Close();
        }
        public void StockAdjustmentView()
        {
            cn.Open();
            cmd = new SQLiteCommand("SELECT sql FROM sqlite_master WHERE type='view' AND name='vwStockAdjustment'", cn);
            dr = cmd.ExecuteReader();
            bool needtomodify = false;


            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    var stringsql = @"CREATE VIEW vwStockAdjustment
                                        AS 
                                        SELECT
	                                        tblAdjustment.referenceno,
	                                        tblAdjustment.pcode,
	                                        tblProduct.pdesc,
	                                        tblAdjustment.qty,
	                                        tblAdjustment.actprice,
	                                        tblAdjustment.sellprice as price,
	                                        date(tblAdjustment.sdate) as sdate,
	                                        tblAdjustment.user,
	                                        tblAdjustment.action,
	                                        tblAdjustment.remarks,
	                                        tblProduct.unit,
	                                        tblAdjustment.id
                                        FROM tblAdjustment
                                        INNER JOIN tblProduct ON tblAdjustment.pcode = tblProduct.pcode";
                    if (dr[0].ToString() == stringsql)
                    {
                        needtomodify = false;
                    }
                    else
                    {
                        needtomodify = true;
                    }
                }
                if (needtomodify)
                {
                    SQLiteCommand Dropcmd = new SQLiteCommand("DROP VIEW vwStockAdjustment", cn);
                    Dropcmd.ExecuteNonQuery();
                    SQLiteCommand Createcmd = new SQLiteCommand(@"CREATE VIEW vwStockAdjustment
                                                                    AS 
                                                                    SELECT
	                                                                    tblAdjustment.referenceno,
	                                                                    tblAdjustment.pcode,
	                                                                    tblProduct.pdesc,
	                                                                    tblAdjustment.qty,
	                                                                    tblAdjustment.actprice,
	                                                                    tblAdjustment.sellprice as price,
	                                                                    date(tblAdjustment.sdate) as sdate,
	                                                                    tblAdjustment.user,
	                                                                    tblAdjustment.action,
	                                                                    tblAdjustment.remarks,
	                                                                    tblProduct.unit,
	                                                                    tblAdjustment.id
                                                                    FROM tblAdjustment
                                                                    INNER JOIN tblProduct ON tblAdjustment.pcode = tblProduct.pcode", cn);
                    Createcmd.ExecuteNonQuery();
                }
            }
            else
            {
                cmd = new SQLiteCommand(@"CREATE VIEW vwStockAdjustment
                                            AS 
                                            SELECT
	                                            tblAdjustment.referenceno,
	                                            tblAdjustment.pcode,
	                                            tblProduct.pdesc,
	                                            tblAdjustment.qty,
	                                            tblAdjustment.actprice,
	                                            tblAdjustment.sellprice as price,
	                                            date(tblAdjustment.sdate) as sdate,
	                                            tblAdjustment.user,
	                                            tblAdjustment.action,
	                                            tblAdjustment.remarks,
	                                            tblProduct.unit,
	                                            tblAdjustment.id
                                            FROM tblAdjustment
                                            INNER JOIN tblProduct ON tblAdjustment.pcode = tblProduct.pcode", cn);
                cmd.ExecuteNonQuery();
            }
            dr.Close();
            cn.Close();
        }
    }
}
